/*
Navicat MySQL Data Transfer

Source Server         : pzz
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : wymusic

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2018-06-22 09:58:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album` (
  `albumId` int(20) NOT NULL AUTO_INCREMENT,
  `albumName` varchar(255) NOT NULL,
  `songerId` int(20) DEFAULT NULL,
  `songerName` varchar(255) NOT NULL,
  `albumStyle` varchar(255) DEFAULT NULL,
  `publicCompany` varchar(255) DEFAULT NULL,
  `publicArea` varchar(255) DEFAULT NULL,
  `publicTime` datetime DEFAULT NULL,
  `albumInfo` text,
  `albumOverUrl` varchar(255) DEFAULT NULL COMMENT '专辑封面地址',
  `albumStatus` int(10) NOT NULL DEFAULT '1' COMMENT '0为已下架 1为正常 2为无版权',
  `albumHot` int(255) DEFAULT '0' COMMENT '0',
  PRIMARY KEY (`albumId`),
  KEY `songerId` (`songerId`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES ('1', '7080 추억이 울리는 음악다방', '1', '韩国群星 (Korea Various Artists)', '抒情，人气', '暂无', '韩国', '2016-01-22 00:00:00', '韩国群星于2016年1月22日发行专辑《7080 추억이 울리는 음악다방 (7080 발라드, 그룹사운드, 통기타 인기가요 베스트)》。', 'be827dfb-1c33-4500-b3ae-00956d005cd8.jpg', '1', '7984');
INSERT INTO `album` VALUES ('2', 'All Falls Down', '2', 'Alan Walker', '流行电音', '索尼音乐娱乐公司', '欧美', '2017-10-27 00:00:00', '《All Falls Down》由挪威DJ、音乐制作人Alan Walker、英国DJ Digital Farm Animals联合制作，瑞典歌手Juliander、美国新星歌手麦莉·赛勒斯（Miley Cyrus）的亲妹妹诺雅·塞勒斯（Noah Cyrus）演唱，于2017年10月27日通过索尼音乐娱乐公司发行   。', '8e953ac1-7a24-4583-a6b4-93803d8c7345.jpg', '1', '45654');
INSERT INTO `album` VALUES ('3', '壹伍壹捌·离', '3', 'Snigellin', 'pop', '暂无', '未知', '2016-08-11 00:00:00', '一首纯音乐，敬请欣赏。', '8ff18e09-3a80-4665-8be7-47b2252b319b.jpg', '1', '464132');
INSERT INTO `album` VALUES ('4', 'Blossom', '4', 'J.Fla (제이플라)', '流行音乐，R&B，Dancehall', 'Ostereo', '韩国', '2017-03-10 00:00:00', '韩国歌手J.Fla于2017年3月10日发布翻唱专辑《Blossom》，包括《Shape of You》等欧美热单。', 'fdd23c4a-661a-4e55-95e4-ed8515637340.jpg', '1', '4');
INSERT INTO `album` VALUES ('5', '3ラムジ (3 Lambsey)', '5', 'ラムジ (Lambsey)', '流行音乐', 'エイベックス・マーケティング・コミュニケーションズ', '日本', '2006-06-07 00:00:00', '昨年11月に「1ラムジ」でデビュー。ショボクレた風貌には似つかない、聴く者を惹きつける声と楽曲のラムジ！MTVと共に制作したドラマPVは「3ラムジ」にて遂に完結！そのドラマPVは小説化も決定！M3「捨て犬（仮）」には175Rがコーラスで参加！', 'a9aa52ce-c31d-4e48-8a8a-463dacd830a5.jpg', '1', '456');
INSERT INTO `album` VALUES ('6', 'WONDER Tourism', '6', 'DAISHI DANCE', '和风', 'UNIVERSAL MUSIC JAPAN', '日本', '2012-11-14 00:00:00', '3年ぶりとなる久々のオリジナル作品は、COLDFEETや吉田兄弟、blanc.など馴染みの面々に加え、Crystal Kayやピクシー・ロットら国内外のビッグネームも招かれています。叙情的なシンセを多用した歌ものハウスで踊らせるなか、GILLEの力強いヴォーカルやハネるビートを盛り立てた、武田真司のファンキーなサックスに惚れる“NEW GATE”が白眉。トランス感が炸裂した見事なフロア・チューンになっています!', 'c8af4ee1-822b-4c91-838a-7e43d5715a89.jpg', '1', '45646');
INSERT INTO `album` VALUES ('7', 'Battlecry', '7', 'Two Steps From Hell', '纯音乐,摇滚', 'Two Steps From Hell', '欧美', '2015-04-28 00:00:00', '《Battlecry》专辑是Two Steps From Hell [1]  公司于2015年4月28日发布的第八张公开专辑，第一次发行的2CD专辑，专辑由Thomas Bergersen（托马斯·伯格森）和Nick Phoenix（尼克·菲尼克斯）完成26首曲目。歌曲人声参与有Merethe Soltvedt，Nick Phoenix和Felicia Farerre。专辑封面由Steven R. Gilmore设计', '43afb288-7137-44cf-966d-d42833f2dfac.jpg', '1', '123156');
INSERT INTO `album` VALUES ('8', 'The Spectre', '2', 'Alan Walker', '电音', '索尼音乐娱乐公司', '欧美', '2017-09-15 00:00:00', '《The Spectre》是由挪威DJ、音乐制作人Alan Walker制作的一首流行舞曲，单曲于2017年9月15日通过索尼音乐娱乐公司正式发行 。', '09765644-37ed-4d6d-a013-7c8e4327f420.jpg', '1', '7895');
INSERT INTO `album` VALUES ('9', '君の名は。', '9', 'RADWIMPS', '流行', '暂无', '日本', '2016-08-24 00:00:00', '《なんでもないや（没什么大不了）》为新海诚监督动画电影新作《你的名字。》的片尾曲，由RADWIMPS演唱，同时电影中出现的2个版本收录于专辑《君の名は。（你的名字）》，专辑于8月24日发售。', '9f0baac7-8cba-4f74-b3ab-89dc1898d6e4.jpg', '1', '5463');
INSERT INTO `album` VALUES ('10', '一切随风', '10', '张国荣', '流行', '环球唱片公司', '港台', '2003-07-08 00:00:00', '《一切随风》是张国荣的遗作专辑，收录了张国荣临终前所灌录的10首歌曲。这张专辑由环球唱片公司于2003年发行，专辑的制作人是梁荣骏。', '7f2e67ec-94d1-41f8-97d2-8213a51689fd.jpg', '0', '4451213');
INSERT INTO `album` VALUES ('11', '春梦秋云', '11', '邓丽君', '古典', '无', '港台', '2015-05-08 00:00:00', '《清平调》，是邓丽君于20世纪90代初，邀请当时新生代的作曲家曹俊鸿，将唐代文坛巨擘李白同名七言名绝句谱成的新曲。邓丽君仅录制半首歌后即去世，录音档一录完整封存超过20年，也躲过上世纪96年香港宝丽金公司大火劫难。', '4837ff02-387e-40e0-b684-132a80f218ec.jpg', '0', '4564');
INSERT INTO `album` VALUES ('12', 'test', '11', '邓丽君', 'test', 'test', 'test', '2099-03-05 00:00:00', 'test----------------》', '59aa65af-9ef6-41e7-9940-749bed979016.jpg', '2', '54545');
INSERT INTO `album` VALUES ('20', 'ttt', '16', 'qqq', 'qqq', 'qqq', 'qqq', '2014-03-02 00:00:00', 'wwwwwwwwwwwwwwwwwwsss', 'b72d794b-4dc0-4837-8804-b36be9e37b91.jpg', '2', '0');
INSERT INTO `album` VALUES ('21', '帅帅帅帅', '31', '庄心妍', '帅帅帅帅', '帅帅帅帅帅帅', '帅帅帅帅1111', '2014-11-03 00:00:00', '帅帅帅帅帅帅帅帅帅帅帅帅帅帅', '01761bdf-d27c-4022-81bd-c0e212fd3fad.jpg', '2', '0');
INSERT INTO `album` VALUES ('22', '彭泽泽贼帅', '31', '庄心妍', '帅', '帅', '中国', '2018-05-23 00:00:00', '彭泽泽贼帅！！！！！', '0df6f0ba-d29d-4a5c-bc26-14cfaee59165.jpg', '2', '0');
INSERT INTO `album` VALUES ('25', '不安', '31', '庄心妍', null, null, null, null, null, null, '2', '0');
INSERT INTO `album` VALUES ('26', '测试专辑', '31', '庄心妍', null, null, null, null, null, null, '2', '0');
INSERT INTO `album` VALUES ('27', 'Red', '32', 'Taylor Swift', 'Blues', '外星唱片公司', '欧美', '2012-10-22 00:00:00', '《Red》是美国女歌手泰勒·斯威夫特录制的第四张录音室专辑，该专辑于2012年10月22日通过大机器唱片公司首发 ，标准版唱片共收录16首歌曲，豪华版唱片则多收录3首歌曲、两首语音备忘录以及一首原音版歌曲。该专辑的音乐总监和创意总监分别由斯科特·波切塔、泰勒·斯威夫特担任，中国大陆引进版于2012年12月12日通过星外星唱片公司发行   。', '5f0a3d07-3cf7-455c-b490-8a449805c33d.jpg', '1', '0');
INSERT INTO `album` VALUES ('33', '电光幻影', '33', '杨千嬅', '流行', '未知', '港台', '2004-06-25 00:00:00', '《电光幻影》是由林夕作词，于逸尧作曲，杨千嬅演唱的一首歌曲，收录在同名专辑《电光幻影》中。', 'c324c489-09bf-4067-88a1-fe8f2d5886e5.png', '1', '0');
INSERT INTO `album` VALUES ('34', '测试', '35', '周深', null, null, null, null, null, null, '1', '0');

-- ----------------------------
-- Table structure for albumcomments
-- ----------------------------
DROP TABLE IF EXISTS `albumcomments`;
CREATE TABLE `albumcomments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `commentId` int(20) NOT NULL,
  `albumId` int(20) DEFAULT NULL,
  `albumName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `commentId` (`commentId`),
  KEY `albumId` (`albumId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of albumcomments
-- ----------------------------
INSERT INTO `albumcomments` VALUES ('1', '1', '33', '电光幻影');
INSERT INTO `albumcomments` VALUES ('2', '2', '33', '电光幻影');
INSERT INTO `albumcomments` VALUES ('3', '3', '33', '电光幻影');
INSERT INTO `albumcomments` VALUES ('4', '4', '11', '春梦秋云');
INSERT INTO `albumcomments` VALUES ('5', '7', '2', 'All Falls Down');
INSERT INTO `albumcomments` VALUES ('6', '8', '2', 'All Falls Down');
INSERT INTO `albumcomments` VALUES ('7', '9', '2', 'All Falls Down');
INSERT INTO `albumcomments` VALUES ('8', '10', '2', 'All Falls Down');
INSERT INTO `albumcomments` VALUES ('9', '11', '1', '7080 추억이 울리는 음악다방');
INSERT INTO `albumcomments` VALUES ('10', '12', '1', '7080 추억이 울리는 음악다방');

-- ----------------------------
-- Table structure for albumcomts
-- ----------------------------
DROP TABLE IF EXISTS `albumcomts`;
CREATE TABLE `albumcomts` (
  `commentId` int(20) NOT NULL AUTO_INCREMENT,
  `userId` int(20) NOT NULL,
  `context` text,
  `status` int(10) NOT NULL COMMENT '1表示通过审核，0表示已删除,2审核不通过',
  `pubTime` datetime DEFAULT NULL,
  PRIMARY KEY (`commentId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of albumcomts
-- ----------------------------
INSERT INTO `albumcomts` VALUES ('1', '9', '这个专辑贼棒', '1', '2018-06-03 13:13:35');
INSERT INTO `albumcomts` VALUES ('2', '11', '我的意见跟刘亦菲一样，这个专辑贼棒', '1', '2018-06-03 13:14:24');
INSERT INTO `albumcomts` VALUES ('3', '12', '这个专辑贼棒', '1', '2018-06-02 13:15:12');
INSERT INTO `albumcomts` VALUES ('4', '3', '你们可以上往忆云网站系统上找到好听专辑哦', '1', '2018-06-01 13:17:06');
INSERT INTO `albumcomts` VALUES ('7', '9', '英文歌我很喜欢  音乐很好第三方士大夫莱克斯顿风刀霜剑六年级发顺丰', '1', '2018-06-04 01:06:20');
INSERT INTO `albumcomts` VALUES ('8', '9', '酸豆角方法见识到了附近的时刻火箭发射', '1', '2018-06-04 01:06:26');
INSERT INTO `albumcomts` VALUES ('9', '9', '年末数扫ID还撒谎达康书记安徽的撒铜峰电子VB想象中非常', '1', '2018-06-04 01:06:33');
INSERT INTO `albumcomts` VALUES ('10', '9', '归来仍是少年', '1', '2018-06-04 01:06:58');
INSERT INTO `albumcomts` VALUES ('11', '9', 'sjlfjsda;ofhasdljfjiasdf', '1', '2018-06-04 19:34:30');
INSERT INTO `albumcomts` VALUES ('12', '11', '还撒谎的的撒娇立刻打开建设大街', '1', '2018-06-08 08:50:54');

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `commentId` int(20) NOT NULL AUTO_INCREMENT,
  `userId` int(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `context` text,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1表示通过审核，0表示已删除,2审核不通过',
  `pubTime` datetime DEFAULT NULL,
  PRIMARY KEY (`commentId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('1', '9', '刘亦菲', '这首歌真的很好听，我很喜欢', '1', '2018-06-03 13:13:35');
INSERT INTO `comments` VALUES ('2', '11', '彭泽泽', '我的意见跟刘亦菲一样，觉得这个确实不错啊', '1', '2018-06-03 13:14:24');
INSERT INTO `comments` VALUES ('3', '2', '宋祖儿', '我觉得这首歌歌词美', '1', '2018-06-02 13:15:12');
INSERT INTO `comments` VALUES ('4', '13', '庄心妍', '你们可以上往忆云网站系统上找到好听的歌曲哦', '1', '2018-06-01 13:17:06');
INSERT INTO `comments` VALUES ('6', '9', '刘亦菲', '这首歌节奏真的很好  我很喜欢这种类型的歌曲 。', '1', '2018-06-03 23:17:29');
INSERT INTO `comments` VALUES ('7', '12', '刘珂矣', '我太喜欢这首歌了  整的沙口路等会撒来得及萨达暗示大！', '1', '2018-06-03 23:23:06');
INSERT INTO `comments` VALUES ('8', '12', '刘珂矣', '我太喜欢这首歌了  整的沙口路等会撒来得及萨达暗示大！\n讲的是 扩大师傅大是大非', '1', '2018-06-03 23:25:08');
INSERT INTO `comments` VALUES ('9', '12', '刘珂矣', '我太喜欢这首歌了  整的沙口路等会撒来得及萨达暗示大！\n讲的是 扩大师傅大是大非', '1', '2018-06-03 23:26:41');
INSERT INTO `comments` VALUES ('10', '12', '刘珂矣', '的sad撒多撒多撒规范会计理论和规范健康挂号费', '1', '2018-06-03 23:27:28');
INSERT INTO `comments` VALUES ('11', '11', '彭泽泽', '叶鹏。牛B啊', '1', '2018-06-07 20:14:22');

-- ----------------------------
-- Table structure for comments_copy
-- ----------------------------
DROP TABLE IF EXISTS `comments_copy`;
CREATE TABLE `comments_copy` (
  `commentId` int(20) NOT NULL AUTO_INCREMENT,
  `userId` int(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `context` text,
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1表示通过审核，0表示已删除,2审核不通过',
  `pubTime` datetime DEFAULT NULL,
  PRIMARY KEY (`commentId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments_copy
-- ----------------------------
INSERT INTO `comments_copy` VALUES ('1', '9', '刘亦菲', '这首歌真的很好听，我很喜欢', '1', '2018-06-03 13:13:35');
INSERT INTO `comments_copy` VALUES ('2', '11', '彭泽泽', '我的意见跟刘亦菲一样，觉得这个确实不错啊', '2', '2018-06-03 13:14:24');
INSERT INTO `comments_copy` VALUES ('3', '2', '宋祖儿', '我觉得这首歌歌词美', '1', '2018-06-02 13:15:12');
INSERT INTO `comments_copy` VALUES ('4', '13', '庄心妍', '你们可以上往忆云网站系统上找到好听的歌曲哦', '1', '2018-06-01 13:17:06');

-- ----------------------------
-- Table structure for mv
-- ----------------------------
DROP TABLE IF EXISTS `mv`;
CREATE TABLE `mv` (
  `mvId` int(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`mvId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mv
-- ----------------------------

-- ----------------------------
-- Table structure for songer
-- ----------------------------
DROP TABLE IF EXISTS `songer`;
CREATE TABLE `songer` (
  `songerId` int(20) NOT NULL AUTO_INCREMENT,
  `songerName` varchar(255) DEFAULT NULL,
  `songerCountry` varchar(255) DEFAULT NULL,
  `songerInfo` text,
  `songerPicUrl` varchar(255) DEFAULT NULL,
  `songerStatus` int(10) NOT NULL DEFAULT '1' COMMENT '0为已下架 1为正常 2为无版权',
  `songerHot` int(255) DEFAULT '0' COMMENT '歌手热门程度',
  PRIMARY KEY (`songerId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songer
-- ----------------------------
INSERT INTO `songer` VALUES ('1', '韩国群星', '韩国', '韩国群星于2016年1月22日发行专辑《7080 추억이 울리는 음악다방 (7080 발라드, 그룹사운드, 통기타 인기가요 베스트)》。', 'c33ede73-37a2-4e11-b265-3a029abe0293.jpg', '1', '1');
INSERT INTO `songer` VALUES ('2', 'Alan Walker', '挪威', '艾兰·奥拉夫·沃克（Alan Olav Walker），1997年8月24日出生于英国英格兰北安普敦郡，挪威DJ、音乐制作人。', 'ecdbba45-c4a2-4a16-9b7f-34473731da8b.jpg', '1', '2');
INSERT INTO `songer` VALUES ('3', 'Snigellin', '未知', '暂无', '7a29bf3b-66ad-4845-a2b8-6e97f7373c6d.jpg', '1', '3');
INSERT INTO `songer` VALUES ('4', 'J.Fla (제이플라)', '韩国', 'J.Fla，原名Kim Jung Hwa，韩国歌手,歌曲制作人。', '508af61d-ab79-41f8-a787-fa1b2eb9cd0a.jpg', '1', '4');
INSERT INTO `songer` VALUES ('5', 'ラムジ (Lambsey)', '日韩组合', '山下祐樹 （やました ゆうき、1981年7月15日 - ），担任组合主唱。当时遇到井上慎二郎时，看到他被雨淋湿的姿态，联想到了动画《ラムヂーちゃん》里“子羊”这一角色，因决定采用ラムジ这一名字为组合名。\r\n山下祐樹 （やました ゆうき、1981年7月15日 - ），担任组合主唱。当时遇到井上慎二郎时，看到他被雨淋湿的姿态，联想到了动画《ラムヂーちゃん》里“子羊”这一角色，因决定采用ラムジ这一名字为组合名。\r\n井上 慎二郎 （いのうえ しんじろう、別名：SJR、Shinjiroh Inoue、1967年7月14）。负责音乐制作，吉他担当。是组合所属公司株式会社グロール的社长。', '8b803caa-9b00-48ff-a3a6-1ad6ebc43585.jpg', '1', '5');
INSERT INTO `songer` VALUES ('6', 'DAISHI DANCE', '日本', 'DAISHI DANCE，一位在日本，乃至世界，都享有盛誉的DJ。他来自日本北海道，曾为滨崎步、中岛美嘉打造过mix版本的HEAVEN和SAKURA~花霞，他的标志是：如涓涓流水般细腻，流畅的钢琴伴奏；表演时惯用3台turntable混音；将哀愁与优雅发挥得淋漓尽致。', '3d548518-833c-415c-b3cd-9239480edc40.jpg', '1', '6');
INSERT INTO `songer` VALUES ('7', 'Two Steps From Hell', '美国', 'Two Steps from Hell / 两步逃离地狱（又名 地狱边缘、地狱咫尺、两步逃离地狱、逃陷地域）成立于2006年3月，总部设于美国加州洛杉矶，是一家专业的音乐制作公司，Two Steps from Hell (以下简称TSFH) 是由尼克·菲尼克斯和托马斯·J·柏格森共同创办，他们创作的音乐类型属于SOUND TRACK（原声配乐），而现在此公司属于HANS ZIMMER (汉斯季莫) 旗下的RC公司所拥有。', 'd2e259ca-1c22-4e94-8b77-e9b5c4e10c7a.jpg', '1', '7');
INSERT INTO `songer` VALUES ('8', '亮声open', '中国', '二四六晚上 斗鱼直播间：636792 公众号搜索：亮声open', '35c450b4-4b24-43ad-bb69-e3ede68c3651.jpg', '1', '8');
INSERT INTO `songer` VALUES ('9', 'RADWIMPS', '日本', 'RADWIMPS，日本摇滚乐队，简称RAD。所属唱片公司为emi music japan，所属事务所为bokuchin（旗下艺人只有RADWIMPS，英文记做voque ting）。乐队名是有表示极好的，顶呱呱的rad（偏英语俚语）和表示没用的人的wimp组合而成的，大概整理理解为就是非常帅气的胆小鬼，非常有用的混蛋这样的意思。', 'c33866a1-b8d6-45fb-8dfe-5437f0df7608.jpg', '1', '9');
INSERT INTO `songer` VALUES ('10', '张国荣', '中国', '张国荣（1956年9月12日-2003年4月1日），生于香港，男歌手、演员、音乐人；影视歌多栖发展的代表之一。', 'bcac144a-4948-41b9-bafa-de96ff59c834.jpg', '1', '1000');
INSERT INTO `songer` VALUES ('11', '邓丽君', '中国', '邓丽君（1953年1月29日-1995年5月8日），出生于中国台湾省云林县，祖籍河北省大名县，中国台湾歌唱家、日本昭和时代代表性日语女歌手之一。', '60412bc0-cd6f-40c7-a8bc-b6f7edb216ea.jpg', '1', '100');
INSERT INTO `songer` VALUES ('12', '买辣椒也用券', '中国', '无', '4f50dcc8-e965-446e-b35f-60ccf01a9867.jpg', '1', '1300');
INSERT INTO `songer` VALUES ('13', '陈粒', '中国', '陈粒，又名粒粒，1990年7月26日出生于贵州省贵阳市，中国内地民谣女歌手、独立音乐人、唱作人，前空想家乐队主唱，毕业于上海对外经贸大学。', '8edbeec7-2d91-4d0d-a548-e05c59ca97b5.jpg', '1', null);
INSERT INTO `songer` VALUES ('14', 'dsad', '未知', '', '53a60ae6-6249-4264-acd8-fc7947547e36.jpg', '1', null);
INSERT INTO `songer` VALUES ('15', 'sss', '未知', '', '90801094-086e-4717-bb80-91db26657d12.jpg', '0', null);
INSERT INTO `songer` VALUES ('16', 'qqq', '英国', '', '5acf840a-dbb7-4f96-bfe5-af229a8d1ab7.jpg', '0', null);
INSERT INTO `songer` VALUES ('17', '达到撒旦', '未知', '123456', 'c10c8f16-d1c2-404e-8c8d-62b52daa3571.jpg', '1', null);
INSERT INTO `songer` VALUES ('18', '姜曹杨', '中国', '很牛B！！', 'f409789e-fb0c-4ea1-98c2-4e23a67d6ab8.jpg', '1', null);
INSERT INTO `songer` VALUES ('23', '帅帅', null, null, null, '0', '0');
INSERT INTO `songer` VALUES ('24', '泽泽', null, null, null, '0', '0');
INSERT INTO `songer` VALUES ('31', '庄心妍', null, null, null, '0', '0');
INSERT INTO `songer` VALUES ('32', 'Taylor Swift', '美国', '泰勒·斯威夫特（Taylor Swift），1989年12月13日出生于美国宾夕法尼亚州，美国流行音乐、乡村音乐创作型女歌手、音乐制作人、演员、慈善家。', 'baa852fe-e76b-4cb8-9519-1f453b0245d1.jpg', '1', '0');
INSERT INTO `songer` VALUES ('33', '杨千嬅', '中国', '杨千嬅，原名杨泽嬅，1974年2月3日出生于中国香港，香港流行女歌手、影视演员，毕业于香港理工大学。', '22683749-56d9-4a81-ab6f-d1415abb726e.jpg', '1', '0');
INSERT INTO `songer` VALUES ('34', '陈慧娴', '中国', '陈慧娴，1965年7月28日出生于香港，中国香港女歌手。\r\n1984年，凭借歌曲《逝去的诺言》出道，同年获得十大中文金曲最有前途新人奖 [1]  。1985年因演唱歌曲《花店》而在香港歌坛受到关注。', 'e2683b3a-da5e-4503-b3ed-b057f1046350.jpg', '1', '0');
INSERT INTO `songer` VALUES ('35', '周深', null, null, null, '1', '0');

-- ----------------------------
-- Table structure for songercomments
-- ----------------------------
DROP TABLE IF EXISTS `songercomments`;
CREATE TABLE `songercomments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `commentId` int(20) DEFAULT NULL,
  `songerId` int(20) DEFAULT NULL,
  `songerName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songercomments
-- ----------------------------

-- ----------------------------
-- Table structure for songlist
-- ----------------------------
DROP TABLE IF EXISTS `songlist`;
CREATE TABLE `songlist` (
  `songlistId` int(20) NOT NULL AUTO_INCREMENT,
  `songlistPic` varchar(255) DEFAULT NULL,
  `songlistName` varchar(255) DEFAULT NULL,
  `songlistInfo` text,
  `userId` int(20) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `songlistStatus` int(10) NOT NULL DEFAULT '1' COMMENT '0 为已删除 1为正常',
  PRIMARY KEY (`songlistId`),
  KEY `u_id` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songlist
-- ----------------------------
INSERT INTO `songlist` VALUES ('1', '7ede8e6a-536a-4342-95b5-ba8035d1bcb0.jpg', '魑魅魍魉', '好听', '11', '彭泽泽', '1');
INSERT INTO `songlist` VALUES ('2', 'a7809cc4-dd36-4abd-8edf-7c4482b42cb1.jpg', '古风', '宁静', '11', '彭泽泽', '1');
INSERT INTO `songlist` VALUES ('3', 'f6c2d18a-ee6f-4c5b-8208-50cead963eb2.jpg', '中文歌曲', '我爱你', '11', '彭泽泽', '1');
INSERT INTO `songlist` VALUES ('4', 'f6c2d18a-ee6f-4c5b-8208-50cead963eb2.jpg', '英文歌曲', '听不懂', '11', '彭泽泽', '1');
INSERT INTO `songlist` VALUES ('5', '1a78867c-4241-487d-b27c-2451e13aa2ce.jpg', '皮皮虾', '皮一下很开心', '9', '刘亦菲', '1');
INSERT INTO `songlist` VALUES ('6', '7e228c44-29e9-490f-b160-3aba8e5ca05a.jpg', '开心果', '虽然他有点小赚，但是我不会亏', '9', '刘亦菲', '1');
INSERT INTO `songlist` VALUES ('7', '10e182ce-9c8f-4ea0-83d2-5be44fc51dbe.jpg', 'jFla', '韩国小姐姐。侧颜贼漂亮欧！', '11', null, '1');

-- ----------------------------
-- Table structure for songlistandsongs
-- ----------------------------
DROP TABLE IF EXISTS `songlistandsongs`;
CREATE TABLE `songlistandsongs` (
  `songlistAndsongsId` int(20) NOT NULL AUTO_INCREMENT,
  `songlistId` int(20) DEFAULT NULL,
  `songlistName` varchar(255) DEFAULT NULL,
  `songsId` int(20) DEFAULT NULL,
  `songsName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`songlistAndsongsId`),
  KEY `song_list_id` (`songlistId`),
  KEY `song_id` (`songsId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songlistandsongs
-- ----------------------------
INSERT INTO `songlistandsongs` VALUES ('1', '1', '魑魅魍魉', '13', '起风了');
INSERT INTO `songlistandsongs` VALUES ('2', '1', '魑魅魍魉', '3', 'Sakura Tears');
INSERT INTO `songlistandsongs` VALUES ('3', '3', '中文歌曲', '9', '风的季节');
INSERT INTO `songlistandsongs` VALUES ('4', '3', '中文歌曲', '11', '玻璃之情');
INSERT INTO `songlistandsongs` VALUES ('5', '3', '中文歌曲', '12', '清平调');
INSERT INTO `songlistandsongs` VALUES ('6', '6', '开心果', '15', 'Red');
INSERT INTO `songlistandsongs` VALUES ('7', '3', '中文歌曲', '16', '处处吻');
INSERT INTO `songlistandsongs` VALUES ('10', '4', '英文歌曲', '17', '电光幻影');
INSERT INTO `songlistandsongs` VALUES ('11', '1', '魑魅魍魉', '17', '电光幻影');
INSERT INTO `songlistandsongs` VALUES ('12', '3', '中文歌曲', '17', '电光幻影');
INSERT INTO `songlistandsongs` VALUES ('13', '3', '中文歌曲', '18', '假如让我说下去');
INSERT INTO `songlistandsongs` VALUES ('14', '3', '中文歌曲', '19', '烈女');
INSERT INTO `songlistandsongs` VALUES ('15', '3', '中文歌曲', '22', '少女的祈祷');
INSERT INTO `songlistandsongs` VALUES ('16', '3', '中文歌曲', '23', '小城大事');
INSERT INTO `songlistandsongs` VALUES ('17', '3', '中文歌曲', '25', '勇');
INSERT INTO `songlistandsongs` VALUES ('18', '3', '中文歌曲', '26', '再见二丁目');
INSERT INTO `songlistandsongs` VALUES ('19', '7', 'jFla', '4', 'Shape of You');
INSERT INTO `songlistandsongs` VALUES ('20', '1', '魑魅魍魉', '1', '담다디 (Damdadi)');
INSERT INTO `songlistandsongs` VALUES ('21', '7', 'jFla', '1', '담다디 (Damdadi)');
INSERT INTO `songlistandsongs` VALUES ('22', '1', '魑魅魍魉', '26', '再见二丁目');
INSERT INTO `songlistandsongs` VALUES ('23', '1', '魑魅魍魉', '18', '假如让我说下去');
INSERT INTO `songlistandsongs` VALUES ('24', '1', '魑魅魍魉', '19', '烈女');
INSERT INTO `songlistandsongs` VALUES ('25', '4', '英文歌曲', '3', 'Sakura Tears');
INSERT INTO `songlistandsongs` VALUES ('26', '3', '中文歌曲', '4', 'Shape of You');
INSERT INTO `songlistandsongs` VALUES ('27', '1', '魑魅魍魉', '4', 'Shape of You');
INSERT INTO `songlistandsongs` VALUES ('28', '7', 'jFla', '9', '风的季节');
INSERT INTO `songlistandsongs` VALUES ('29', '1', '魑魅魍魉', '11', '玻璃之情');
INSERT INTO `songlistandsongs` VALUES ('30', '2', '古风', '12', '清平调');
INSERT INTO `songlistandsongs` VALUES ('31', '1', '魑魅魍魉', '16', '处处吻');

-- ----------------------------
-- Table structure for songs
-- ----------------------------
DROP TABLE IF EXISTS `songs`;
CREATE TABLE `songs` (
  `songId` int(20) NOT NULL AUTO_INCREMENT,
  `songName` varchar(255) NOT NULL,
  `songStyle` varchar(50) DEFAULT NULL,
  `albumId` int(20) DEFAULT NULL,
  `songerId` int(20) NOT NULL,
  `mp3Url` varchar(255) DEFAULT NULL,
  `songUrl` varchar(255) DEFAULT NULL,
  `songLrcUrl` varchar(255) DEFAULT NULL,
  `songLrc` text,
  `songTime` varchar(255) NOT NULL,
  `songStatus` int(5) NOT NULL DEFAULT '1' COMMENT '0为已下架 1为正常 2为无版权',
  `songHot` int(255) DEFAULT '0',
  PRIMARY KEY (`songId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songs
-- ----------------------------
INSERT INTO `songs` VALUES ('1', '담다디 (Damdadi)', '抒情', '1', '1', '/mp3/韩国群星 (Korea Various Artists) - 담다디 (Damdadi).mp3', '/pic/71189882-93c5-4eb8-8fa1-f87de2983f22.jpg', null, '담다디 (Damdadi) (《请回答 1988》韩剧插曲|\'88 제9회 강변가요제 대상) - 韩国群星 (Korea Various Artists)\r\n\r\n词：김남균\r\n\r\n曲：김남균\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 담다디 담\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 다다담\r\n\r\n그대는 나를 떠나려나요\r\n\r\n내 마음 이렇게 아프게 하고\r\n\r\n그대는 나를 떠나려나요\r\n\r\n내 마음이 이렇게 슬프게 하고\r\n\r\n그대는 나를 사랑할 수 없나요\r\n\r\n난 정말 그대를 사랑해\r\n\r\n그대가 나를 떠나도\r\n\r\n난 정말 그대를 사랑해\r\n\r\n그대가 나를 떠나도\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 담다디 담\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 다다담\r\n\r\n난 정말 그대 그리워 할 수 없나요\r\n\r\n당신께 이렇게 애원합니다\r\n\r\n난 정말 그대 사랑할 수 없나요\r\n\r\n날 사랑한다고 속삭여줘요\r\n\r\n그대는 나를 사랑할 수 없나요\r\n\r\n난 정말 그대를 사랑해\r\n\r\n그대가 나를 떠나도\r\n\r\n난 정말 그대를 사랑해\r\n\r\n그대가 나를 떠나도\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 담다디 담\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 다다담\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 담다디 담\r\n\r\n담다디 담다디 담다디 담\r\n\r\n담다디 다담 다다담', '02:56', '1', '1');
INSERT INTO `songs` VALUES ('2', 'All Falls Down', '流行电音', '2', '2', '/mp3/Alan Walker - All Falls Down.mp3', '/pic/222ad122-d6ae-41be-8e64-fc664a537b8a.jpg', null, 'All Falls Down - Alan Walker/Noah Cyrus/Digital Farm Animals\r\n\r\nWritten by：Alan Walker/Anders Froen/Pablo Bowman/Nick Gale/Rick Boardman/Sarah Blanchard/Daniel Boyle\r\n\r\nWhat\'s the trick I wish I knew\r\n\r\nI\'m so done with thinking through\r\n\r\nAll the things I could\'ve been\r\n\r\nAnd I know you wonder too\r\n\r\nAll it takes is that one look you do\r\n\r\nAnd I run right back to you\r\n\r\nU crossed the line\r\n\r\n& it\'s time to say F u\r\n\r\nWhat\'s the point in saying that\r\n\r\nWhen u know how I\'ll react\r\n\r\nU think u can just take it back\r\n\r\nBut sh*t just don\'t work like that\r\n\r\nYou\'re the drug that I\'m addicted to\r\n\r\nAnd I want you so bad\r\n\r\nGuess I\'m stuck with you\r\n\r\nAnd that\'s that\r\n\r\nCus when it all falls down then whatever\r\n\r\nWhen it don\'t work out for the better\r\n\r\nIf we just ain\'t right and it\'s time to say goodbye\r\n\r\nWhen it all falls down\r\n\r\nWhen it all falls down\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nYou\'re the drug that I\'m addicted to\r\n\r\nAnd I want you so bad\r\n\r\nBut I\'ll be fine\r\n\r\nWhy we fight I don\'t know\r\n\r\nWe say what hurts the most\r\n\r\nOh I try staying cold\r\n\r\nBut you take it personal\r\n\r\nAll this firing shots\r\n\r\nAnd making grounder\r\n\r\nIt\'s way too hard to cope\r\n\r\nBut I still can\'t let you go go go\r\n\r\nCus when it all falls down then whatever\r\n\r\nWhen it don\'t work out for the better\r\n\r\nIf we just ain\'t right and it\'s time to say goodbye\r\n\r\nWhen it all falls down\r\n\r\nWhen it all falls down\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nYou\'re the drug that I\'m addicted to\r\n\r\nAnd I want you so bad\r\n\r\nBut I\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nFalling down\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine fine fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nFalling down\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nCus when it all falls down then whatever\r\n\r\nWhen it don\'t work out for the better\r\n\r\nIf we just ain\'t right and it\'s time to say goodbye\r\n\r\nWhen it all falls down\r\n\r\nWhen it all falls down\r\n\r\nI\'ll be fine\r\n\r\nFine fine fine\r\n\r\nI\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nYou\'re the drug that I\'m addicted to\r\n\r\nAnd I want you so bad\r\n\r\nBut I\'ll be fine\r\n\r\nI\'ll be fine\r\n\r\nAnd that\'s that', '03:19', '1', '2');
INSERT INTO `songs` VALUES ('3', 'Sakura Tears', 'pop', '3', '3', '/mp3/Snigellin - Sakura Tears.mp3', '/pic/20c81bee-b594-4204-8164-b634e2aa3245.jpg', null, '此歌曲为没有填词的纯音乐，请您欣赏', '03:04', '1', '3');
INSERT INTO `songs` VALUES ('4', 'Shape of You', '流行音乐，R&B，Dancehall', '4', '4', '/mp3/J.Fla (제이플라) - Shape of You.mp3', '/pic/4e7db506-d101-4d7d-afc6-f1970be06803.jpg', null, 'Shape of You - J.Fla (제이플라)\r\n\r\nThe club isn\'t the best place to find a lover\r\n\r\nSo the bar is where I go\r\n\r\nMe and my friends at the table doing shots\r\n\r\nDrinking fast and then we talk slow\r\n\r\nCome over and start up a conversation with just me\r\n\r\nAnd trust me I\'ll give it a chance now\r\n\r\nTake my hand stop\r\n\r\nPut van the man on the jukebox\r\n\r\nAnd then we start to dance\r\n\r\nAnd now I\'m singing like\r\n\r\nDo you know I want your love\r\n\r\nYour love was handmade for somebody like me\r\n\r\nCome on now follow my lead\r\n\r\nI may be crazy don\'t mind me\r\n\r\nSay boy let\'s not talk too much\r\n\r\nGrab on my waist and put that body on me\r\n\r\nCome on now follow my lead\r\n\r\nCome come on now follow my lead\r\n\r\nI\'m in love with the shape of you\r\n\r\nWe push and pull like a magnet do\r\n\r\nAlthough my heart is falling too\r\n\r\nI\'m in love with your body\r\n\r\nLast night you were in my room\r\n\r\nAnd now my bed sheets smell like you\r\n\r\nEvery day discovering something brand new\r\n\r\nOh I\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nEvery day discovering something brand new\r\n\r\nI\'m in love with the shape of you\r\n\r\nOne week in we let the story begin\r\n\r\nWe\'re going out on our first date\r\n\r\nBut you and me are thrifty\r\n\r\nSo go all you can eat\r\n\r\nFill up your bag and I fill up a plate\r\n\r\nWe talk for hours and hours about the sweet and the sour\r\n\r\nAnd how your family is doing okay\r\n\r\nLeave and get in a taxi then kiss in the back seat\r\n\r\nTell the driver make the radio play\r\n\r\nAnd now I\'m singing like\r\n\r\nDo you know I want your love\r\n\r\nYour love was handmade for somebody like me\r\n\r\nCome on now follow my lead\r\n\r\nI may be crazy don\'t mind me\r\n\r\nSay boy let\'s not talk too much\r\n\r\nGrab on my waist and put that body on me\r\n\r\nCome on now follow my lead\r\n\r\nCome come on now follow my lead\r\n\r\nI\'m in love with the shape of you\r\n\r\nWe push and pull like a magnet do\r\n\r\nAlthough my heart is falling too\r\n\r\nI\'m in love with your body\r\n\r\nLast night you were in my room\r\n\r\nAnd now my bed sheets smell like you\r\n\r\nEvery day discovering something brand new\r\n\r\nOh I\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nOh I oh I oh I oh I\r\n\r\nI\'m in love with your body\r\n\r\nEvery day discovering something brand new\r\n\r\nI\'m in love with the shape of you', '02:52', '1', '4');
INSERT INTO `songs` VALUES ('5', 'PLANET', '流行音乐', '5', '5', '/mp3/ラムジ (Lambsey) - PLANET.mp3', '/pic/25a31f26-c732-444a-bd8a-d9ef1c2393c9.jpg', null, 'PLANET - ラムジ\r\n\r\n詞：ラムジ\r\n\r\n曲：ラムジ\r\n\r\nどうやってこうやって\r\n\r\nまたほら君と話そうか\r\n\r\nあれだってこれだって\r\n\r\n今すぐ気付いてくれ\r\n\r\n僕は君の惑星\r\n\r\n回り続けて\r\n\r\nいつも君のそばで\r\n\r\n黒点数えてたけれど\r\n\r\nサヨナラなんてないよ\r\n\r\n今日から軌道を外れんだ\r\n\r\n最後まで見送ってよ\r\n\r\n永遠に離れてくんだ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ\r\n\r\nどうなってこうなって\r\n\r\n結局独り佇んで\r\n\r\n失って勘づいて\r\n\r\n今さら戻れやしない\r\n\r\n君のいない場所で\r\n\r\n途方に暮れて\r\n\r\nもう一度引力を\r\n\r\n感じたかったんだけれど\r\n\r\n神様なんていないよ\r\n\r\nいつまで待っても巡回中\r\n\r\n選ばれない悲しみを\r\n\r\n何度でも噛みしめるんだ\r\n\r\nああ ああ\r\n\r\n君は僕の太陽\r\n\r\n全てを燃やしたけれど\r\n\r\nサヨナラなんてないよ\r\n\r\n今日から軌道を外れんだ\r\n\r\n最後まで見送ってよ\r\n\r\n永遠に離れてくんだ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ\r\n\r\nラララ ララララララ\r\n\r\nラララ ラララララ', '04:03', '1', '5');
INSERT INTO `songs` VALUES ('6', 'Take Me Hand', '和风', '6', '6', '/mp3/DAISHI DANCE - Take Me Hand.mp3', '/pic/f1b83eae-7cb2-43bd-8710-f7ce853d28b9.jpg', null, 'Take Me Hand (抓紧我的手) - DAISHI DANCE/Cécile Corbel (赛西儿·柯贝尔)\r\n\r\nWritten by：Cecile Corbel/Tomoharu Moriya\r\n\r\nIn my dreams\r\n\r\nI feel your light\r\n\r\nI feel love is born again\r\n\r\nFireflies\r\n\r\nIn the moonlight\r\n\r\nRising stars\r\n\r\nRemember\r\n\r\nThe day\r\n\r\nI fell in love with you\r\n\r\nDarling won\'t you break\r\n\r\nMy heart\r\n\r\nTake my hand now\r\n\r\nStay close to me\r\n\r\nBe my lover\r\n\r\nWon\'t you let me go\r\n\r\nClose your eyes now\r\n\r\nAnd you will see\r\n\r\nThere\'s a rainbow\r\n\r\nFor you and me\r\n\r\nAs I wake up\r\n\r\nI see your face\r\n\r\nI feel love is born again\r\n\r\nCherry blossom\r\n\r\nFlying birds\r\n\r\nIn the sky\r\n\r\nCan\'t you see\r\n\r\nThe sun\r\n\r\nThat is shining on the fields\r\n\r\nIs it shining in\r\n\r\nYour heart\r\n\r\nTake my hand now\r\n\r\nStay close to me\r\n\r\nBe my lover\r\n\r\nWon\'t you let me go\r\n\r\nClose your eyes now\r\n\r\nAnd you will see\r\n\r\nThere\'s a rainbow\r\n\r\nFor you and me\r\n\r\nAnd I dream of you\r\n\r\nEvery night\r\n\r\nCause\'s there only you\r\n\r\nIn my mind\r\n\r\nWill you be\r\n\r\nA stranger or a friend in my life\r\n\r\nDarling won\'t you break\r\n\r\nMy heart\r\n\r\nTake my hand now\r\n\r\nStay close to me\r\n\r\nBe my lover\r\n\r\nWon\'t you let me go\r\n\r\nClose your eyes now\r\n\r\nAnd you will see\r\n\r\nThere\'s a rainbow\r\n\r\nFor you and me\r\n\r\nTake my hand now\r\n\r\nStay close to me\r\n\r\nBe my lover\r\n\r\nWon\'t you let me go\r\n\r\nClose your eyes now\r\n\r\nAnd you will see\r\n\r\nThere\'s a rainbow\r\n\r\nFor you and me\r\n\r\nTake my hand now\r\n\r\nStay close to me\r\n\r\nBe my lover\r\n\r\nWon\'t you let me go\r\n\r\nClose your eyes now\r\n\r\nAnd you will see\r\n\r\nThere\'s a rainbow\r\n\r\nFor you and me', '04:20', '1', '6');
INSERT INTO `songs` VALUES ('7', 'Star Sky', '纯音乐,摇滚', '7', '7', '/mp3/Two Steps From Hell - Star Sky.mp3', '/pic/39829be4-7524-492e-82c5-48e2e1f57f86.jpg', null, 'Star Sky (星空) - Two Steps From Hell (两步逃离地狱)\r\n\r\nHere we are\r\n\r\nRiding the sky\r\n\r\nPainting the night with sun\r\n\r\nYou and I mirrors of light\r\n\r\nTwin flames of fire\r\n\r\nLit in another time and place\r\n\r\nI knew your name\r\n\r\nI knew your face\r\n\r\nYour love and grace\r\n\r\nPast and present now embrace\r\n\r\nWorlds collide in inner space\r\n\r\nUnstoppable the song we play\r\n\r\nBurn the page for me\r\n\r\nI cannot erase the time of sleep\r\n\r\nI cannot be loved so set me free\r\n\r\nI cannot deliver your love\r\n\r\nOr caress your soul so\r\n\r\nTurn that page for me\r\n\r\nI cannot embrace the touch that you give\r\n\r\nI cannot find solice in your words\r\n\r\nI cannot deliver you your love\r\n\r\nOr caress your soul\r\n\r\nAge to age\r\n\r\nI feel the call\r\n\r\nMemory of future dreams\r\n\r\nYou and I riding the sky\r\n\r\nKeeping the fire bright\r\n\r\nFrom another time and place\r\n\r\nI know your name\r\n\r\nI know your face\r\n\r\nYour touch and grace\r\n\r\nAll of time can not erase\r\n\r\nWhat our hearts remember stays\r\n\r\nForever on a song we play\r\n\r\nBurn the page for me\r\n\r\nI cannot erase the time of sleep\r\n\r\nI cannot be loved so set me free\r\n\r\nI cannot deliver your love\r\n\r\nOr caress your soul so\r\n\r\nTurn that page for me\r\n\r\nI cannot embrace the touch that you give\r\n\r\nI cannot find solice in your words\r\n\r\nI cannot deliver you your love\r\n\r\nOr caress your soul', '05:30', '1', '7');
INSERT INTO `songs` VALUES ('8', 'The Spectre', '电音', '8', '2', '/mp3/Alan Walker - The Spectre.mp3', '/pic/09845f3a-b7af-467c-b4b3-262873ec834b.jpg', null, 'The Spectre - Alan Walker\r\n\r\nWritten by：Anders Froen/Alan Walker/Jesper Borgen/Lars Kristian Rosness/Marcus Arnbekk/Gunnar Greve/Tommy LaVerdi\r\n\r\nHello hello\r\n\r\nCan you hear me as I scream your name\r\n\r\nHello hello\r\n\r\nDo you meet me before I fade away\r\n\r\nIs this a place that I call home\r\n\r\nTo find what I\'ve become\r\n\r\nWalk along the path unknown\r\n\r\nWe live we love we lie\r\n\r\nDeep in the dark I don\'t need the light\r\n\r\nThere is a ghost inside me\r\n\r\nIt all belongs to the other side\r\n\r\nWe live we love we lie\r\n\r\nWe live we love we lie\r\n\r\nHello hello\r\n\r\nNice to meet you\r\n\r\nVoices inside my head\r\n\r\nHello hello\r\n\r\nI believe you\r\n\r\nHow can I forget\r\n\r\nIs this a place that I call home\r\n\r\nTo find what I\'ve become\r\n\r\nWalk along the path unknown\r\n\r\nWe live we love we lie\r\n\r\nDeep in the dark I don\'t need the light\r\n\r\nThere is a ghost inside me\r\n\r\nIt all belongs to the other side\r\n\r\nWe live we love we lie\r\n\r\nWe live we love we lie\r\n\r\nWe live we love we lie', '03:13', '1', '8');
INSERT INTO `songs` VALUES ('9', '风的季节', '流行', '32', '8', '/mp3/亮声open - 风的季节.mp3', '/pic/c94791c5-2305-432e-b066-cd5e0e932851.jpg', null, '风的季节 - 亮声open\r\n\r\n凉风轻轻吹到悄然进了我衣襟\r\n\r\n夏天偷去听不见声音\r\n\r\n日子匆匆走过倍令我有百感生\r\n\r\n记挂那一片景象缤纷\r\n\r\n随风轻轻吹到你步进了我的心\r\n\r\n在一息间改变我一生\r\n\r\n付出多少热诚也没法去计得真\r\n\r\n却也不需再惊惧风雨侵\r\n\r\n吹呀吹让这风吹\r\n\r\n抹干眼眸里亮晶的眼泪\r\n\r\n吹呀吹让这风吹\r\n\r\n哀伤通通带走管风里是谁\r\n\r\n从风沙初起想到是季节变更\r\n\r\n梦中醒却岁月如飞奔\r\n\r\n是否早订下来你或我也会变心\r\n\r\n慨叹怎么会久合终要分\r\n\r\n狂风吹得起劲朗日也要被蔽隐\r\n\r\n泛起一片迷朦尘埃滚\r\n\r\n掠走心里一切美梦带去那欢欣\r\n\r\n带去我的爱只是独留恨\r\n\r\n吹呀吹让这风吹\r\n\r\n抹干眼眸里亮晶的眼泪\r\n\r\n吹呀吹让这风吹\r\n\r\n哀伤通通带走\r\n\r\n吹呀吹让这风吹\r\n\r\n抹干眼眸里亮晶的眼泪\r\n\r\n吹呀吹让这风吹\r\n\r\n哀伤通通带走管风里是谁', '03:27', '1', '9');
INSERT INTO `songs` VALUES ('10', 'なんでもないや', '流行', '9', '9', '/mp3/RADWIMPS (ラッドウィンプス) - なんでもないや (没什么大不了) (Movie ver.).mp3', '/pic/aa6ec8b3-40f2-4ad3-b212-36248eab7677.jpg', null, 'なんでもないや (没什么大不了) (Movie ver.) (《你的名字。》动画电影片尾曲) - RADWIMPS (ラッドウィンプス)\r\n\r\n词：野田洋次郎\r\n\r\n曲：野田洋次郎\r\n\r\n二人の間\r\n\r\n通り過ぎた風は\r\n\r\nどこから寂しさを\r\n\r\n運んできたの\r\n\r\n泣いたりした\r\n\r\nそのあとの空は\r\n\r\nやけに\r\n\r\n透き通っていたりしたんだ\r\n\r\nいつもは尖ってた父の言葉が\r\n\r\n今日は暖かく感じました\r\n\r\n優しさも笑顔も夢の語り方も\r\n\r\n知らなくて全部\r\n\r\n君を真似たよ\r\n\r\nもう少しだけでいい\r\n\r\nあと少しだけでいい\r\n\r\nもう少しだけでいいから\r\n\r\nもう少しだけでいい\r\n\r\nあと少しだけでいい\r\n\r\nもう少しだけ\r\n\r\nくっついていようか\r\n\r\n僕らタイムフライヤー\r\n\r\n時を駆け上がるクライマー\r\n\r\n時のかくれんぼ\r\n\r\nはぐれっこはもういやなんだ\r\n\r\n嬉しくて泣くのは\r\n\r\n悲しくて笑うのは\r\n\r\n君の心が\r\n\r\n君を追い越したんだよ\r\n\r\n星にまで願って\r\n\r\n手にいれたオモチャも\r\n\r\n部屋の隅っこに今\r\n\r\n転がってる\r\n\r\n叶えたい夢も\r\n\r\n今日で100個できたよ\r\n\r\nたった一つといつか\r\n\r\n交換こしよう\r\n\r\nいつもは喋らない\r\n\r\nあの子に今日は\r\n\r\n放課後「また明日」と\r\n\r\n声をかけた\r\n\r\n慣れないことも\r\n\r\nたまにならいいね\r\n\r\n特にあなたが\r\n\r\n隣にいたら\r\n\r\nもう少しだけでいい\r\n\r\nあと少しだけでいい\r\n\r\nもう少しだけでいいから\r\n\r\nもう少しだけでいい\r\n\r\nあと少しだけでいい\r\n\r\nもう少しだけくっついていようよ\r\n\r\n僕らタイムフライヤー\r\n\r\n君を知っていたんだ\r\n\r\n僕が僕の名前を\r\n\r\n覚えるよりずっと前に\r\n\r\n君のいない世界にも\r\n\r\n何かの意味はきっとあって\r\n\r\nでも君のいない世界など\r\n\r\n夏休みのない八月のよう\r\n\r\n君のいない世界など\r\n\r\n笑うことないサンタのよう\r\n\r\n君のいない世界など\r\n\r\n僕らタイムフライヤー\r\n\r\n時を駆け上がるクライマー\r\n\r\n時のかくれんぼ\r\n\r\nはぐれっこはもういやなんだ\r\n\r\nなんでもないや\r\n\r\nやっぱりなんでもないや\r\n\r\n今から行くよ\r\n\r\n僕らタイムフライヤー\r\n\r\n時を駆け上がるクライマー\r\n\r\n時のかくれんぼ\r\n\r\nはぐれっこはもういいよ\r\n\r\n君は派手なクライヤー\r\n\r\nその涙止めてみたいな\r\n\r\nだけど君は拒んだ\r\n\r\n零れるままの涙を\r\n\r\n見てわかった\r\n\r\n嬉しくて泣くのは\r\n\r\n悲しくて笑うのは\r\n\r\n僕の心が\r\n\r\n僕を追い越したんだよ', '05:44', '1', '10');
INSERT INTO `songs` VALUES ('11', '玻璃之情', '流行', '10', '10', '/mp3/张国荣 - 玻璃之情.mp3', '/pic/eab6584a-3a7c-449a-8b43-53af0bc55c60.jpg', null, '玻璃之情 - 张国荣 (Leslie Cheung)\r\n\r\n词：林夕\r\n\r\n曲：张国荣\r\n\r\n从前我会使你快乐\r\n\r\n现在却最多叫你寂寞\r\n\r\n再吻下去\r\n\r\n像皱纸轻薄\r\n\r\n撕开了 都不觉\r\n\r\n我这苦心\r\n\r\n已有预备\r\n\r\n随时有块玻璃 破碎堕地\r\n\r\n勉强下去 我会憎你\r\n\r\n只差那一口气\r\n\r\n不信眼泪\r\n\r\n能令失落的你爱下去\r\n\r\n难收的覆水\r\n\r\n将感情漫漫荡开去\r\n\r\n如果你太累\r\n\r\n及时的道别没有罪\r\n\r\n牵手来 空手去 就去\r\n\r\n我这苦心 已有预备\r\n\r\n随时有块玻璃\r\n\r\n破碎堕地\r\n\r\n勉强下去我会憎你\r\n\r\n只差那一口气\r\n\r\n不信眼泪\r\n\r\n能令失落的你爱下去\r\n\r\n难收的覆水\r\n\r\n将感情漫漫荡开去\r\n\r\n如果你太累\r\n\r\n及时的道别没有罪\r\n\r\n牵手来 空手去\r\n\r\n一起要许多福气\r\n\r\n或者承受不起\r\n\r\n或者怀恨比相爱更合理\r\n\r\n即使 可悲\r\n\r\n不信眼泪\r\n\r\n能令失落的你爱下去\r\n\r\n难收的覆水\r\n\r\n将感情漫漫荡开去\r\n\r\n如果你太累\r\n\r\n及时地道别没有罪\r\n\r\n一生人 不只一伴侣\r\n\r\n你会记得 我是谁\r\n\r\n犹如偶尔想起过气玩具\r\n\r\n我抱住过哪怕失去\r\n\r\n早想到玻璃很 易碎', '04:45', '1', '11000');
INSERT INTO `songs` VALUES ('12', '清平调', '古典', '11', '11', '/mp3/王菲 - 清平调.mp3', '/pic/b4e41f6b-5182-48c9-a5fb-ee8d472da390.jpg', null, '清平调 - 王菲 (Faye Wong)/邓丽君 (Teresa Teng)\r\n\r\n词：李白\r\n\r\n曲：曹俊鸿\r\n\r\n邓：云想衣裳花想容\r\n\r\n春风拂槛露华浓\r\n\r\n若非群玉山头见\r\n\r\n会向瑶台月下逢\r\n\r\n一枝红艳露凝香\r\n\r\n云雨巫山枉断肠\r\n\r\n借问汉宫谁得似\r\n\r\n可怜飞燕倚新妆\r\n\r\n名花倾国两相欢\r\n\r\n常得君王带笑看\r\n\r\n解释春风无限恨\r\n\r\n沉香亭北倚阑干\r\n\r\n解释春风无限恨\r\n\r\n沉香亭北倚阑干\r\n\r\n王：云想衣裳花想容\r\n\r\n春风拂槛露华浓\r\n\r\n若非群玉山头见\r\n\r\n会向瑶台月下逢\r\n\r\n一枝红艳露凝香\r\n\r\n云雨巫山枉断肠\r\n\r\n借问汉宫谁得似\r\n\r\n可怜飞燕倚新妆\r\n\r\n名花倾国两相欢\r\n\r\n常得君王带笑看\r\n\r\n解释春风无限恨\r\n\r\n沉香亭北倚阑干\r\n\r\n解释春风无限恨\r\n\r\n沉香亭北倚阑干\r\n\r\n解释春风无限恨\r\n\r\n沉香亭北倚阑干', '04:32', '1', '120');
INSERT INTO `songs` VALUES ('13', '起风了', '流行', '30', '12', '/mp3/买辣椒也用券 - 起风了.mp3', '/pic/3e87eabd-b8df-4b09-9c8d-ffa6d1f9e44d.jpg', null, '起风了 (Cover 高桥优-《ヤキモチ》) - 买辣椒也用券\r\n\r\n词：米果\r\n\r\n曲：高桥优\r\n\r\n这一路上走走停停\r\n\r\n顺着少年漂流的痕迹\r\n\r\n迈出车站的前一刻\r\n\r\n竟有些犹豫\r\n\r\n不禁笑这近乡情怯\r\n\r\n仍无可避免\r\n\r\n而长野的天\r\n\r\n依旧那么暖\r\n\r\n吹起了从前\r\n\r\n从前初识这世间\r\n\r\n万般流连\r\n\r\n看着天边似在眼前\r\n\r\n也甘愿赴汤蹈火去走它一遍\r\n\r\n如今走过这世间\r\n\r\n万般流连\r\n\r\n翻过岁月不同侧脸\r\n\r\n措不及防闯入你的笑颜\r\n\r\n我曾难自拔于世界之大\r\n\r\n也沉溺于其中梦话\r\n\r\n不得真假 不做挣扎 不惧笑话\r\n\r\n我曾将青春翻涌成她\r\n\r\n也曾指尖弹出盛夏\r\n\r\n心之所动 且就随缘去吧\r\n\r\n逆着光行走 任风吹雨打\r\n\r\n短短的路走走停停\r\n\r\n也有了几分的距离\r\n\r\n不知抚摸的是故事 还是段心情\r\n\r\n也许期待的不过是 与时间为敌\r\n\r\n再次看到你\r\n\r\n微凉晨光里\r\n\r\n笑的很甜蜜\r\n\r\n从前初识这世间\r\n\r\n万般流连\r\n\r\n看着天边似在眼前\r\n\r\n也甘愿赴汤蹈火去走它一遍\r\n\r\n如今走过这世间\r\n\r\n万般流连\r\n\r\n翻过岁月不同侧脸\r\n\r\n措不及防闯入你的笑颜\r\n\r\n我曾难自拔于世界之大\r\n\r\n也沉溺于其中梦话\r\n\r\n不得真假 不做挣扎 不惧笑话\r\n\r\n我曾将青春翻涌成她\r\n\r\n也曾指尖弹出盛夏\r\n\r\n心之所动 且就随缘去吧\r\n\r\n晚风吹起你鬓间的白发\r\n\r\n抚平回忆留下的疤\r\n\r\n你的眼中 明暗交杂 一笑生花\r\n\r\n暮色遮住你蹒跚的步伐\r\n\r\n走进床头藏起的画\r\n\r\n画中的你 低着头说话\r\n\r\n我仍感叹于世界之大\r\n\r\n也沉醉于儿时情话\r\n\r\n不剩真假 不做挣扎 无谓笑话\r\n\r\n我终将青春还给了她\r\n\r\n连同指尖弹出的盛夏\r\n\r\n心之所动 就随风去了\r\n\r\n以爱之名 你还愿意吗', '05:23', '1', '1200');
INSERT INTO `songs` VALUES ('14', 'test', 'pop', '21', '31', null, '/pic/81e8cba9-aec0-4202-b4db-892b6d23f633.jpg', null, '无歌词', '01:11', '0', '800');
INSERT INTO `songs` VALUES ('15', 'Red', 'Blues', '27', '32', '/mp3/Taylor Swift - Red.mp3', '/pic/e1264c0b-545a-488f-a7fa-f941b8bf532e.jpg', null, 'Red (红) - Taylor Swift (泰勒·斯威夫特)\r\n\r\nWritten by：Taylor Swift\r\n\r\nLoving him is like driving a new Maserati down a dead end street\r\n\r\nFaster than the wind passionate as sin ending so suddenly\r\n\r\nLoving him is like trying to change your mind\r\n\r\nOnce you\'re already flying through the free fall\r\n\r\nLike the colors in autumn so bright just before they lose it all\r\n\r\nLosing him was blue like I\'ve never known\r\n\r\nMissing him was dark grey all alone\r\n\r\nForgetting him was like trying to know somebody you never met\r\n\r\nBut loving him was red\r\n\r\nLoving him was red\r\n\r\nTouching him was like realizing\r\n\r\nAll you ever wanted was right there in front of you\r\n\r\nMemorizing him was as easy as knowing all the words to your old favorite song\r\n\r\nFighting with him was like trying to solve a crossword\r\n\r\nAnd realizing there\'s no right answer\r\n\r\nRegretting him was like wishing you\'d never found out that love could be that strong\r\n\r\nLosing him was blue like I\'ve never known\r\n\r\nMissing him was dark grey all alone\r\n\r\nForgetting him was like trying to know somebody you never met\r\n\r\nBut loving him was red\r\n\r\nOh red\r\n\r\nBurning red\r\n\r\nRemembering him comes in flashbacks in echoes\r\n\r\nTell myself it\'s time now gotta let go\r\n\r\nBut moving on from him is impossible when I still see it all in my head\r\n\r\nBurning red\r\n\r\nLoving him was red\r\n\r\nOh losing him was blue like I\'ve never known\r\n\r\nMissing him was dark grey all alone\r\n\r\nForgetting him was like trying to know somebody you never met\r\n\r\nCause loving him was red\r\n\r\nYeah yeah red\r\n\r\nBurning red\r\n\r\nAnd that\'s why he\'s spinning round in my head\r\n\r\nComes back to me burning red\r\n\r\nYeah yeah\r\n\r\nHis love was like driving a new Maserati down a dead end street', '03:43', '1', '400000');
INSERT INTO `songs` VALUES ('16', '处处吻', 'Blues', '33', '33', '/mp3/杨千嬅 - 处处吻.mp3', '/pic/ca8365ae-f5e7-4f81-8522-429969320032.jpg', null, '处处吻 (Kissing everywhere) (Budweiser American Kiss 广告主题曲) - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：雷颂德\r\n\r\n你爱热吻却永不爱人\r\n\r\n练习为乐但是怕熟人\r\n\r\n你爱路过去索取见闻\r\n\r\n陌路人变得必有份好感\r\n\r\n你热爱别离\r\n\r\n再合再离\r\n\r\n似花瓣献技\r\n\r\n叫花粉遍地 噢噢\r\n\r\n你在播弄这穿线游戏\r\n\r\n跟他结束\r\n\r\n他与她再一起\r\n\r\n你小心\r\n\r\n一吻便颠倒众生\r\n\r\n一吻便救一个人\r\n\r\n给你拯救的体温\r\n\r\n总会再捐给某人\r\n\r\n一吻便偷一个心\r\n\r\n一吻便杀一个人\r\n\r\n一寸吻感一寸金\r\n\r\n一脸崎岖的旅行\r\n\r\n让半夜情人\r\n\r\n延续吻别人\r\n\r\n让你旧情人\r\n\r\n又惠顾他人\r\n\r\n每晚大概有上亿个人\r\n\r\n在地球上落力的亲吻\r\n\r\n你那习惯散播给众人\r\n\r\n在地球上惠泽遍及世人\r\n\r\n你热爱别离\r\n\r\n再合再离\r\n\r\n似花瓣献技\r\n\r\n叫花粉遍地 噢噢\r\n\r\n你在播弄这穿线游戏\r\n\r\n跟他结束\r\n\r\n她与他再一起\r\n\r\n你小心 一吻便颠倒众生\r\n\r\n一吻便救一个人\r\n\r\n给你拯救的体温\r\n\r\n总会再捐给某人\r\n\r\n一吻便偷一个心\r\n\r\n一吻便杀一个人\r\n\r\n一寸吻感一寸金\r\n\r\n一脸崎岖的旅行\r\n\r\n你为何未曾尽兴\r\n\r\n这塑胶的爱情\r\n\r\n跳蚤的旅程\r\n\r\n延展偷天盖地\r\n\r\n好本领\r\n\r\n这吊诡的爱情\r\n\r\n播种的旅程\r\n\r\n别了她 她吻他\r\n\r\n他吻她吻他吻她\r\n\r\n延续愉快过程\r\n\r\n你我他真高兴\r\n\r\n下个她 她吻他\r\n\r\n他吻她再亲你结束这旅程\r\n\r\n多得你这煞星\r\n\r\n你小心\r\n\r\n一吻便颠倒众生\r\n\r\n一吻便救一个人\r\n\r\n给你拯救的体温\r\n\r\n总会再捐给某人\r\n\r\n一吻便偷一个心\r\n\r\n一吻便杀一个人\r\n\r\n一寸吻感一寸金\r\n\r\n一脸崎岖的旅行\r\n\r\n别了她 她吻他\r\n\r\n他吻她吻他吻她\r\n\r\n延续愉快过程\r\n\r\n你我他真高兴\r\n\r\n下个她\r\n\r\n她吻他\r\n\r\n他吻她吻他吻她\r\n\r\n延续愉快过程\r\n\r\n你我他真高兴\r\n\r\n十个她 千个她\r\n\r\n恩爱扩展的旅程', '03:19', '1', '0');
INSERT INTO `songs` VALUES ('17', '电光幻影', 'Blues', '33', '33', '/mp3/杨千嬅 - 电光幻影.mp3', '/pic/c192c901-5cdf-48d0-95a2-743cadd7fea8.jpg', null, '电光幻影 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：于逸尧\r\n\r\n扑面而过\r\n\r\n如难过眼泪淋熄兴奋烈火\r\n\r\n如肥皂在肌肤上偶遇而破\r\n\r\n如时间战胜了的一声苦情歌\r\n\r\n本我到无我\r\n\r\n天涯明灭忽尔没痛楚\r\n\r\n我或忘我\r\n\r\n如微雨放低水花花向上舞的因果\r\n\r\n梦想颠倒 一切在我\r\n\r\n得所以失 执于对便错\r\n\r\n人存在只想为了求证\r\n\r\n曾留下追忆里的情景\r\n\r\n但万法好比电光的幻影\r\n\r\n入静了心境挂念难道靠眼睛\r\n\r\n而遗憾都只为了求证\r\n\r\n最看不开的竟然是感情 感情\r\n\r\n爱恨无常 雪落无声\r\n\r\n色不过色 却碍了空性\r\n\r\n我或无我\r\n\r\n唯时间会去解释苦恼亦要我许可\r\n\r\n恨只因爱 因爱及怖\r\n\r\n歌者与歌 终须要掠过\r\n\r\n人存在只想为了求证\r\n\r\n曾留下追忆里的情景\r\n\r\n但万法好比电光的幻影\r\n\r\n入静了心境挂念难道靠眼睛\r\n\r\n而遗憾都只为了求证\r\n\r\n最看不开的竟然是感情 感情\r\n\r\n爱恨无常 雪落无声\r\n\r\n色不过色 却碍了空性', '03:35', '1', '0');
INSERT INTO `songs` VALUES ('18', '假如让我说下去', 'Blues', '33', '33', '/mp3/杨千嬅 - 假如让我说下去.mp3', '/pic/259d15e1-21b6-41e6-a5f6-cd608d788f5f.png', null, '假如让我说下去 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕/于逸尧\r\n\r\n曲：林夕/于逸尧\r\n\r\n任我想 我最多想一觉睡去\r\n\r\n期待你 也至少劝我别劳累\r\n\r\n但我把 谈情的气力转赠谁\r\n\r\n跟你电话之中讲再会 再会谁\r\n\r\n暴雨天 我至少想讲挂念你\r\n\r\n然后你 你最多会笑着回避\r\n\r\n避到底 明明不筋竭都力疲\r\n\r\n就当我还未放松自己\r\n\r\n我想哭 你可不可以暂时别要睡\r\n\r\n陪着我 像最初相识我当时未怕累\r\n\r\n但如果 但如果说下去 或者 傻得我\r\n\r\n彼此怎能爱下去\r\n\r\n暴雨中 我到底怎么要害怕\r\n\r\n难道你 无台风会决定留下\r\n\r\n但我想 如楼底这夜倒下来 就算临别亦有通电话\r\n\r\n我怕死 你可不可以暂时别要睡\r\n\r\n陪着我 让我可以不靠安眠药进睡\r\n\r\n但如果 但如果说下去 亦无非逼你\r\n\r\n壹句话 如今跟某位同居\r\n\r\n我的天 你可不可以暂时让我睡\r\n\r\n忘掉爱 尚有多少工作失眠亦有罪\r\n\r\n但如果 但如果怨下去 或者 傻得我\r\n\r\n通宵找谁接下去\r\n\r\n离开 不应再打搅爱人 对不对', '03:27', '1', '0');
INSERT INTO `songs` VALUES ('19', '烈女', 'Blues', '33', '33', '/mp3/杨千嬅 - 烈女.mp3', '/pic/083967c4-8883-4e57-ad9a-2114be211aa6.jpg', null, '烈女 (Strong woman) - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：雷颂德\r\n\r\n编曲：雷颂德\r\n\r\n很想装作我没有灵魂\r\n\r\n但你赞我性感\r\n\r\n很想偷呃拐骗的勾引\r\n\r\n完了事便怀孕\r\n\r\n然后便跟你 跟你到家里去扫地\r\n\r\n让情敌跟我讲恭喜 放弃是与非\r\n\r\n与魔鬼在一起\r\n\r\n烈女不怕死 但凭傲气\r\n\r\n绝没有必要呵你似歌姬\r\n\r\n知你好过了便要分离\r\n\r\n没有骨气只会变奸妃\r\n\r\n烈女不怕死 又何惧你\r\n\r\n不会失去血性和品味\r\n\r\n知你一向以我去摄期\r\n\r\n迎合你便令名誉扫地 呸呸\r\n\r\n本应想我变做你类型\r\n\r\n让你与我有景\r\n\r\n只可惜得到你的尊敬\r\n\r\n全因肯当布景\r\n\r\n无谓被选美\r\n\r\n逼你待我好我宁愿伤悲\r\n\r\n若然排在榜中最多三四\r\n\r\n我纵像储妃\r\n\r\n违背了我天地\r\n\r\n烈女不怕死 但凭傲气\r\n\r\n绝没有必要呵你似歌姬\r\n\r\n知你好过了便要分离\r\n\r\n没有骨气只会变奸妃\r\n\r\n烈女不怕死 又何惧你\r\n\r\n不会失去血性和品味\r\n\r\n知你一向以我去摄期\r\n\r\n迎合你便令名誉扫地\r\n\r\n烈女不怕死 但凭傲气\r\n\r\n绝没有必要呵你似歌姬\r\n\r\n知你好过了便要分离\r\n\r\n没有骨气只会变奸妃\r\n\r\n烈女不怕死 又何惧你\r\n\r\n不会失去血性和品味\r\n\r\n知你一向以我去摄期\r\n\r\n迎合你便令名誉扫地 呸呸', '03:56', '1', '0');
INSERT INTO `songs` VALUES ('22', '少女的祈祷', 'Blues', '33', '33', '/mp3/杨千嬅 - 少女的祈祷.mp3', '/pic/e4b8cc58-9930-44cd-ad71-5f4539993b79.png', null, '少女的祈祷 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：Badarzewska\r\n\r\n编曲：陈辉阳\r\n\r\n沿途与他车厢中私奔般恋爱\r\n\r\n再挤逼都不放开\r\n\r\n祈求在路上没任何的阻碍\r\n\r\n令愉快旅程变悲哀\r\n\r\n连气两次绿灯都过渡了\r\n\r\n与他再爱几公里\r\n\r\n当这盏灯转红便会别离\r\n\r\n凭运气决定我生死\r\n\r\n祈求天地放过一双恋人\r\n\r\n怕发生的永远别发生\r\n\r\n从来未顺利遇上好景降临\r\n\r\n如何能重拾信心\r\n\r\n祈求天父做十分钟好人\r\n\r\n赐我他的吻\r\n\r\n如怜悯罪人\r\n\r\n我爱主\r\n\r\n同时亦爱一位世人\r\n\r\n祈求沿途未变心\r\n\r\n请给我护荫\r\n\r\n为了他\r\n\r\n不懂祷告\r\n\r\n都敢祷告\r\n\r\n谁愿眷顾\r\n\r\n这种信徒\r\n\r\n用两手遮掩双眼专心倾诉\r\n\r\n宁愿答案\r\n\r\n望不到\r\n\r\n唯求与他车厢中可抵达未来\r\n\r\n到车毁都不放开\r\n\r\n无论路上历尽任何的伤害\r\n\r\n任由我决定爱不爱\r\n\r\n祈求天地放过一双恋人\r\n\r\n怕发生的永远别发生\r\n\r\n从来未顺利遇上好景降临\r\n\r\n如何能重拾信心\r\n\r\n祈求天父做十分钟好人\r\n\r\n赐我他的吻\r\n\r\n如怜悯罪人\r\n\r\n我爱主\r\n\r\n同时亦爱一位爱人\r\n\r\n祈求沿途未变心\r\n\r\n请给我护荫\r\n\r\n为了他\r\n\r\n不懂祷告\r\n\r\n都敢祷告\r\n\r\n谁愿眷顾\r\n\r\n这种信徒\r\n\r\n太爱他怎么想到这么恐布\r\n\r\n对绿灯\r\n\r\n去哀求哭诉\r\n\r\n然而天父并未体恤好人\r\n\r\n到我睁开眼\r\n\r\n无明灯指引\r\n\r\n我爱主\r\n\r\n为何任我身边爱人\r\n\r\n离弃了我下了车\r\n\r\n你怎可答允', '03:31', '1', '0');
INSERT INTO `songs` VALUES ('23', '小城大事', 'Blues', '33', '33', '/mp3/杨千嬅 - 小城大事.mp3', '/pic/1b2c1c8c-efe1-4f7a-8824-650b273898fd.jpg', null, '小城大事 (Small town event) - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：雷颂德\r\n\r\n青春仿佛因我爱你开始\r\n\r\n但却令我看破爱这个字\r\n\r\n自你患上失忆\r\n\r\n便是我扭转命数的事\r\n\r\n只因当失忆症发作加深\r\n\r\n没记住我但却另有更新蜜运\r\n\r\n像狐狸精般\r\n\r\n并未允许我步近\r\n\r\n无回忆的余生\r\n\r\n忘掉往日情人\r\n\r\n却又记住移情别爱的命运\r\n\r\n无回忆的男人\r\n\r\n就当偷厄与瞒骗\r\n\r\n抱抱我不过份\r\n\r\n吻下来豁出去\r\n\r\n这吻别似覆水\r\n\r\n再来也许要天上团聚\r\n\r\n再回头你不许\r\n\r\n如曾经不登对\r\n\r\n你何以双眼好像流泪\r\n\r\n彼此追忆不怕爱要终止\r\n\r\n但我大概上世做过太多坏事\r\n\r\n能从头开始\r\n\r\n跪在教堂说愿意\r\n\r\n娱乐行的人影\r\n\r\n还在继续繁荣\r\n\r\n我在算着甜言蜜语的寿命\r\n\r\n人造的蠢卫星\r\n\r\n没探测出我们已\r\n\r\n已再见不再认\r\n\r\n吻下来豁出去\r\n\r\n这吻别似覆水\r\n\r\n再来也许要天上团聚\r\n\r\n我下来你出去\r\n\r\n讲再会也心虚\r\n\r\n我还记得到天上团聚\r\n\r\n吻下来豁出去\r\n\r\n从前多么登对\r\n\r\n你何以双眼好像流泪\r\n\r\n每年这天记得再流泪', '03:28', '1', '0');
INSERT INTO `songs` VALUES ('24', '野孩子', 'Blues', '33', '33', '/mp3/杨千嬅 - 野孩子.mp3', '/pic/afa5dda7-4ee5-4521-8cf8-4fa8dbf303c7.jpg', null, '野孩子 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：黄伟文\r\n\r\n曲：雷颂德\r\n\r\n就算只谈一场感情\r\n\r\n除外都是一时虚荣\r\n\r\n不等于在蜜月套房游玩过\r\n\r\n就可自入自出仙境\r\n\r\n情愿获得你的尊敬\r\n\r\n承受太高傲的罪名\r\n\r\n挤得进你臂弯 如情怀渐冷\r\n\r\n未算孤苦也伶仃\r\n\r\n明知爱 这种男孩子\r\n\r\n也许只能如此\r\n\r\n但我会成为你\r\n\r\n最牵挂的一个女子\r\n\r\n朝朝暮暮让你\r\n\r\n猜想如何驯服我\r\n\r\n若果亲手抱住\r\n\r\n或者不必如此\r\n\r\n许多旁人说我\r\n\r\n不太明了男孩子\r\n\r\n不受命令就是\r\n\r\n一种最坏名字\r\n\r\n笑我这个毫无办法\r\n\r\n管束的野孩子\r\n\r\n连没有幸福都不介意\r\n\r\n若我依然坚持忠诚\r\n\r\n难道你又适合安定\r\n\r\n真可惜 说要吻我的还未吻\r\n\r\n自己就自梦中苏醒\r\n\r\n离场是否有点失敬\r\n\r\n还是更轰烈的剧情\r\n\r\n必需有这结果\r\n\r\n才能怀念我\r\n\r\n让我于荒野驰骋\r\n\r\n明知\r\n\r\n爱这种男孩子\r\n\r\n也许只能如此\r\n\r\n但我会成为\r\n\r\n你最牵挂的一个女子\r\n\r\n朝朝暮暮\r\n\r\n让你猜想如何驯服我\r\n\r\n若果亲手抱住\r\n\r\n或者不必如此\r\n\r\n许多旁人\r\n\r\n说我不太明了男孩子\r\n\r\n不受命令\r\n\r\n就是一种最坏名字\r\n\r\n笑我这个毫无办法\r\n\r\n管束的野孩子\r\n\r\n连没有幸福都不介意\r\n\r\n明知爱 这种男孩子\r\n\r\n也许只能如此\r\n\r\n但我会成为\r\n\r\n你最牵挂的一个女子\r\n\r\n朝朝暮暮\r\n\r\n让你猜想如何驯服我\r\n\r\n若果亲手抱住\r\n\r\n或者不必如此\r\n\r\n许多旁人\r\n\r\n说我不太明了男孩子\r\n\r\n不受命令\r\n\r\n就是一种最坏名字\r\n\r\n我也笑我原来\r\n\r\n是个天生的野孩子\r\n\r\n连没有幸福都不介意', '03:44', '1', '0');
INSERT INTO `songs` VALUES ('25', '勇', 'Blues', '33', '33', '/mp3/杨千嬅 - 勇.mp3', '/pic/da20ee0f-78a9-4387-a1ae-bdfe7d4a2ba1.png', null, '勇 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：黄伟文\r\n\r\n曲：Barry Chung\r\n\r\n我也不是大无畏\r\n\r\n我也不是不怕死\r\n\r\n但是在浪漫热吻之前\r\n\r\n如何险要悬崖绝岭\r\n\r\n为你亦当是平地\r\n\r\n爱你不用合情理\r\n\r\n但愿用直觉本能去抓住你\r\n\r\n一想到心仪的你\r\n\r\n从来没有的力气\r\n\r\n突然注入渐软的双臂\r\n\r\n旁人从不赞同\r\n\r\n连情理也不容\r\n\r\n仍全情投入伤都不觉痛\r\n\r\n如穷追一个梦\r\n\r\n谁人如何激进\r\n\r\n亦不及我为你那么勇\r\n\r\n沿途红灯再红\r\n\r\n无人可挡我路\r\n\r\n望着是万马千军都直冲\r\n\r\n我没有温柔\r\n\r\n唯独有这点英勇\r\n\r\n我也希望被怜爱\r\n\r\n但自愿扮作英雄去保护你\r\n\r\n勋章你不留给我\r\n\r\n仍然愿意撑下去\r\n\r\n傲然笑着为你挡兵器\r\n\r\n旁人从不赞同\r\n\r\n连情理也不容\r\n\r\n仍全情投入伤都不觉痛\r\n\r\n如穷追一个梦\r\n\r\n谁人如何激进\r\n\r\n亦不及我为你那么勇\r\n\r\n沿途红灯再红\r\n\r\n无人可挡我路\r\n\r\n望着是万马千军都直冲\r\n\r\n我没有温柔\r\n\r\n唯独有这点英勇\r\n\r\n跌下来 再上去\r\n\r\n就像是 不倒翁\r\n\r\n明明已是扑空\r\n\r\n再尽全力补中\r\n\r\n旁人从不赞同\r\n\r\n连情理也不容\r\n\r\n仍全情投入伤都不觉痛\r\n\r\n如穷追一个梦\r\n\r\n谁人如何激进\r\n\r\n亦不及我为你那么勇\r\n\r\n沿途红灯再红\r\n\r\n无人可挡我路\r\n\r\n望着是万马千军都直冲\r\n\r\n再没有支援\r\n\r\n还是有这点英勇\r\n\r\n渴望爱的人\r\n\r\n全部爱得很英勇', '03:41', '1', '0');
INSERT INTO `songs` VALUES ('26', '再见二丁目', 'Blues', '33', '33', '/mp3/杨千嬅 - 再见二丁目.mp3', '/pic/5dfc78f9-f3f9-4b57-bc97-de0ffe67329d.jpg', null, '再见二丁目 - 杨千嬅 (Miriam Yeung)\r\n\r\n词：林夕\r\n\r\n曲：于逸尧\r\n\r\n满街脚步突然静了\r\n\r\n满天柏树突然没有动摇\r\n\r\n这一刹我只需要一罐热茶吧\r\n\r\n那味道似是什么都不紧要\r\n\r\n唱片店内传来异国民谣\r\n\r\n那种快乐突然被我需要\r\n\r\n不亲切至少不似想你般奥妙\r\n\r\n情和调随着怀缅变得萧条\r\n\r\n原来过得很快乐\r\n\r\n只我一人未发觉\r\n\r\n如能忘掉渴望\r\n\r\n岁月长衣裳薄\r\n\r\n无论于什么角落\r\n\r\n不假设你或会在旁\r\n\r\n我也可畅游异国放心吃喝\r\n\r\n转街过巷就如滑过浪潮\r\n\r\n听天说地仍然剩我心跳\r\n\r\n关于你冥想不了可免都免掉\r\n\r\n情和欲留待下个化身燃烧\r\n\r\n原来过得很快乐\r\n\r\n只我一人未发觉\r\n\r\n如能忘掉渴望\r\n\r\n岁月长衣裳薄\r\n\r\n无论于什么角落\r\n\r\n不假设你或会在旁\r\n\r\n我也可畅游异国放心吃喝\r\n\r\n原来我非不快乐\r\n\r\n只我一人未发觉\r\n\r\n如能忘掉渴望\r\n\r\n岁月长衣裳薄\r\n\r\n无论于什么角落\r\n\r\n不假设你或会在旁\r\n\r\n我也可畅游异国再找寄托\r\n\r\n我也可畅游异国再找寄托', '03:56', '1', '0');
INSERT INTO `songs` VALUES ('27', '大鱼', '流行', '34', '35', '/mp3/周深 - 大鱼.mp3', '/pic/85692842-70b5-4574-b10c-0f18dc505bdf.jpg', null, '', 'abc', '1', '0');

-- ----------------------------
-- Table structure for songscomments
-- ----------------------------
DROP TABLE IF EXISTS `songscomments`;
CREATE TABLE `songscomments` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `commentId` int(20) DEFAULT NULL,
  `songsId` int(20) DEFAULT NULL,
  `songName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_Id` (`commentId`),
  KEY `songs-id` (`songsId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of songscomments
-- ----------------------------
INSERT INTO `songscomments` VALUES ('1', '1', '4', 'Shape of You');
INSERT INTO `songscomments` VALUES ('2', '2', '4', 'Shape of You');
INSERT INTO `songscomments` VALUES ('3', '3', '4', 'Shape of You');
INSERT INTO `songscomments` VALUES ('4', '4', '4', 'Shape of You');
INSERT INTO `songscomments` VALUES ('5', '6', '1', '담다디 (Damdadi)');
INSERT INTO `songscomments` VALUES ('6', '7', '1', '담다디 (Damdadi)');
INSERT INTO `songscomments` VALUES ('7', '8', '1', '담다디 (Damdadi)');
INSERT INTO `songscomments` VALUES ('8', '9', '1', '담다디 (Damdadi)');
INSERT INTO `songscomments` VALUES ('9', '10', '1', '담다디 (Damdadi)');
INSERT INTO `songscomments` VALUES ('10', '11', '9', '风的季节');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `userPhone` varchar(12) NOT NULL,
  `userEmail` varchar(255) DEFAULT '',
  `userStatus` int(1) DEFAULT '0' COMMENT '用户状态：1为已登录 0为已注销 3为已删除',
  `userAuthority` int(1) DEFAULT '0' COMMENT '用户权限：0是普通用户 1是管理员',
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `userPicUrl` varchar(255) DEFAULT NULL,
  `userSex` varchar(10) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'lichangqing', '123456', 'lcq', '18360577159', '763893274@qq.com', '1', '1', '贵州', '贵阳', '13247920-2990-4324-b050-13373c945e5c.jpg', '男', '2018-05-06 09:07:35');
INSERT INTO `user` VALUES ('2', 'songzuer', '123456', '宋祖儿', '132456789', '123456@123', '0', '0', '江苏', '连云港', '9303bbc7-65b0-4611-9efc-4769401165b1.jpg', '女', '2018-05-13 01:06:18');
INSERT INTO `user` VALUES ('3', 'tongliya', '123456', '佟丽娅', '12346578545', '444@123', '0', '0', '上海', '黄浦区', '63f299a0-8b11-4135-9f92-5ae03cc53ecb.jpg', '女', '2018-05-13 01:07:18');
INSERT INTO `user` VALUES ('8', 'jcy', 'jcy123', 'jcy', '18655493278', '555@55.com', '3', '0', '江苏', '南通', '7a6b556c-2589-46d7-9cda-a23706d950e5.jpg', '男', '2018-05-08 17:35:44');
INSERT INTO `user` VALUES ('9', 'liuyifei', '123456', '刘亦菲', '133333333333', '123@123.com', '1', '0', '贵州', '贵阳', 'feea9e24-e159-46b1-8b0d-503545b71075.jpg', '女', '2018-05-11 10:48:55');
INSERT INTO `user` VALUES ('11', 'pengzeze', '123456', '彭泽泽', '18360577158', '1123@qq.com', '1', '0', '贵州', '贵阳', '25756391-7d41-48fc-bb42-488675540447.jpg', '男', '2018-05-13 17:22:39');
INSERT INTO `user` VALUES ('12', 'liukeyi', '123456', '刘珂矣', '18360577157', '111@qq.com', '1', '0', '江苏', '南京', '2ba9ffb0-c25a-4c4a-a1a6-a9fe6fd56db5.jpg', '女', '2018-05-13 17:56:30');
INSERT INTO `user` VALUES ('13', 'zhuangxinyan', '123456', '庄心妍', '18360577184', '123@qq.com', '0', '0', '上海', '杨浦区', '8bbcb61e-c0b1-40fb-86b5-32f4698351a0.jpg', '女', '2018-05-13 18:13:08');
INSERT INTO `user` VALUES ('14', 'dengziqi', '123456', null, '18886545642', null, '3', '0', null, null, null, null, '2018-05-31 00:22:55');
INSERT INTO `user` VALUES ('15', 'xinmuyouzi', '123456', null, '15360745632', null, '3', '0', null, null, null, null, '2018-05-31 00:53:36');
INSERT INTO `user` VALUES ('16', 'admin1', '123456', 'admin', '18360577159', '763893274@qq.com', '1', '1', '贵州', '贵阳', '6a6326c3-85eb-4d4b-bee2-f5873bac0fcd.jpg', '男', '2018-05-06 09:10:01');
INSERT INTO `user` VALUES ('17', 'zhangdeshuai', '123456', null, '18360577183', null, '1', '0', null, null, null, null, '2018-06-08 08:37:44');
