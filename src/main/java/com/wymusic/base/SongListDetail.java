package com.wymusic.base;

import java.util.List;

import com.wymusic.pojo.Songlist;
import com.wymusic.pojo.Songs;
import com.wymusic.pojo.User;

public class SongListDetail {
	private User usre;
	private Songlist songlist;
	private List<Songlist> SongListList;
	private Songs song;
	private List<Songs> songsList;
	private List<SongListAndSongsDetail> songListAndSongsDetailsList;
	public User getUsre() {
		return usre;
	}
	public void setUsre(User usre) {
		this.usre = usre;
	}
	public Songlist getSonglist() {
		return songlist;
	}
	public void setSonglist(Songlist songlist) {
		this.songlist = songlist;
	}
	public List<Songlist> getSongListList() {
		return SongListList;
	}
	public void setSongListList(List<Songlist> songListList) {
		SongListList = songListList;
	}
	public Songs getSong() {
		return song;
	}
	public void setSong(Songs song) {
		this.song = song;
	}
	public List<Songs> getSongsList() {
		return songsList;
	}
	public void setSongsList(List<Songs> songsList) {
		this.songsList = songsList;
	}
	
	public List<SongListAndSongsDetail> getSongListAndSongsDetailsList() {
		return songListAndSongsDetailsList;
	}
	public void setSongListAndSongsDetailsList(List<SongListAndSongsDetail> songListAndSongsDetailsList) {
		this.songListAndSongsDetailsList = songListAndSongsDetailsList;
	}
	
	public SongListDetail(User usre, Songlist songlist, List<Songlist> songListList, Songs song, List<Songs> songsList,
			List<SongListAndSongsDetail> songListAndSongsDetailsList) {
		super();
		this.usre = usre;
		this.songlist = songlist;
		SongListList = songListList;
		this.song = song;
		this.songsList = songsList;
		this.songListAndSongsDetailsList = songListAndSongsDetailsList;
	}
	public SongListDetail() {
		super();
	}
	@Override
	public String toString() {
		return "SongListDetail [usre=" + usre + ", songlist=" + songlist + ", SongListList=" + SongListList + ", song="
				+ song + ", songsList=" + songsList + ", songListAndSongsDetailsList=" + songListAndSongsDetailsList
				+ "]";
	}
}
