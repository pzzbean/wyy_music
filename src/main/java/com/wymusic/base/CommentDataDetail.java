package com.wymusic.base;

import java.util.List;

import com.wymusic.pojo.Album;
import com.wymusic.pojo.Albumcomts;
import com.wymusic.pojo.Comments;
import com.wymusic.pojo.Songs;
import com.wymusic.pojo.User;

public class CommentDataDetail {
	private Comments comments;//歌曲评论
	private List<Comments> commentsList;//歌曲评论集合
	private Albumcomts albumcomts;//专辑评论
	private List<Albumcomts> albumcomtsList;//专辑评论集合
	private Songs songs;
	private List<Songs> songsList;
	private Album album;
	private List<Album> albumList;
	private User user;
	private List<User> userList;
	public Comments getComments() {
		return comments;
	}
	public void setComments(Comments comments) {
		this.comments = comments;
	}
	public List<Comments> getCommentsList() {
		return commentsList;
	}
	public void setCommentsList(List<Comments> commentsList) {
		this.commentsList = commentsList;
	}
	public Songs getSongs() {
		return songs;
	}
	public void setSongs(Songs songs) {
		this.songs = songs;
	}
	public List<Songs> getSongsList() {
		return songsList;
	}
	public void setSongsList(List<Songs> songsList) {
		this.songsList = songsList;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public List<Album> getAlbumList() {
		return albumList;
	}
	public void setAlbumList(List<Album> albumList) {
		this.albumList = albumList;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	
	public Albumcomts getAlbumcomts() {
		return albumcomts;
	}
	public void setAlbumcomts(Albumcomts albumcomts) {
		this.albumcomts = albumcomts;
	}
	public List<Albumcomts> getAlbumcomtsList() {
		return albumcomtsList;
	}
	public void setAlbumcomtsList(List<Albumcomts> albumcomtsList) {
		this.albumcomtsList = albumcomtsList;
	}
	@Override
	public String toString() {
		return "CommentDataDetail [comments=" + comments + ", commentsList=" + commentsList + ", albumcomts="
				+ albumcomts + ", albumcomtsList=" + albumcomtsList + ", songs=" + songs + ", songsList=" + songsList
				+ ", album=" + album + ", albumList=" + albumList + ", user=" + user + ", userList=" + userList + "]";
	}
	
	public CommentDataDetail(Comments comments, List<Comments> commentsList, Albumcomts albumcomts,
			List<Albumcomts> albumcomtsList, Songs songs, List<Songs> songsList, Album album, List<Album> albumList,
			User user, List<User> userList) {
		super();
		this.comments = comments;
		this.commentsList = commentsList;
		this.albumcomts = albumcomts;
		this.albumcomtsList = albumcomtsList;
		this.songs = songs;
		this.songsList = songsList;
		this.album = album;
		this.albumList = albumList;
		this.user = user;
		this.userList = userList;
	}
	public CommentDataDetail() {
		super();
	}
}
