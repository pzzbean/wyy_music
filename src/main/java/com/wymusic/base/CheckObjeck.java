package com.wymusic.base;

public class CheckObjeck<E> {
	private E obj;
	private boolean flag;
	
	public E getObj() {
		return obj;
	}
	public void setObj(E obj) {
		this.obj = obj;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public CheckObjeck(E obj, boolean flag) {
		super();
		this.obj = obj;
		this.flag = flag;
	}
	public CheckObjeck() {
		super();
	}
	@Override
	public String toString() {
		return "CheckObjeck [obj=" + obj + ", flag=" + flag + "]";
	}
	
}
