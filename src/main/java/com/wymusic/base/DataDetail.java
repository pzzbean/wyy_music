package com.wymusic.base;

import java.util.List;

import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.Songs;

public class DataDetail {
	private Songer songer;
	private Album album;
	private Songs song;
	private List<Songer> songerList;
	private List<Album> albumList;
	private List<Songs> songList;
	public Songer getSonger() {
		return songer;
	}
	public void setSonger(Songer songer) {
		this.songer = songer;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public Songs getSong() {
		return song;
	}
	public void setSong(Songs song) {
		this.song = song;
	}
	public List<Album> getAlbumList() {
		return albumList;
	}
	public void setAlbumList(List<Album> albumList) {
		this.albumList = albumList;
	}
	public List<Songs> getSongList() {
		return songList;
	}
	public void setSongList(List<Songs> songList) {
		this.songList = songList;
	}
	public DataDetail(Songer songer, Album album, Songs song) {
		super();
		this.songer = songer;
		this.album = album;
		this.song = song;
	}
	public DataDetail(Songer songer, List<Album> albumList, List<Songs> songList) {
		super();
		this.songer = songer;
		this.albumList = albumList;
		this.songList = songList;
	}
	public DataDetail(Songer songer, Album album, List<Songs> songList) {
		super();
		this.songer = songer;
		this.album = album;
		this.songList = songList;
	}
	public DataDetail() {
		super();
	}
	public List<Songer> getSongerList() {
		return songerList;
	}
	public void setSongerList(List<Songer> songerList) {
		this.songerList = songerList;
	}
	
	@Override
	public String toString() {
		return "DataDetail [songer=" + songer + ", album=" + album + ", song=" + song + ", songerList=" + songerList
				+ ", albumList=" + albumList + ", songList=" + songList + "]";
	}
}
