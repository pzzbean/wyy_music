package com.wymusic.base;

import java.util.List;

import com.wymusic.pojo.Songer;
import com.wymusic.pojo.Songlist;
import com.wymusic.pojo.Songs;

public class SongListAndSongsDetail {
	private Songlist songlist;
	private List<Songs> songsList;
	private List<Songer> songerList;
	public Songlist getSonglist() {
		return songlist;
	}
	public void setSonglist(Songlist songlist) {
		this.songlist = songlist;
	}
	public List<Songs> getSongsList() {
		return songsList;
	}
	public void setSongsList(List<Songs> songsList) {
		this.songsList = songsList;
	}
	public SongListAndSongsDetail(Songlist songlist, List<Songs> songsList) {
		super();
		this.songlist = songlist;
		this.songsList = songsList;
	}
	public SongListAndSongsDetail() {
		super();
	}
	
	public List<Songer> getSongerList() {
		return songerList;
	}
	public void setSongerList(List<Songer> songerList) {
		this.songerList = songerList;
	}
	@Override
	public String toString() {
		return "SongListAndSongsDetail [songlist=" + songlist + ", songsList=" + songsList + ", songerList="
				+ songerList + "]";
	}
}
