package com.wymusic.base;

/**
 * API状态格式封装
 */
public class ApiResponse {
    private int code;
    private String massage;
    private Object data;
    private boolean more;
    private String str;

    public ApiResponse(int code, String massage, Object data) {
        this.code = code;
        this.massage = massage;
        this.data = data;
    }

    public ApiResponse() {
        this.code=Status.SUCCESS.getCode();
        this.massage=Status.SUCCESS.getStandarMassage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    
    
    public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public static ApiResponse ofMassage(int code,String massage){
        return new ApiResponse(code,massage,null);
    }

    public static ApiResponse ofSuccess(Object data){
        return  new ApiResponse(Status.SUCCESS.getCode(),Status.SUCCESS.getStandarMassage(),data);
    }

    public static ApiResponse ofStatus(Status status){
        return new ApiResponse(status.getCode(),status.getStandarMassage(),null);
    }

    public enum Status{
        SUCCESS(200,"OK"),
        BAD_REQUEST(400,"错误的请求！"),
        NOT_FOUND(404,"没有找到资源！"),
        INTERNAL_SERVER_ERROR(500,"未知的内部错误！"),
        NOT_VALID_PARAM(40005,"无效的参数！"),
        NOT_SUPPORTED_OPERATION(40006,"不支持的操作！"),
        NOT_LOGIN(50000,"未登录状态!");

        private int code;
        private String standarMassage;

        Status(int code, String standarMassage) {
            this.code = code;
            this.standarMassage = standarMassage;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getStandarMassage() {
            return standarMassage;
        }

        public void setStandarMassage(String standarMassage) {
            this.standarMassage = standarMassage;
        }

        public class NOT_FOUND {
        }
    }
}
