package com.wymusic.vo;

public class PageVo {
	private int page;
	private int pageSize;
	private int count;
	private Object parm;
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public PageVo(int page, int pageSize, int count) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.count = count;
	}
	public PageVo() {
		super();
	}
	
	
	public Object getParm() {
		return parm;
	}
	public void setParm(Object parm) {
		this.parm = parm;
	}
	
	
	public PageVo(int page, int pageSize, int count, Object parm) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.count = count;
		this.parm = parm;
	}
	@Override
	public String toString() {
		return "PageVo [page=" + page + ", pageSize=" + pageSize + ", count=" + count + ", parm=" + parm + "]";
	}
	
}
