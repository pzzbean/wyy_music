package com.wymusic.vo;

public class UserVo {
	private String account;
	private String userPhone;
	
	public UserVo(String account, String userPhone) {
		this.account = account;
		this.userPhone = userPhone;
	}

	public UserVo() {
		super();
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	@Override
	public String toString() {
		return "UserVo [account=" + account + ", userPhone=" + userPhone + "]";
	}
}
