package com.wymusic.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @auth ChenLong
 * @Date 2019/5/14 15:13
 */
@Component
@Scope("prototype")
@Slf4j
public class FTPHolder {
    @Value("${ftp.ip}")
    private String ftpIp;
    @Value("${ftp.port}")
    private String ftpPort;
    @Value("${ftp.user}")
    private String ftpUser;
    @Value("${ftp.pwd}")
    private String ftpPwd;

    private FTPClient ftpClient;

    private boolean login() throws Exception {
        if (StringUtils.isBlank(ftpUser) || StringUtils.isBlank(ftpPwd)) {
            throw new Exception("FTP用户名为空或密码为空");
        }
        ftpClient = new FTPClient();
        FTPClientConfig config = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
        ftpClient.configure(config);
        ftpClient.connect(this.ftpIp, Integer.parseInt(this.ftpPort));// 连接FTP服务器
        return ftpClient.login(ftpUser, ftpPwd);// 登陆
    }

    public boolean uploadFile(String fileName, String path, InputStream in) throws Exception {
        try {
            if (login()) {
                int reply = ftpClient.getReplyCode();
                if (!FTPReply.isPositiveCompletion(reply)) {
                    ftpClient.disconnect();
                    return false;
                }
                log.info("图片上传路径=====>"+path);
                changeWorkingDirectory(ftpClient,path);
                //ftp服务器被动模式的时候需要设置该语句
                ftpClient.enterLocalPassiveMode();//每次数据连接之前,ftp client告诉ftp server开通一个端口来传输数据
                //ftp主动模式 （默认主动模式）
//                ftpClient.enterLocalActiveMode();
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                ftpClient.storeFile(fileName, in);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            in.close();
            ftpClient.logout();
            ftpClient.disconnect();
        }
        return false;
    }

    public InputStream downLoadFile(String remotePath, String fileName) throws Exception {
        try {
            if (login()) {
                ftpClient.enterLocalPassiveMode();
                if (!ftpClient.changeWorkingDirectory(remotePath)) {
                    return null;
                }
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                ftpClient.changeWorkingDirectory(remotePath);
                InputStream stream = ftpClient.retrieveFileStream(new String(fileName.getBytes("utf-8"), "iso-8859-1"));
                ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
                if (stream == null) {
                    return null;
                }
                byte[] buff = new byte[2048];
                int rc = 0;
                while ((rc = stream.read(buff, 0, buff.length)) > 0) {
                    swapStream.write(buff, 0, rc);
                }
                byte[] in2b = swapStream.toByteArray();
                InputStream result = new ByteArrayInputStream(in2b);
                stream.close();
                swapStream.close();
                if (ftpClient.completePendingCommand()) {
                    return result;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ftpClient.logout();
            ftpClient.disconnect();
        }
        return null;

    }

    public boolean isFile(String remotePath, String fileName) throws Exception {
        try {
            if (login()) {
                if (!ftpClient.changeWorkingDirectory(remotePath)) {
                    return false;
                }
                ftpClient.changeWorkingDirectory(remotePath);
                String[] strArray = ftpClient.listNames(new String(fileName.getBytes("utf-8"), "iso-8859-1"));
                return strArray != null && strArray.length > 0;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            ftpClient.logout();
            ftpClient.disconnect();
        }
        return false;
    }

    private static boolean changeWorkingDirectory(FTPClient ftpClient,String path) throws IOException {
        int index = path.indexOf("/") == -1 ? path.indexOf("\\") : path.indexOf("/");
        if ("".equals(path) || index == -1){
            return true;
        }
        String tempPath = path.substring(0,index);
        if (!ftpClient.changeWorkingDirectory(tempPath)){
            ftpClient.makeDirectory(tempPath);
        }
        ftpClient.changeWorkingDirectory(tempPath);
        String next = path.substring(index + 1,path.length());
        return changeWorkingDirectory(ftpClient,next);
    }
}

