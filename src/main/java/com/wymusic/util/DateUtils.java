package com.wymusic.util;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author: pzz
 * @date: 2020/5/22 13:35
 * @version: 2.0
 * @description:
 */
public class DateUtils {

    /**
     * 获去一周前的时间
     * @return
     */
    public static Date getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        return today;
    }

    /**
     * 获去一天的开始时间
     * @return
     */
    public static Date getStartByDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND,0);
        return calendar.getTime();
    }



    /**
     * @description:时间转字符串
     */
    public static String dateToString(String pattern, Date date){
        String temp = pattern;
        if (StringUtils.isBlank(pattern)){
            temp = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(temp);
        return sdf.format(date);
    }

    /**
     * @description:时间转字符串
     */
    public static String dateToString(Date date){
        return dateToString("",date);
    }
}
