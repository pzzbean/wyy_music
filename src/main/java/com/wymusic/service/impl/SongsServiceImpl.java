package com.wymusic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wymusic.base.CheckObjeck;
import com.wymusic.base.DataDetail;
import com.wymusic.mapper.AlbumMapper;
import com.wymusic.mapper.SongerMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.SongerExample;
import com.wymusic.pojo.Songs;
import com.wymusic.pojo.SongsExample;
import com.wymusic.service.SongsService;

@Service
public class SongsServiceImpl implements SongsService{
	
	@Autowired
	private SongsMapper songsMapper;
	
	@Autowired
	private SongerMapper songerMapper;
	
	@Autowired
	private AlbumMapper albumMapper;
	
	//通过Id查询歌曲详细信息
	@Override
	public DataDetail findSongsDetailInfoById(String songsid) {
		DataDetail data = new DataDetail();
		if (songsid!=null||!("".equals(songsid))) {
			Songs song = songsMapper.selectByPrimaryKey(Integer.valueOf(songsid));
			if(song!=null){
				data.setSong(song);
				if(song.getAlbumid()!=null){
					Album album = albumMapper.selectByPrimaryKey(song.getAlbumid());
					if(album!=null){
						data.setAlbum(album);
					}else{
						data.setAlbum(new Album());
					}
				}else{
					data.setAlbum(new Album());
				}
				
				if(song.getSongerid()!=null){
					Songer songer = songerMapper.selectByPrimaryKey(song.getSongerid());
					if(songer!=null){
						data.setSonger(songer);
					}else{
						data.setSonger(new Songer());
					}
				}else{
					data.setSonger(new Songer());
				}
			}
		}
		return data;
	}

	//通过id数组查询对应歌曲
	@Override
	public DataDetail findSongsByIdList(String songidlist) {
		DataDetail data = new DataDetail();
		List<Songs> songList = new ArrayList<>();
		List<Songer> sonegrList = new ArrayList<>();
		if (songidlist != null) {
			String[] idLit = songidlist.split(",");
			for (String str : idLit) {
				Songs song = songsMapper.selectByPrimaryKey(Integer.valueOf(str));
				if(song.getSongerid() !=null){
					Songer songer = songerMapper.selectByPrimaryKey(song.getSongerid());
					sonegrList.add(songer);
				}
				songList.add(song);
			}
			data.setSongList(songList);
			data.setSongerList(sonegrList);
		}
		// TODO Auto-generated method stub
		return data;
	}
	
	//查询全部歌曲信息
	@Override
	public DataDetail findAll() {
		DataDetail data = new DataDetail();
		SongsExample example =new SongsExample();
		List<Songs> songList = songsMapper.selectByExample(example);
		List<Songer> songerList = new ArrayList<Songer>();
		List<Album> albumList = new ArrayList<>();
		for (Songs songs : songList) {
			if (songs.getSongerid()!=null) {
				Songer songer = songerMapper.selectByPrimaryKey(songs.getSongerid());
				if(songer !=null){
					songerList.add(songer);
				}else{
					songerList.add(new Songer());
				}
			}else{
				songerList.add(new Songer());
			}
			
			if(songs.getAlbumid()!=null){
				Album album = albumMapper.selectByPrimaryKey(songs.getAlbumid());
				if(album!=null){
					albumList.add(album);
				}else{
					albumList.add(new Album());
				}
			}else{
				albumList.add(new Album());
			}
		}
		data.setSongList(songList);
		data.setSongerList(songerList);
		data.setAlbumList(albumList);
		return data;
	}
	
	//根据id更新歌曲状态
	@Override
	public void updateSongsStatusById(String songsid,String songsStatus) {
		if (songsid!=null) {
			Songs song =new Songs();
			song.setSongid(Integer.valueOf(songsid));
			song.setSongstatus(Integer.valueOf(songsStatus));
			songsMapper.updateByPrimaryKeySelective(song);
		}
		
	}
	
	//更新歌曲信息
	@Override
	public void updatasongInfo(String songPicUrl, String songername, String albumname, Songs song) {
		if(songPicUrl!=null||"".equals(songPicUrl)){
			song.setSongurl(songPicUrl);
		}
		songername = songername.substring(0,songername.indexOf(','));
		albumname = albumname.substring(0 ,albumname.indexOf(','));
		if(songername!=null||"".equals(songername)){
			Songer songer = songerMapper.selectSongerByName(songername);
			if(songer !=null){
				song.setSongerid(songer.getSongerid());
				List<Album> albumList = albumMapper.selectAlbumBySongerId(songer.getSongerid());
				if(albumname!=null||"".equals(albumname)){
					Album tempAlbum = new Album();
					if(albumList!=null){
						CheckObjeck<Album> checkData = albumIsExist(albumList, albumname);
						if(checkData.isFlag()){
							song.setAlbumid(checkData.getObj().getAlbumid());
						}else{
							tempAlbum.setAlbumname(albumname);
							tempAlbum.setAlbumstatus(1);
							tempAlbum.setSongerid(songer.getSongerid());
							tempAlbum.setSongername(songer.getSongername());
							albumMapper.insertSelective(tempAlbum);
							
							Album newAlbum = albumMapper.selectAlbumByName(albumname);
							System.out.println("===>newAlbum"+newAlbum);
							song.setAlbumid(newAlbum.getAlbumid());
						}
					}else{
						tempAlbum.setAlbumname(albumname);
						tempAlbum.setAlbumstatus(1);
						tempAlbum.setSongerid(songer.getSongerid());
						tempAlbum.setSongername(songer.getSongername());
						albumMapper.insertSelective(tempAlbum);
						Album newAlbum = albumMapper.selectAlbumByName(albumname);
						System.out.println("===>newAlbum2"+newAlbum);
						song.setAlbumid(newAlbum.getAlbumid());
					}
				}
			}else{
				Songer tempSonger =new Songer();
				tempSonger.setSongername(songername);
				tempSonger.setSongerstatus(1);
				songerMapper.insertSelective(tempSonger);
				
				Songer songer1 = songerMapper.selectSongerByName(songername);
				System.out.println("===>songer1"+songer1);
				song.setSongerid(songer1.getSongerid());
				if(albumname!=null||"".equals(albumname)){
					albumname = albumname.substring(0 ,albumname.indexOf(','));
					Album tempAlbum = new Album();
					tempAlbum.setAlbumname(albumname);
					tempAlbum.setAlbumstatus(1);
					tempAlbum.setSongerid(songer1.getSongerid());
					tempAlbum.setSongername(songer1.getSongername());
					albumMapper.insertSelective(tempAlbum);
					
					Album newAlbum = albumMapper.selectAlbumByName(albumname);
					System.out.println("===>newAlbum3"+newAlbum);
					song.setAlbumid(newAlbum.getAlbumid());
				}
				
			}
		}
		
		if (song!=null) {
			songsMapper.updateByPrimaryKeySelective(song);
		}
	}
	
	//添加歌曲
	@Override
	public void addSongs(String songPicUrl, String songername, String albumname, Songs song) {
		if(songPicUrl!=null||"".equals(songPicUrl)){
			song.setSongurl(songPicUrl);
		}
		songername = songername.substring(0,songername.indexOf(','));
		albumname = albumname.substring(0 ,albumname.indexOf(','));
		if(songername!=null||"".equals(songername)){
			Songer songer = songerMapper.selectSongerByName(songername);
			if(songer !=null){
				song.setSongerid(songer.getSongerid());
				List<Album> albumList = albumMapper.selectAlbumBySongerId(songer.getSongerid());
				if(albumname!=null||"".equals(albumname)){
					Album tempAlbum = new Album();
					if(albumList!=null){
						CheckObjeck<Album> checkData = albumIsExist(albumList, albumname);
						if(checkData.isFlag()){
							song.setAlbumid(checkData.getObj().getAlbumid());
						}else{
							tempAlbum.setAlbumname(albumname);
							tempAlbum.setAlbumstatus(1);
							tempAlbum.setSongerid(songer.getSongerid());
							tempAlbum.setSongername(songer.getSongername());
							albumMapper.insertSelective(tempAlbum);
							
							Album newAlbum = albumMapper.selectAlbumByName(albumname);
							song.setAlbumid(newAlbum.getAlbumid());
						}
					}else{
						tempAlbum.setAlbumname(albumname);
						tempAlbum.setAlbumstatus(1);
						tempAlbum.setSongerid(songer.getSongerid());
						tempAlbum.setSongername(songer.getSongername());
						albumMapper.insertSelective(tempAlbum);
						Album newAlbum = albumMapper.selectAlbumByName(albumname);
						song.setAlbumid(newAlbum.getAlbumid());
					}
				}
			}else{
				Songer tempSonger =new Songer();
				tempSonger.setSongername(songername);
				tempSonger.setSongerstatus(1);
				songerMapper.insertSelective(tempSonger);
				
				Songer songer1 = songerMapper.selectSongerByName(songername);
				System.out.println("===>songer1"+songer1);
				song.setSongerid(songer1.getSongerid());
				if(albumname!=null||"".equals(albumname)){
					albumname = albumname.substring(0 ,albumname.indexOf(','));
					Album tempAlbum = new Album();
					tempAlbum.setAlbumname(albumname);
					tempAlbum.setAlbumstatus(1);
					tempAlbum.setSongerid(songer1.getSongerid());
					tempAlbum.setSongername(songer1.getSongername());
					albumMapper.insertSelective(tempAlbum);
					
					Album newAlbum = albumMapper.selectAlbumByName(albumname);
					System.out.println("===>newAlbum3"+newAlbum);
					song.setAlbumid(newAlbum.getAlbumid());
				}
				
			}
		}
		
		if (song!=null) {
			songsMapper.insertSelective(song);
		}
	}
	
	@Override
	public void delSongById(String songid) {
		if (songid!=null) {
			songsMapper.deleteByPrimaryKey(Integer.valueOf(songid));
		}
	}
	
	//按songhot降序查询歌曲信息
	@Override
	public DataDetail selectBySongHotDesc(){
		List<Songs> songList = songsMapper.selectBySongHotDesc();
		DataDetail data = selectSongDetailUtil(songList);
		return data;
	}
	
	public CheckObjeck<Album> albumIsExist(List<Album> list,String dstAlbumName){
		CheckObjeck<Album>  checkData = new CheckObjeck<Album>();
		for (Album album : list) {
			if(dstAlbumName.equals(album.getAlbumname())||dstAlbumName==album.getAlbumname()){
				checkData.setFlag(true);
				checkData.setObj(album);
				break;
			}else{
				checkData.setFlag(false);
				checkData.setObj(null);
			}
		}
		return checkData;
	}
	
	public DataDetail selectSongDetailUtil(List<Songs> songList){
		DataDetail data = new DataDetail();
		List<Songer> songerList = new ArrayList<Songer>();
		List<Album> albumList = new ArrayList<>();
		for (Songs songs : songList) {
			if (songs.getSongerid()!=null) {
				Songer songer = songerMapper.selectByPrimaryKey(songs.getSongerid());
				if(songer !=null){
					songerList.add(songer);
				}else{
					songerList.add(new Songer());
				}
			}else{
				songerList.add(new Songer());
			}
			
			if(songs.getAlbumid()!=null){
				Album album = albumMapper.selectByPrimaryKey(songs.getAlbumid());
				if(album!=null){
					albumList.add(album);
				}else{
					albumList.add(new Album());
				}
			}else{
				albumList.add(new Album());
			}
		}
		data.setSongList(songList);
		data.setSongerList(songerList);
		data.setAlbumList(albumList);
		return data;
	}
	
	
	//点击其他排行榜
	@Override
	public DataDetail songsTopListByArea(String area) {
		String parm = "";
		DataDetail data = new DataDetail();
		List<Songs> songList = new ArrayList<>();
		if("巅峰热歌榜".equals(area)){
			songList = songsMapper.selectBySongHotDesc();
			data = selectSongDetailUtil(songList);
			return data;
		}else if("巅峰新歌榜".equals(area)){
			songList = songsMapper.selectBySongIdDesc();
			data = selectSongDetailUtil(songList);
			return data;
		}else if("华语热歌榜".equals(area)){
			parm="中国";
			List list =new ArrayList<>();
			list.add(parm);
			songList = songsMapper.selectBySongerCountryAndSongHotDesc(list);
			data = selectSongDetailUtil(songList);
			return data;
			
		}else if("欧美热歌榜".equals(area)){
			parm="法国,德国,意大利,荷兰,比利时,卢森堡,英国,丹麦,爱尔兰,希腊,西班牙,葡萄牙,"
			        +"奥地利,瑞典,芬兰塞,浦路斯,匈牙利,捷克,爱沙尼亚,拉脱维亚,立陶宛,马耳他,波兰,"
					+"斯洛伐克,斯洛文尼亚,美国,挪威";
			String[] strArray = parm.split(",");
			List list =new ArrayList<>();
			for (String string : strArray) {
				list.add(string);
			}
			songList = songsMapper.selectBySongerCountryAndSongHotDesc(list);
			data = selectSongDetailUtil(songList);
			return data;
		}else if("日韩热歌榜".equals(area)){
			parm="日本,韩国";
			String[] strArray = parm.split(",");
			List list =new ArrayList<>();
			for (String string : strArray) {
				list.add(string);
			}
			songList = songsMapper.selectBySongerCountryAndSongHotDesc(list);
			data = selectSongDetailUtil(songList);
			return data;
		}
		return data;
	}
	
}
