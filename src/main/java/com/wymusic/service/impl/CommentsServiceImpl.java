package com.wymusic.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wymusic.base.CommentDataDetail;
import com.wymusic.mapper.AlbumMapper;
import com.wymusic.mapper.AlbumcommentsMapper;
import com.wymusic.mapper.AlbumcomtsMapper;
import com.wymusic.mapper.CommentsMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.mapper.SongscommentsMapper;
import com.wymusic.mapper.UserMapper;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Albumcomments;
import com.wymusic.pojo.Albumcomts;
import com.wymusic.pojo.AlbumcomtsExample;
import com.wymusic.pojo.Comments;
import com.wymusic.pojo.CommentsExample;
import com.wymusic.pojo.Songs;
import com.wymusic.pojo.Songscomments;
import com.wymusic.pojo.User;
import com.wymusic.service.CommentsService;
@Service
public class CommentsServiceImpl implements CommentsService{
	
	@Autowired
	private CommentsMapper commentsMapper;//歌曲评论
	
	@Autowired
	private AlbumcomtsMapper albumcomtsMapper; //专辑评论
	
	@Autowired
	private SongsMapper songsMapper;
	
	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private AlbumMapper albumMapper;
	
	@Autowired
	private SongscommentsMapper songscommentsMapper;
	
	@Autowired
	private AlbumcommentsMapper albumcommentsMapper;
    
	//查询所有歌曲评论和对应用户
	@Override
	public CommentDataDetail findAll() {
		CommentsExample example =new CommentsExample();
		List<Comments> commentsList = commentsMapper.selectByExampleWithBLOBs(example);
		List<User> userList = new ArrayList<>();
		for (Comments comments : commentsList) {
			if (comments!=null) {
				User user = userMapper.selectByPrimaryKey(comments.getUserid());
				userList.add(user);
			}else{
				userList.add(new User());
			}
		}
		CommentDataDetail commentDataDetail = new CommentDataDetail();
		commentDataDetail.setCommentsList(commentsList);
		commentDataDetail.setUserList(userList);
		return commentDataDetail;
	}
    
	//更改歌曲评论状态
	@Override
	public void updateStatusById(String commentId,String status) {
		if (commentId!=null||"".equals(commentId)) {
			Comments comments =new Comments();
			comments.setCommentid(Integer.valueOf(commentId));
			comments.setStatus(Integer.valueOf(status));
			commentsMapper.updateByPrimaryKeySelective(comments);
		}
	}

	//根据评论ID查询评论
	@Override
	public Comments findCommentsById(String commentId) {
		Comments comments =new Comments();
		if (commentId!=null||"".equals(commentId)) {
			comments = commentsMapper.selectByPrimaryKey(Integer.valueOf(commentId));
		}
		return comments;
	}
	
	//查询所有专辑评论和对应用户
	@Override
	public CommentDataDetail findAllAlbumComts() {
		AlbumcomtsExample example =new AlbumcomtsExample();
		List<Albumcomts> albumcomtsList = albumcomtsMapper.selectByExampleWithBLOBs(example);
		List<User> userList = new ArrayList<>();
		for (Albumcomts albumcomts : albumcomtsList) {
			if (albumcomts!=null) {
				User user = userMapper.selectByPrimaryKey(albumcomts.getUserid());
				userList.add(user);
			}else{
				userList.add(new User());
			}
		}
		CommentDataDetail commentDataDetail = new CommentDataDetail();
		commentDataDetail.setAlbumcomtsList(albumcomtsList);
		commentDataDetail.setUserList(userList);
		return commentDataDetail;
	}

	//更改专辑评论状态
	@Override
	public void updateALbumComtsStatusById(String commentId, String status) {
		if (commentId!=null||"".equals(commentId)) {
			Albumcomts  albumcomts =new Albumcomts();
			albumcomts.setCommentid(Integer.valueOf(commentId));
			albumcomts.setStatus(Integer.valueOf(status));
			albumcomtsMapper.updateByPrimaryKeySelective(albumcomts);
		}
		
	}
	
	//根据歌曲ID查询评论
	@Override
	public CommentDataDetail findCommentsBySongId(String songId) {
		CommentDataDetail commentDataDetail =new CommentDataDetail();
		List<Comments> commentsList = new ArrayList<>();
		List<User> userList = new ArrayList<>();
		if (songId!=null||"".equals(songId)) {
			List<Songscomments> songscommentsList = 
					songscommentsMapper.findCommentsBySongId(Integer.valueOf(songId));
			
			for (Songscomments songscomments : songscommentsList) {
				if(songscomments!=null){
					Comments comments = commentsMapper.selectByPrimaryKey(songscomments.getCommentid());
					User user = userMapper.selectByPrimaryKey(comments.getUserid());
					commentsList.add(comments);
					userList.add(user);
				}else{
					commentsList.add(new Comments());
					userList.add(new User());
				}
			}
		}
		commentDataDetail.setCommentsList(commentsList);
		commentDataDetail.setUserList(userList);
		return commentDataDetail;
	}
	
	//给歌曲添加评论
	@Override
	public void addComments(String userid, String cotext, String songId) {
		User user = userMapper.selectByPrimaryKey(Integer.valueOf(userid));
		Songs songs = songsMapper.selectByPrimaryKey(Integer.valueOf(songId));
		Comments comments = new Comments(user.getUserid(), user.getUsername(),
				2, new Date(), cotext);
		commentsMapper.insertSelective(comments);
		Comments newComment = commentsMapper.selectMaxId();
		if (newComment!=null) {
			Songscomments songscomments = new Songscomments(newComment.getCommentid(),
					songs.getSongid(), songs.getSongname());
			songscommentsMapper.insertSelective(songscomments);
		}
	}
	
	//根据歌曲id查询评论
	@Override
	public CommentDataDetail findAlbumComtsByAlbumId(String albumId) {
		CommentDataDetail commentDataDetail =new CommentDataDetail();
		List<Albumcomts> albumcomtsList = new ArrayList<>();
		List<User> userList = new ArrayList<>();
		if (albumId!=null||"".equals(albumId)) {
			List<Albumcomments> albumcommentsList = 
					albumcommentsMapper.findAlbumComtsByAlbumId(Integer.valueOf(albumId));
			for (Albumcomments albumcomments : albumcommentsList) {
				if(albumcomments!=null){
					Albumcomts albumcomts = albumcomtsMapper.selectByPrimaryKey(albumcomments.getCommentid());
					User user = userMapper.selectByPrimaryKey(albumcomts.getUserid());
					albumcomtsList.add(albumcomts);
					userList.add(user);
				}else{
					albumcomtsList.add(new Albumcomts());
					userList.add(new User());
				}
			}
		}
		commentDataDetail.setAlbumcomtsList(albumcomtsList);
		commentDataDetail.setUserList(userList);
		return commentDataDetail;
	}

	//给专辑添加评论
	@Override
	public void addAlbumcomts(String userid, String cotext, String AlbumId) {
		User user = userMapper.selectByPrimaryKey(Integer.valueOf(userid));
		Album  album = albumMapper.selectByPrimaryKey(Integer.valueOf(AlbumId));
		Albumcomts albumcomts = new Albumcomts(user.getUserid(), 2, new Date(), cotext);
		albumcomtsMapper.insertSelective(albumcomts);
		Albumcomts newAlbumcomts = albumcomtsMapper.selectMaxId();
		if (newAlbumcomts!=null) {
			Albumcomments albumcomments = new Albumcomments(newAlbumcomts.getCommentid(),
					album.getAlbumid(), album.getAlbumname());
			albumcommentsMapper.insert(albumcomments);
		}
	}
}
