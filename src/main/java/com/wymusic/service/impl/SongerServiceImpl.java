package com.wymusic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wymusic.base.DataDetail;
import com.wymusic.mapper.AlbumMapper;
import com.wymusic.mapper.SongerMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.SongerExample;
import com.wymusic.pojo.Songs;
import com.wymusic.service.SongerService;
import com.wymusic.vo.PageVo;

@Service
public class SongerServiceImpl implements SongerService {
	
	@Autowired
	private SongerMapper songerMapper;
	
	@Autowired
	private AlbumMapper albumMapper;
	
	@Autowired
	private SongsMapper songsMapper;
	
	//查询所有歌手
	@Override
	public List<Songer> findAll(){
		SongerExample example = new SongerExample(); 
		
		return songerMapper.selectByExampleWithBLOBs(example);
	}
	
	//通过歌手名字查找歌手
	@Override
	public Songer findSongerByName(String songername) {
		if(songername!=null){
			Songer songer = songerMapper.selectSongerByName(songername);
			if(songer!=null){
				return songer;
			}
			return null;
		}
		return null;
	}
	
	//添加歌手
	public void insertSonger(Songer songer){
		if(songer!=null){
			songerMapper.insertSelective(songer);
		}
	}
   
	//歌手分页
	@Override
	public List<Songer> findByPage(String page, String pageSize, String songerCountry) {
		int tempPageSize = Integer.valueOf(pageSize);
		int tempPage1 = Integer.valueOf(page)*tempPageSize-tempPageSize;
		PageVo pageVo =new PageVo(tempPage1, tempPageSize, 0,null);
		return songerMapper.findByPage(pageVo);
	}
	
	//按国家地区查询歌手
	@Override
	public List<Songer> findBysongerCountry(String songerCountry) {
		String parm = "";
		List<Songer> resList = new ArrayList<Songer>();
		if("歌手首页".equals(songerCountry)){
			SongerExample example = new SongerExample(); 
			List<Songer> list =new ArrayList<>();
			list = songerMapper.selectByExampleWithBLOBs(example);
			for (Songer songer : list) {
				if(songer.getSongerstatus()==1){
					resList.add(songer);
				}
			}
		}else if("华语歌手".equals(songerCountry)){
			List list =new ArrayList<>();
			parm="中国";
			list.add(parm);
			resList = songerMapper.findBysongerCountry(list);
		}else if("欧美歌手".equals(songerCountry)){
			List list =new ArrayList<>();
			parm="法国,德国,意大利,荷兰,比利时,卢森堡,英国,丹麦,爱尔兰,希腊,西班牙,葡萄牙,"
			        +"奥地利,瑞典,芬兰塞,浦路斯,匈牙利,捷克,爱沙尼亚,拉脱维亚,立陶宛,马耳他,波兰,"
					+"斯洛伐克,斯洛文尼亚,美国,挪威";
			
			String[] strArray = parm.split(",");
			for (String string : strArray) {
				list.add(string);
			}
			resList = songerMapper.findBysongerCountry(list);
		}else if("日本歌手".equals(songerCountry)){
			List list =new ArrayList<>();
			parm="日本";
			list.add(parm);
			resList = songerMapper.findBysongerCountry(list);
		}else if("韩国歌手".equals(songerCountry)){
			List list =new ArrayList<>();
			parm="韩国";
			list.add(parm);
			resList = songerMapper.findBysongerCountry(list);
		}else if("其他歌手".equals(songerCountry)){
			List list =new ArrayList<>();
			parm="未知";
			list.add(parm);
			resList = songerMapper.findBysongerCountry(list);
		}
		return resList;
	}
	
	//下架歌手
	@Override
	public void updateSongerStatusById(String songerid, Integer songerstatus) {
		Songer songer = new Songer(); 
		if(songerid!=null){
			songer.setSongerid(Integer.valueOf(songerid));
			songer.setSongerstatus(songerstatus);
			songerMapper.updateSongerStatusById(songer);
		}
		
	}
    
	//根据id查询歌手
	@Override
	public Songer findSongerById(String songerid) {
		Songer songer = new Songer();
		if (songerid!=null) {
			songer = songerMapper.selectByPrimaryKey(Integer.valueOf(songerid));
		}
		return songer;
	}

	//修改歌手信息
	@Override
	public void updateSonger(Songer songer) {
		if (songer!=null) {
			songerMapper.updateByPrimaryKeySelective(songer);
		}
	}
    
	//根据ID删除歌手
	@Override
	public void delSongerById(String songerid) {
		if (songerid!=null||!("".equals(songerid))) {
			songerMapper.deleteByPrimaryKey(Integer.valueOf(songerid));
		}
		
	}

	@Override
	public DataDetail findSongerDetailInfoById(String songerid) {
		DataDetail data = new DataDetail();
		if (songerid!=null) {
			Songer songer = songerMapper.selectByPrimaryKey(Integer.valueOf(songerid));
			List<Album> albumList = albumMapper.selectAlbumBySongerId(Integer.valueOf(songerid));
			List<Songs> songList = songsMapper.selectSongBySongerId(Integer.valueOf(songerid));
			data.setSonger(songer);
			data.setAlbumList(albumList);
			data.setSongList(songList);
		}
		return data;
	}
}
