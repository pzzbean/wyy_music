package com.wymusic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wymusic.base.SongListAndSongsDetail;
import com.wymusic.base.SongListDetail;
import com.wymusic.mapper.SongerMapper;
import com.wymusic.mapper.SonglistMapper;
import com.wymusic.mapper.SonglistandsongsMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.mapper.UserMapper;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.Songlist;
import com.wymusic.pojo.Songlistandsongs;
import com.wymusic.pojo.Songs;
import com.wymusic.service.SongListService;

@Service
public class SongListServiceImpl implements SongListService{
	
	@Autowired
	private SonglistMapper songlistMapper;
	
	@Autowired
	private SonglistandsongsMapper songlistandsongsMapper;
	
	@Autowired
	private UserMapper userMapper;
	
    @Autowired
    private SongsMapper songsMapper;
    
    @Autowired
    private SongerMapper songerMapper;
    
    
    //查询用户下的所有歌单
	@Override
	public SongListDetail findSongListByUserId(String userId) {
		//歌单的详细信息
		SongListDetail songListDetail =new SongListDetail();
		List<SongListAndSongsDetail> songListAndSongsDetailsList =new ArrayList<>();
		List<Songlist> songlistsList =new ArrayList<>();
		List list = new ArrayList<>();
		if (userId!=null||!("".equals(userId))) {
			//根据用户ID查询所有歌单
			songlistsList = songlistMapper.findSongListByUserId(Integer.valueOf(userId));
			System.out.println(songlistsList.size());
			//循环歌单 查询歌单下的所有歌曲信息
			if (songlistsList.size()!=0) {
				for (Songlist songlist : songlistsList) {
					//存放每张歌单下的歌曲
					SongListAndSongsDetail songListAndSongsDetail =new SongListAndSongsDetail();
					
					List<Songlistandsongs> SonglistandsongsList = songlistandsongsMapper
					.findSongBySongListId(songlist.getSonglistid());
					
					List<Songs> songsList = new ArrayList<>();
					for (Songlistandsongs songlistandsongs : SonglistandsongsList) {
						Songs songs = songsMapper.selectByPrimaryKey(songlistandsongs.getSongsid());
						songsList.add(songs);
					}
					songListAndSongsDetail.setSonglist(songlist);
					songListAndSongsDetail.setSongsList(songsList);
					songListAndSongsDetailsList.add(songListAndSongsDetail);
				}
				
			}
			songListDetail.setSongListAndSongsDetailsList(songListAndSongsDetailsList);
		}
		return songListDetail;
	}

	
	//根据歌单ID查询歌曲
	@Override
	public Songlist findSongListById(String songlistid) {
		Songlist songlist = new Songlist();
		if(songlistid!=null||!("".equals(songlistid))){
			songlist = songlistMapper.selectByPrimaryKey(Integer.valueOf(songlistid));
		}
		return songlist;
	}
	
	//根据歌单ID查询歌单下歌曲
	@Override
	public SongListAndSongsDetail findSongsBySonglistId(String songlistid) {
		Songlist songlist = new Songlist();
		SongListAndSongsDetail songListAndSongsDetail =new SongListAndSongsDetail();
		List<Songs> songsList = new ArrayList<>();
		List<Songer> songerList = new ArrayList<>();
		if(songlistid!=null||!("".equals(songlistid))){
			songlist = songlistMapper.selectByPrimaryKey(Integer.valueOf(songlistid));
			if (songlist!=null) {
				songListAndSongsDetail.setSonglist(songlist);
				List<Songlistandsongs> songlistandsongsList = songlistandsongsMapper.
						findSongBySongListId(songlist.getSonglistid());
				if (songlistandsongsList!=null) {
					Songs songs = new Songs();
					Songer songer = new Songer();
					for (Songlistandsongs songlistandsongs : songlistandsongsList) {
						songs = songsMapper.selectByPrimaryKey(songlistandsongs.getSongsid());
						songer = songerMapper.selectByPrimaryKey(songs.getSongerid());
						songsList.add(songs);
						songerList.add(songer);
					}
					songListAndSongsDetail.setSongsList(songsList);
					songListAndSongsDetail.setSongerList(songerList);
				}
			}
		}
		return songListAndSongsDetail;
	}


	@Override
	public void updateSonglist(Songlist songlist) {
		if (songlist!=null) {
			songlistMapper.updateByPrimaryKeySelective(songlist);
			Songlistandsongs songlistandsongs =new Songlistandsongs();
			songlistandsongs.setSonglistid(songlist.getSonglistid());
			songlistandsongs.setSonglistname(songlist.getSonglistname());
			songlistandsongsMapper.updateSonglistNameBysonglistId(songlistandsongs);
		}
	}


	@Override
	public void addSonglist(Songlist songlist) {
		if (songlist!=null) {
			songlistMapper.insertSelective(songlist);
		}
	}


	@Override
	public void updateStatusById(Songlist songlist) {
		if (songlist!=null) {
			songlistMapper.updateByPrimaryKeySelective(songlist);
		}
	}


	@Override
	public String addToSongList(String songlistid, String songid) {
		Songlistandsongs songlistandsongs =new Songlistandsongs();
		if(songlistid!=null||!("".equals(songlistid))){
			Songlist songlist =songlistMapper.selectByPrimaryKey(Integer.valueOf(songlistid));
			if (songlist!=null) {
				songlistandsongs.setSonglistid(songlist.getSonglistid());
				songlistandsongs.setSonglistname(songlist.getSonglistname());
			}
		}
		if(songid!=null||!("".equals(songid))){
			Songs songs = songsMapper.selectByPrimaryKey(Integer.valueOf(songid));
			
			if (songs!=null) {
				songlistandsongs.setSongsid(songs.getSongid());
				songlistandsongs.setSongsname(songs.getSongname());
			}
		}
		Songlistandsongs temp = songlistandsongsMapper.songsExist(songlistandsongs);
		if(temp!=null){
			return "歌曲已存在！";
		}else{
			songlistandsongsMapper.insertSelective(songlistandsongs);
			return "歌曲已添加！";
		}
	}
}
