package com.wymusic.service.impl;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.DataDetail;
import com.wymusic.mapper.AlbumMapper;
import com.wymusic.mapper.SongerMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.AlbumExample;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.Songs;
import com.wymusic.service.AlbumService;
import com.wymusic.vo.PageVo;

@Service
public class AlbumServiceImpl implements AlbumService{
	
	@Autowired
	private AlbumMapper albumMapper;
	
	@Autowired
	private SongerMapper songerMapper;
	
	@Autowired
	private SongsMapper songsMapper;
	
	//查询所有专辑
	@Override
	public List<Album> findAll() {
		AlbumExample example = new AlbumExample();
		List<Album> resList = albumMapper.selectByExampleWithBLOBs(example);
		return resList;
	}

	//分页查询
	@Override
	public List<Album> findByPage(String page, String pageSize,String publicArea) {
		int tempPageSize = Integer.valueOf(pageSize);
		int tempPage1 = Integer.valueOf(page)*tempPageSize-tempPageSize;
		
		PageVo pVo=new PageVo(tempPage1, tempPageSize, 0,publicArea);
		List<Album> resList = albumMapper.findByPage(pVo);
	
		return resList;
	}

	@Override
	public List<Album> findByPublicArea(String publicArea) {
		List<Album> resList = albumMapper.findByPublicArea(publicArea);
		return resList;
	}

	@Override
	public Album findAlbumById(String albumid) {
		Album album = new Album();
		if (albumid!=null||albumid!="") {
		  album = albumMapper.selectByPrimaryKey(Integer.valueOf(albumid));
		  return album;
		}
		return album;
	}
	
	//修改专辑信息
	@Override
	public void updateAlbum(Album album) {
		if(album!=null){
			album.setAlbumstatus(1);
			albumMapper.updateByPrimaryKeySelective(album);
		}
	}
	
	//上架或者下架专辑
	@Override
	public void updateAlbumStatusById(String albumid,Integer albumstatus) {
		Album album = new Album();
		if (albumid!=null) {
			album.setAlbumid(Integer.valueOf(albumid));
			album.setAlbumstatus(albumstatus);
			albumMapper.updateAlbumStatusById(album);
		}
		
	}
	
	@Override
	public void insertAlbum(Album album){
		if(album!=null){
			albumMapper.insertSelective(album);
		}
	}

	@Override
	public DataDetail findAlbumDetailInfoById(String albumid) {
		DataDetail data= new DataDetail();
		if (albumid!=null) {
			Album album = albumMapper.selectByPrimaryKey(Integer.valueOf(albumid));
			List<Songs> songList= songsMapper.selectSongByAlbumId(Integer.valueOf(albumid));
			data.setAlbum(album);
			data.setSongList(songList);
		}
		return data;
	}
	
}
