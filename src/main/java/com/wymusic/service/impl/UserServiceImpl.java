package com.wymusic.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wymusic.mapper.UserMapper;
import com.wymusic.pojo.User;
import com.wymusic.pojo.UserExample;
import com.wymusic.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserMapper userMapper;
	
	//注册用户
	@Override
	public int insertUser(User user) {
		int status = userMapper.insert(user);
		return status;
	}

	//验证用户是否存在
	@Override
	public User findUserByAccount(String account) {
		
		User user = userMapper.findUserByAccount(account);
		
		return user;
	}

	//验证手机是否已被注册
	@Override
	public boolean findUserByUserPhone(String userPhone) {
		
		List<User> userlist = userMapper.findUserByUserPhone(userPhone);
		
		if (0!=userlist.size()) {
			
			return true;
		}
		return false;
	}

	//查询所有用户
	@Override
	public List<User> findAllUser() {
		UserExample example=new UserExample();
		List<User> newUserList=new ArrayList();
		List<User> userlist = userMapper.selectByExample(example);
		for (User user : userlist) {
			User newUser= toNewUser(user); 
			newUserList.add(newUser);
		}
		return newUserList;
	}
    
	//通过Id查询用户
	@Override
	public User findUserById(Integer userId) {
		User user = userMapper.selectByPrimaryKey(userId);
		User newUser= toNewUser(user); 
		return newUser;
	}
	
	//转换新的user
	public User toNewUser(User user){
		User newUser =new User(user.getUserid(), user.getAccount(), user.getPassword(),
				user.getUserphone(), user.getUserstatus(),user.getUserauthority());
		
		if("".equals(user.getUsername())||""==user.getUsername()||user.getUsername()==null){
			newUser.setUsername("");
		}else{
			newUser.setUsername(user.getUsername());
		}
		
		if("".equals(user.getUseremail())||""==user.getUseremail()||user.getUseremail()==null){
			newUser.setUseremail("");
		}else{
			newUser.setUseremail(user.getUseremail());
		}
		
		if("".equals(user.getProvince())||""==user.getProvince()||user.getProvince()==null){
			newUser.setProvince("");
		}else{
			newUser.setProvince(user.getProvince());
		}
		
		if("".equals(user.getCity())||""==user.getCity()||user.getCity()==null){
			newUser.setCity("");
		}else{
			newUser.setCity(user.getCity());
		}
		
		if("".equals(user.getUserpicurl())||""==user.getUserpicurl()||user.getUserpicurl()==null){
			newUser.setUserpicurl("");
		}else{
			newUser.setUserpicurl(user.getUserpicurl());
		}
		
		if("".equals(user.getUsersex())||""==user.getUsersex()||user.getUsersex()==null){
			newUser.setUsersex("");
		}else{
			newUser.setUsersex(user.getUsersex());
		}
		
	    newUser.setCreatetime(user.getCreatetime());
	    
		return newUser;
	}

	//修改用户信息
	@Override
	public void updateUser(User user) {
		userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public void deleteUserById(Integer userid) {
		userMapper.deleteByPrimaryKey(userid);
	}

}
