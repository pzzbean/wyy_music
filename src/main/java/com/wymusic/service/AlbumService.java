package com.wymusic.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Album;

public interface AlbumService {
    public List<Album> findAll();
    
    public List<Album> findByPage(String page, String pageSize, String publicArea);
    
    public List<Album> findByPublicArea(String publicArea);
    
    public Album findAlbumById(String albumid);
    
    public void updateAlbum(Album album);
    
    public void updateAlbumStatusById(String albumid, Integer albumstatus);
    
    public void insertAlbum(Album album);
    
    public DataDetail findAlbumDetailInfoById(String albumid);
}
