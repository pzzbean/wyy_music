package com.wymusic.service;

import java.util.List;

import com.wymusic.base.CommentDataDetail;
import com.wymusic.pojo.Albumcomts;
import com.wymusic.pojo.Comments;

public interface CommentsService {
	//查询所有歌曲评论
	CommentDataDetail findAll(); 
	//更改歌曲评论状态
	void updateStatusById(String commentId, String status);
	
	Comments findCommentsById(String commentId);
	//查询所有专辑评论
	CommentDataDetail findAllAlbumComts();
	//更改专辑评论状态
	void updateALbumComtsStatusById(String commentId, String status);
	
	//根据歌曲id查询评论
	CommentDataDetail findCommentsBySongId(String songId);
	
	//根据专辑id查询评论
    CommentDataDetail findAlbumComtsByAlbumId(String albumId);
	
	//插入歌曲评论
	void addComments(String userid, String cotext, String songId);
	
	void addAlbumcomts(String userid, String cotext, String AlbumId);
}
