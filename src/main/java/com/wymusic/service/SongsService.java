package com.wymusic.service;


import com.wymusic.base.ApiResponse;
import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Songs;

public interface SongsService {
	
	public DataDetail findSongsDetailInfoById(String songsid);
	
	public DataDetail findSongsByIdList(String songidlist);
	
	public DataDetail findAll();
	
	public void updateSongsStatusById(String songsid, String songsStatus);
	
	public void updatasongInfo(String songPicUrl, String songername, String albumname, Songs song);
	
	public void addSongs(String songPicUrl, String songername, String albumname, Songs song);
	
	public void delSongById(String songid);
	
	public DataDetail selectBySongHotDesc();
	
	public DataDetail songsTopListByArea(String area);
}
