package com.wymusic.service;

import com.wymusic.base.SongListAndSongsDetail;
import com.wymusic.base.SongListDetail;
import com.wymusic.pojo.Songlist;

public interface SongListService {
	
	SongListDetail findSongListByUserId(String userId);
	
	Songlist findSongListById(String songlistid);
	
	void updateSonglist(Songlist songlist);
	
	void addSonglist(Songlist songlist);
	
	void updateStatusById(Songlist songlist);
	
	SongListAndSongsDetail findSongsBySonglistId(String songlistid);
	
	String  addToSongList(String songlistid, String songid);
}
