package com.wymusic.service;

import java.util.List;

import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Songer;
import com.wymusic.vo.PageVo;

public interface SongerService {
	
	public List<Songer> findAll();
	
	Songer findSongerByName(String songername);
	
	public void insertSonger(Songer songer);
	
	List<Songer> findByPage(String page, String pageSize, String songerCountry);
	
	List<Songer> findBysongerCountry(String songerCountry);
	
	public void updateSongerStatusById(String songerid, Integer songerstatus);
	
	Songer findSongerById(String songerid);
	
	public void updateSonger(Songer songer);
	
	public void delSongerById(String songerid);
	
	public DataDetail findSongerDetailInfoById(String songerid);
	
}
