package com.wymusic.service;

import java.util.List;

import com.wymusic.base.ApiResponse;
import com.wymusic.pojo.User;

public interface UserService {
	
    public int insertUser(User user);
    
    public User findUserByAccount(String account);
    
    public boolean findUserByUserPhone(String userPhone);
    
    public List<User> findAllUser();
    
    public User findUserById(Integer userId);
    
    public void updateUser(User user);
    
    public void deleteUserById(Integer userid);
}
