package com.wymusic.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.ApiResponse;
import com.wymusic.pojo.User;
import com.wymusic.service.UserService;

@Controller  
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	//用户登录
	@RequestMapping("/login")
	@ResponseBody
	public ApiResponse login(String account, String password, HttpServletRequest request){
		ApiResponse apiResponse=new ApiResponse();
		HttpSession session = request.getSession();
		if(account!=null&&password!=null){
			User user = userService.findUserByAccount(account);//查询用户是否存在
			if(user!=null){
				if(account.equals(user.getAccount())&&password.equals(user.getPassword())){
					if(user.getUserstatus() != 3){ //判断用户状态是否已删除
						if (user.getUserauthority() == 1) { //判断用户权限是否为管理员
							user.setUserstatus(1);
							session.setAttribute("admin", user);//保存管理员信息
							userService.updateUser(user);
							session.setAttribute("authority", "1");
							apiResponse.setStr("admin/index");
							apiResponse.setMassage("登录成功！");
						}else if(user.getUserauthority() == 0){ //判断权限是否为用户
							session.setAttribute("user", user); //保存用户信息
							user.setUserstatus(1);
							userService.updateUser(user);
							session.setAttribute("authority", "0"); 
							apiResponse.setStr("index.html");
							apiResponse.setMassage("登录成功！");
						}else{   //登录失败
							apiResponse.setStr("login.html");
							apiResponse.setMassage("登录信息错误！！");
						}
					}else{
						apiResponse.setStr("login.html"); 
						apiResponse.setMassage("用户已失效，需要重新激活！");
					}
				}
			}else{
				apiResponse.setStr("login.html"); 
				apiResponse.setMassage("登录信息错误！");
			}
		}
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//验证用户名是否存在
	@RequestMapping("/validataAccount")
	@ResponseBody
	public ApiResponse validataAccount(String account){
		ApiResponse apiResponse=new ApiResponse();
		User user = userService.findUserByAccount(account);
		boolean flag = false;
		if(user!=null){
			apiResponse.setMassage("用户已存在！");
			flag = true;
		}else{
			apiResponse.setMassage("账号可以使用！");
		}
		apiResponse.setCode(200);
		apiResponse.setMore(flag);
		return apiResponse;
	}
	
    //验证手机是否已被注册
	@RequestMapping("/validataUserPhone")
	@ResponseBody
	public ApiResponse validataUserPhone(String userPhone){
		ApiResponse apiResponse=new ApiResponse();
		boolean flag = userService.findUserByUserPhone(userPhone);
		if(flag){
			apiResponse.setMassage("该手机号码已被注册！");
		}else{
			apiResponse.setMassage("手机号码可以使用！");
		}
		apiResponse.setCode(200);
		apiResponse.setMore(flag);
		return apiResponse;
	}
	
	//注册用户
	@RequestMapping("/insertUser")
	@ResponseBody
	public ApiResponse insertUser(String account,String userPhone ,String password){
		ApiResponse apiResponse=new ApiResponse();
		User user=new User(account, password, userPhone);//接收注册用户信息
		if(userService.insertUser(user)!=-1){  //向数据库插入用户
			apiResponse.setCode(200);
			apiResponse.setMassage("恭喜您注册成功！");
		}else{
			apiResponse.setCode(202);
			apiResponse.setMassage("注册失败！");
		}
		return apiResponse;
	}
	
	//查询所有用户
	@RequestMapping("/userList")
	@ResponseBody
	public ApiResponse findAllUser(){
		ApiResponse apiResponse=new ApiResponse();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(userService.findAllUser());
		return apiResponse;
	}
	
	//根据ID查询用户
	@RequestMapping("/findUserById")
	@ResponseBody
	public ApiResponse findUserById(Integer userId){
		ApiResponse apiResponse=new ApiResponse();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(userService.findUserById(userId));
		return apiResponse;
	}
	
	//管理员修改用户信息
	@RequestMapping(value="/updateUser",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateUser(String userPicUrl ,User user){
		System.out.println(user);
		ApiResponse apiResponse=new ApiResponse();
		//接收用户图片地址
		if (!("".equals(userPicUrl))) { 
			//把图片名称保存到数据库
			user.setUserpicurl(userPicUrl);
		}
		//更新用户状态
		userService.updateUser(user);
		//将更新后的用户信息返回前台
		apiResponse.setData(userService.findUserById(user.getUserid()));
		apiResponse.setMassage("修改成功！");
		apiResponse.setCode(200);
		return apiResponse;
	}
	

	//用户修改用户信息
	@RequestMapping(value="/updateUser2",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateUser2(String userPicUrl ,User user,HttpServletRequest request){
		ApiResponse apiResponse=new ApiResponse();
		//接收用户图片地址
		if (!("".equals(userPicUrl))) { 
			System.out.println(".....");
			//把图片名称保存到数据库
			user.setUserpicurl(userPicUrl);
		}
		//更新用户状态
		userService.updateUser(user);
		//用户修改成功后更新session信息
		User newuser =  userService.findUserById(user.getUserid());
		HttpSession session = request.getSession();
		session.removeAttribute("user");
		session.setAttribute("user", newuser);
		apiResponse.setMassage("修改成功！");
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	
	//修改用户密码
	@RequestMapping(value="/updateUserPassword",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse updateUserPassword(String userid,String newpassword){
		User user =new User();
		user.setUserid(Integer.valueOf(userid)); //接收用户信息
		user.setPassword(newpassword);  //接收新密码
		userService.updateUser(user);  //设置新密码
		ApiResponse apiResponse=new ApiResponse();
		apiResponse.setCode(200);
		apiResponse.setMassage("修改成功");
		return apiResponse;
	}
	
	//删除用户
    @RequestMapping(value="/delUser",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse delUser(String userid){
		System.out.println(userid);
		User user =new User();
		user.setUserid(Integer.valueOf(userid));//接收用户信息
		user.setUserstatus(3);
		userService.updateUser(user);  //根据信息修改用户状态
		ApiResponse apiResponse=new ApiResponse(); 
		apiResponse.setCode(200);
		apiResponse.setMassage("该用户已经删除！");//返回提示信息
		return apiResponse;
	}
    
    //还原用户
    @RequestMapping(value="/recoverUser",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse recoverUser(String userid){
		User user =new User();
		user.setUserid(Integer.valueOf(userid)); //接收用户信息
		user.setUserstatus(0);
		userService.updateUser(user);  //更改用户信息
		ApiResponse apiResponse=new ApiResponse();
		apiResponse.setCode(200);
		apiResponse.setMassage("还原用户成功！");
		return apiResponse;
	}
    
    //将用户从数据库删除
    @RequestMapping(value="/delUserFromData",method = RequestMethod.POST)
	@ResponseBody
	public ApiResponse delUserFromData(String userid){
		User user =new User();
		ApiResponse apiResponse=new ApiResponse();
		userService.deleteUserById(Integer.valueOf(userid));
		apiResponse.setCode(200);
		apiResponse.setMassage("该用户已删除！");
		return apiResponse;
	}
    
    //修改用户权限
  	@RequestMapping(value="/updateUserAuthority",method = RequestMethod.POST)
  	@ResponseBody
  	public ApiResponse updateUserAuthority(String userid,String authority){
  		User user =new User();
  		user.setUserid(Integer.valueOf(userid)); //接收用户信息
  		user.setUserauthority(Integer.valueOf(authority)); 
  		userService.updateUser(user); //修改用户权限
  		ApiResponse apiResponse=new ApiResponse();
  		apiResponse.setCode(200);
  		apiResponse.setMassage("权限修改成功");
  		return apiResponse;
  	}
  	
    //回显图片
  	@RequestMapping("/showUserPic")
  	@ResponseBody
  	public ApiResponse showSongerPic(@RequestParam("userPicfile") MultipartFile userPicfile){
  		ApiResponse apiResponse = new ApiResponse();
  		//获取图片路径名称
  		String fileStr=userPicfile.getOriginalFilename();
  		if(!fileStr.isEmpty()){
  			//使用随机字母生成新的路径
  			String newFileName=UUID.randomUUID().toString()+fileStr.substring(fileStr.lastIndexOf('.'));
  			
  			//把图片保存到硬盘
  			try {
  				userPicfile.transferTo(new File("D:\\image\\"+newFileName));
  			} catch (Exception e) {
  				e.printStackTrace();
  			}
  			//把图片回显
  			apiResponse.setStr(newFileName);
  			apiResponse.setCode(200);
  		}
  		return apiResponse;
  	}
  	
  	//加载用户
	@RequestMapping("/loaduser")
	@ResponseBody
	public ApiResponse loaduser(HttpServletRequest request,HttpServletResponse response){
		ApiResponse apiResponse = new ApiResponse();
		String authority = (String)request.getSession().getAttribute("authority");
		if("1"==authority||"1".equals(authority)){
			User user = (User)request.getSession().getAttribute("admin");
			apiResponse.setCode(200);
			apiResponse.setData(user);
		}else if("0"==authority||"0".equals(authority)){
			User user = (User)request.getSession().getAttribute("user");
			apiResponse.setCode(200);
			apiResponse.setData(user);
		}else{
			apiResponse.setCode(200);
			apiResponse.setData(null);
		}
		return apiResponse;
	}
	
	//用户退出
	@RequestMapping("/removeSession")
	@ResponseBody
	public ApiResponse removeSession(HttpServletRequest request,HttpServletResponse response){
		ApiResponse apiResponse = new ApiResponse();
		String authority = (String)request.getSession().getAttribute("authority");
		if("1"==authority||"1".equals(authority)){
			request.getSession().removeAttribute("admin");
			apiResponse.setCode(200);
		}else if("0"==authority||"0".equals(authority)){
			request.getSession().removeAttribute("user");
			apiResponse.setCode(200);
		}else{
			apiResponse.setCode(200);
			apiResponse.setData(null);
		}
		return apiResponse;
	}

}
