package com.wymusic.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.ApiResponse;
import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.service.SongerService;

@Controller
@RequestMapping("/songer")
public class SongerController {
	
	@Autowired
	private SongerService songerService;
	
	//查询所有歌手
	@RequestMapping("/findAll")
	@ResponseBody
	public ApiResponse findAll(){
		ApiResponse apiResponse = new ApiResponse();
		List<Songer> songerList = songerService.findAll();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(songerList); //返回查询结果
		return apiResponse;
		
	}
	
	//按热门程度查询歌手
	@RequestMapping("/findAllByStatusDesc")
	@ResponseBody
	public ApiResponse findAllByStatusDesc(){
		ApiResponse apiResponse = new ApiResponse();
		
		List<Songer> songerList = songerService.findAll();
		
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(songerList);
		return apiResponse;
	}
	
	//歌手分页
	@RequestMapping("/findByPage")
	@ResponseBody
	public ApiResponse findByPage(String page,String pageSize,String songerCountry){
		ApiResponse apiResponse = new ApiResponse();
		List<Songer> newSongerList = songerService.findByPage(page, pageSize, songerCountry);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(newSongerList);
		return apiResponse;
	}
	
	//按国籍分类歌手
	@RequestMapping("/findBysongerCountry")
	@ResponseBody
	public ApiResponse findBysongerCountry(String songerCountry){
		System.out.println("====>"+songerCountry);
		ApiResponse apiResponse = new ApiResponse();
		List<Songer> newSongerList = songerService.findBysongerCountry(songerCountry);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(newSongerList);
		return apiResponse;
	}
	
	
	//下架歌手
	@RequestMapping("/updateSongerStatusById")
	@ResponseBody
	public ApiResponse updateSongerStatusById(String songerid){
		ApiResponse apiResponse = new ApiResponse();
		songerService.updateSongerStatusById(songerid, 0); //修改歌手状态
		apiResponse.setMassage("此歌手已下架！");//返回下架信息
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//根据ID查询歌手
	@RequestMapping("/findSongerById")
	@ResponseBody
	public ApiResponse findSongerById(String songerid){
		ApiResponse apiResponse = new ApiResponse();
		Songer songer = songerService.findSongerById(songerid);
		apiResponse.setData(songer);
		apiResponse.setMassage("查询成功！");
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//编辑歌手信息
	@RequestMapping("/updateSonger")
	@ResponseBody
	public ApiResponse updateSonger(Songer songer,String songerPicUrl){
		ApiResponse apiResponse = new ApiResponse();
		//获取图片名称
		if (songerPicUrl!=null||!("".equals(songerPicUrl))) {
			songer.setSongerpicurl(songerPicUrl);
		}
		//接收歌手信息并更新歌手信息
		songerService.updateSonger(songer);
		apiResponse.setCode(200);
		apiResponse.setMassage("修改信息成功！");
		//返回歌手信息
		apiResponse.setData(songerService.findSongerById(songer.getSongerid()+""));
		return apiResponse;
	}
	
	//回显图片
	@RequestMapping("/showSongerPic")
	@ResponseBody
	public ApiResponse showSongerPic(@RequestParam("songerPicfile") MultipartFile userPicfile){
		ApiResponse apiResponse = new ApiResponse();
		//获取图片路径名称
		String fileStr=userPicfile.getOriginalFilename();
		if(!fileStr.isEmpty()){
			//使用随机字母生成新的路径
			String newFileName=UUID.randomUUID().toString()+fileStr.substring(fileStr.lastIndexOf('.'));
			
			//把图片保存到硬盘
			try {
				userPicfile.transferTo(new File("D:\\image\\"+newFileName));
			} catch (Exception e) {
				e.printStackTrace();
			}
			//把图片回显
			apiResponse.setStr(newFileName);
			apiResponse.setCode(200);
		}
		return apiResponse;
	}
	
	//添加歌手
	@RequestMapping("/addSonger")
	@ResponseBody
	public ApiResponse showSongerPic(Songer songer,String picUrl){
		ApiResponse apiResponse = new ApiResponse();
		if(picUrl!=null||!("".equals(picUrl))){
			songer.setSongerpicurl(picUrl); //接收歌曲图片
		}
		songerService.insertSonger(songer); //接收歌手信息并插入歌手信息
		apiResponse.setCode(200);
		apiResponse.setMassage("添加成功!");
		return apiResponse;
	}
	
	//入驻歌手（还原歌手）
	@RequestMapping("/upSonger")
	@ResponseBody
	public ApiResponse upSonger(String songerid){
		ApiResponse apiResponse = new ApiResponse();
		songerService.updateSongerStatusById(songerid, 1);
		apiResponse.setMassage("入驻成功！");
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//根据ID删除歌手
	@RequestMapping("/delSongerById")
	@ResponseBody
	public ApiResponse delSongerById(String songerid){
		ApiResponse apiResponse = new ApiResponse();
		songerService.delSongerById(songerid);//接收歌手新消息
		apiResponse.setMassage("删除成功！");//返回删除信息
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//根据ID查询歌手详细信息
	@RequestMapping("/findSongerDetailInfoById")
	@ResponseBody
	public ApiResponse findSongerDetailInfoById(String songerid){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = songerService.findSongerDetailInfoById(songerid);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(data);
		apiResponse.setCode(200);
		return apiResponse;
	}
	
}
