package com.wymusic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wymusic.base.ApiResponse;
import com.wymusic.base.CommentDataDetail;
import com.wymusic.pojo.Comments;
import com.wymusic.pojo.User;
import com.wymusic.service.CommentsService;

@Controller
@RequestMapping("/comments")
public class CommentsController {
	
	@Autowired
	private CommentsService commentsService;
	
	//查询所有歌曲评论
	@RequestMapping("/findAll")
	@ResponseBody
	public ApiResponse findAll(){
		ApiResponse apiResponse =new ApiResponse();
		CommentDataDetail commentDataDetail = commentsService.findAll();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(commentDataDetail);
		return apiResponse;
	}
	
	//查询所有专辑评论
	@RequestMapping("/findAllAlbumComts")
	@ResponseBody
	public ApiResponse findAllAlbumComts(){
		ApiResponse apiResponse =new ApiResponse();
		CommentDataDetail commentDataDetail = commentsService.findAllAlbumComts();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(commentDataDetail);
		return apiResponse;
	}
	
	//根据评论ID查询评论
	@RequestMapping("findCommentsById")
	@ResponseBody
	public ApiResponse findCommentsById(String commentId){
		ApiResponse apiResponse =new ApiResponse();
		Comments comments = commentsService.findCommentsById(commentId);
		apiResponse.setData(comments);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//更改歌曲评论状态
	@RequestMapping("updateStatusById")
	@ResponseBody
	public ApiResponse updateStatusById(String commentId,String status){
		ApiResponse apiResponse =new ApiResponse();
		commentsService.updateStatusById(commentId, status);
		apiResponse.setCode(200);
		apiResponse.setMassage("审核成功");
		return apiResponse;
	}
	
	//更改专辑评论状态
	@RequestMapping("updateALbumComtsStatusById")
	@ResponseBody
	public ApiResponse updateALbumComtsStatusById(String commentId,String status){
		ApiResponse apiResponse =new ApiResponse();
		commentsService.updateALbumComtsStatusById(commentId, status);
		apiResponse.setCode(200);
		apiResponse.setMassage("审核成功");
		return apiResponse;
	}
	
	//根据歌曲ID查询评论
	@RequestMapping("findCommentsBySongId")
	@ResponseBody
	public ApiResponse findCommentsBySongId(String songId){
		ApiResponse apiResponse =new ApiResponse();
		CommentDataDetail commentDataDetail = commentsService.findCommentsBySongId(songId);
		apiResponse.setCode(200);
		apiResponse.setData(commentDataDetail);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//添加歌曲评论
	@RequestMapping("/addComments")
	@ResponseBody
	public ApiResponse addComments(String userid,String cotext,String songId){
		ApiResponse apiResponse =new ApiResponse();
		if(userid!=null){  //判断用户是否为空
			if (songId!=null) { //判断歌曲信息是否为空
				if(cotext!=null||!("".equals(cotext))){ //判断评论信息是否为空
					commentsService.addComments(userid, cotext, songId);//添加评论
				}
			}
		}
		apiResponse.setMassage("发表评论成功！");//提示评论信息
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//根据专辑ID查询评论
	@RequestMapping("findAlbumComtsByAlbumId")
	@ResponseBody
	public ApiResponse findAlbumComtsByAlbumId(String albumId){
		ApiResponse apiResponse =new ApiResponse();
		CommentDataDetail commentDataDetail = commentsService.findAlbumComtsByAlbumId(albumId);
		apiResponse.setCode(200);
		apiResponse.setData(commentDataDetail);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//添加专辑评论
	@RequestMapping("/addAlbumcomts")
	@ResponseBody
	public ApiResponse addAlbumcomts(String userid,String cotext,String AlbumId){
		ApiResponse apiResponse =new ApiResponse();
		if(userid!=null){
			if (AlbumId!=null) {
				if(cotext!=null||!("".equals(cotext))){
					commentsService.addAlbumcomts(userid, cotext, AlbumId);
				}
			}
		}
		apiResponse.setMassage("发表评论成功！");
		apiResponse.setCode(200);
		return apiResponse;
	}
}
