package com.wymusic.controller;

import java.io.File;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.ApiResponse;
import com.wymusic.base.SongListAndSongsDetail;
import com.wymusic.base.SongListDetail;
import com.wymusic.pojo.Songlist;
import com.wymusic.pojo.User;
import com.wymusic.service.SongListService;

@Controller
@RequestMapping("/songlist")
public class SongListController {
	@Autowired
	private SongListService songListService;
	
	//查询用户所有歌单信息
	@RequestMapping("/findSongListByUserId")
	@ResponseBody
	public ApiResponse findSongListByUserId(String userId){
		ApiResponse apiResponse = new ApiResponse();
		if(!("".equals(userId))){
			SongListDetail songListDetail = songListService.findSongListByUserId(userId);
			apiResponse.setData(songListDetail);
		}
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//根据歌单id查询歌单
	@RequestMapping("/findSongListById")
	@ResponseBody
	public ApiResponse findSongListById(String songlistid){
		ApiResponse apiResponse = new ApiResponse();
		if(!("".equals(songlistid))){
			Songlist songlist = songListService.findSongListById(songlistid); 
			apiResponse.setData(songlist);
		}
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//根据歌单ID查询歌单下歌曲
	@RequestMapping("/findSongsBySonglistId")
	@ResponseBody
	public ApiResponse findSongsBySonglistId(String songlistid){
		ApiResponse apiResponse = new ApiResponse();
		if(!("".equals(songlistid))){
			SongListAndSongsDetail songListAndSongsDetail = songListService.findSongsBySonglistId(songlistid);
			apiResponse.setData(songListAndSongsDetail);
		}
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		return apiResponse;
	}
	
	//回显图片
  	@RequestMapping("/showUserPic")
  	@ResponseBody
  	public ApiResponse showSongerPic(@RequestParam("songlistPicfile") MultipartFile userPicfile){
  		ApiResponse apiResponse = new ApiResponse();
  		//获取图片路径名称
  		String fileStr=userPicfile.getOriginalFilename();
  		if(!fileStr.isEmpty()){
  			//使用随机字母生成新的路径
  			String newFileName=UUID.randomUUID().toString()+fileStr.substring(fileStr.lastIndexOf('.'));
  			
  			//把图片保存到硬盘
  			try {
  				userPicfile.transferTo(new File("D:\\image\\"+newFileName));
  			} catch (Exception e) {
  				e.printStackTrace();
  			}
  			//把图片回显
  			apiResponse.setStr(newFileName);
  			apiResponse.setCode(200);
  		}
  		return apiResponse;
  	}
  	
    //修改歌单信息
  	@RequestMapping(value="/updateSonglist",method = RequestMethod.POST)
  	@ResponseBody
  	public ApiResponse updateSonglist(String songlistPicUrl ,Songlist songlist){
  		ApiResponse apiResponse=new ApiResponse();
  		//接收歌单图片地址
  		if (!("".equals(songlistPicUrl))) { 
  			//把图片名称保存到数据库
  			songlist.setSonglistpic(songlistPicUrl);
  		}
  		//更新歌单信息
  		songListService.updateSonglist(songlist);
  		//将更新后的用户信息返回前台
  		apiResponse.setData(songListService.findSongListById(String.valueOf(songlist.getSonglistid())));
  		apiResponse.setMassage("修改成功！");
  		apiResponse.setCode(200);
  		return apiResponse;
  	}
  	
    //添加歌单
  	@RequestMapping(value="/addSonglist",method = RequestMethod.POST)
  	@ResponseBody
  	public ApiResponse addSonglist(String add_songlistPicUrl ,Songlist songlist){
  		ApiResponse apiResponse=new ApiResponse();
  		//接收歌单图片地址
  		if (!("".equals(add_songlistPicUrl))) { 
  			//把图片名称保存到数据库
  			songlist.setSonglistpic(add_songlistPicUrl);
  		}
  		//添加歌单信息
  		songListService.addSonglist(songlist);
  		//将更新后的用户信息返回前台
  		apiResponse.setMassage("添加歌单成功！");
  		apiResponse.setCode(200);
  		return apiResponse;
  	}
  	
  	//更改歌单状态
  	@RequestMapping(value="/updateStatusById",method = RequestMethod.POST)
  	@ResponseBody
  	public ApiResponse updateStatusById(String songlistid ,String status){
  		ApiResponse apiResponse=new ApiResponse();
  		Songlist songlist=new Songlist();
  		if (!("".equals(songlistid))&&!("".equals(status))) { 
  			songlist.setSonglistid(Integer.valueOf(songlistid));
  			songlist.setSongliststatus(Integer.valueOf(status));
  		}
  		songListService.updateStatusById(songlist);
  		apiResponse.setMassage("删除成功！");
  		apiResponse.setCode(200);
  		return apiResponse;
  	}
  	
  	//把歌曲添加到歌单
  	@RequestMapping(value="/addToSongList",method = RequestMethod.POST)
  	@ResponseBody
  	public ApiResponse addToSongList(String songlistid ,String songid){
  		ApiResponse apiResponse=new ApiResponse();
  		String massage = songListService.addToSongList(songlistid, songid);
  		apiResponse.setMassage(massage);
  		apiResponse.setCode(200);
  		return apiResponse;
  	}
}
