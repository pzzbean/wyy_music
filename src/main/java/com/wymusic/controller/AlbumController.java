package com.wymusic.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.ApiResponse;
import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.service.AlbumService;
import com.wymusic.service.SongerService;

@Controller
@RequestMapping("/album")
public class AlbumController {
	
	@Autowired
	private AlbumService albumService;
	
	@Autowired
	private SongerService songerService;
	
	//查询所有专辑
	@RequestMapping("/findAll")
	@ResponseBody
	public ApiResponse findAll(){
		ApiResponse apiResponse = new ApiResponse();
		List<Album> albumList = new ArrayList<Album>();
		List<Album> resList = albumService.findAll();
		for (Album album : resList) {
			if(album.getAlbumstatus()== 1){
				albumList.add(album);
			}
		}
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		
		apiResponse.setData(albumList);
		return apiResponse;
	}
	

	//分页查询专辑
	@RequestMapping("/findByPage")
	@ResponseBody
	public ApiResponse findByPage(String page,String pageSize,String publicArea){
		ApiResponse apiResponse = new ApiResponse();
		if(page!=null){
			if("全部"==publicArea||"全部".equals(publicArea)){
				List<Album> resList = albumService.findByPage(page, pageSize,null);
				apiResponse.setData(resList);
			}else if("其他"==publicArea||"其他".equals(publicArea)){
				publicArea = "未知";
				List<Album> resList = albumService.findByPage(page, pageSize,publicArea);
				apiResponse.setData(resList);
			}else{
				List<Album> resList = albumService.findByPage(page, pageSize,publicArea);
				apiResponse.setData(resList);
			}
			apiResponse.setCode(200);
			apiResponse.setMassage("查询成功！");
		}else{
			apiResponse.setMassage("参数异常！");
		}
		return apiResponse;
	}
	
	//专辑分类
	@RequestMapping("/findByPublicArea")
	@ResponseBody
	public ApiResponse findByPublicArea(String publicArea){
		ApiResponse apiResponse = new ApiResponse();
		List<Album> albumList = new ArrayList<Album>();
		if(publicArea!=null){
			if("全部"==publicArea||"全部".equals(publicArea)){
				List<Album> resList = albumService.findAll();
				for (Album album : resList) {
					if(album.getAlbumstatus()== 1){
						albumList.add(album);
					}
				}
				apiResponse.setData(albumList);
			}else if("其他"==publicArea||"其他".equals(publicArea)){
				publicArea = "未知";
				List<Album> resList = albumService.findByPublicArea(publicArea);
				for (Album album : resList) {
					if(album.getAlbumstatus()== 1){
						albumList.add(album);
					}
				}
				apiResponse.setData(albumList);
			}else{
				List<Album> resList = albumService.findByPublicArea(publicArea);
				for (Album album : resList) {
					if(album.getAlbumstatus()== 1){
						albumList.add(album);
					}
				}
				apiResponse.setData(albumList);
			}
			apiResponse.setCode(200);
			apiResponse.setMassage("查询成功！");
		}else{
			apiResponse.setMassage("参数异常！");
		}
		return apiResponse;
	}
	
	//查询所有专辑
	@RequestMapping("/findAll2")
	@ResponseBody
	public ApiResponse findAll2(){
		ApiResponse apiResponse = new ApiResponse();
		List<Album> resList = albumService.findAll();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功！");
		apiResponse.setData(resList);
		return apiResponse;
	}
	
	//根据id查询专辑
	@RequestMapping("/findAlbumById")
	@ResponseBody
	public ApiResponse findAlbumById(String albumid){
		ApiResponse apiResponse = new ApiResponse();
		Album album =albumService.findAlbumById(albumid);
		if(album!=null){
			apiResponse.setMassage("查询成功！");
			apiResponse.setData(album);
		}else{
			apiResponse.setMassage("查询失败！");
			apiResponse.setData(null);
		}
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//修改专辑信息
	@RequestMapping("/updateAlbum")
	@ResponseBody
	public ApiResponse updateAlbum(String albumPicUrl, String publicTime,Album album){
		ApiResponse apiResponse = new ApiResponse();
		if(album.getSongername()!=null){  //判断专辑名字是否为空
			//查询专辑是否存在
			Songer songer = songerService.findSongerByName(album.getSongername()); 
			if(songer!=null){
				album.setSongerid(songer.getSongerid());
				album.setSongername(songer.getSongername());//设置专辑所属歌手
			}else{
				Songer tempSonger =new Songer();
				tempSonger.setSongername(album.getSongername());
				tempSonger.setSongerstatus(1);
				songerService.insertSonger(tempSonger);
			}
		}
		if(publicTime!=null||publicTime!=""){ //专辑修改时间是否为空
			try {
				album.setPublictime(new SimpleDateFormat("yyyy-MM-dd").parse(publicTime));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(album.getAlbumhot()==null){  //专辑点击量是否为空
			album.setAlbumhot(0);
		}
		if(albumPicUrl!=null||("".equals(albumPicUrl))){ //判断专辑图片是否为空
			//把图片名称保存到数据库
			album.setAlbumoverurl(albumPicUrl);
		}
		albumService.updateAlbum(album); //修改专辑信息
		apiResponse.setCode(200);
		apiResponse.setMassage("修改信息成功！");
		//返回专辑信息
		apiResponse.setData(albumService.findAlbumById(String.valueOf(album.getAlbumid()))); 
		return apiResponse;
	}
	
	//下架专辑
	@RequestMapping("/updateAlbumStatusById")
	@ResponseBody
	public ApiResponse updateAlbumStatusById(String albumid){
		ApiResponse apiResponse = new ApiResponse();
		albumService.updateAlbumStatusById(albumid,2); //接收专辑信息并修改专辑状态
		apiResponse.setMassage("此专辑已下架！");//返回提示信息
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//上架专辑
	@RequestMapping("/upAlbum")
	@ResponseBody
	public ApiResponse upAlbum(String albumid){
		ApiResponse apiResponse = new ApiResponse();
		albumService.updateAlbumStatusById(albumid,1);
		apiResponse.setMassage("此专辑已上架！");
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//删除专辑
	@RequestMapping("/delAlbum")
	@ResponseBody
	public ApiResponse delAlbum(String albumid){
		ApiResponse apiResponse = new ApiResponse();
		albumService.updateAlbumStatusById(albumid,0);//接收专辑信息和修改专辑状态
		apiResponse.setMassage("此专辑已删除！");//返回提示信息
		apiResponse.setCode(200);
		return apiResponse;
	}
	
	//添加专辑信息
	@RequestMapping("/addAlbum")
	@ResponseBody
	public ApiResponse addAlbum(String albumPicUrl,String publicTime,Album album){
		ApiResponse apiResponse = new ApiResponse();
		System.out.println("----->"+album);
		//判断专辑名称是否为空
		if(album.getAlbumname()==null||""==album.getAlbumname()||"".equals(album.getAlbumname())){
			apiResponse.setCode(606);
			apiResponse.setMassage("请填专辑的名称！");
			return apiResponse;
		}
		
		//判断歌手姓名是否为空
		if(album.getSongername()==null||""==album.getSongername()||"".equals(album.getSongername())){
			apiResponse.setCode(606);
			apiResponse.setMassage("请填写此专辑的歌手！");
			return apiResponse;
		}else{
			Songer songer = songerService.findSongerByName(album.getSongername());
			if(songer!=null){
				album.setSongerid(songer.getSongerid());
				album.setSongername(songer.getSongername());
			}else{  //如果歌手不在数据库则新添加相应歌手
				Songer tempSonger =new Songer();
				tempSonger.setSongername(album.getSongername());
				tempSonger.setSongerstatus(1);
				songerService.insertSonger(tempSonger);
			}
		}
		
		if(publicTime==null||""==publicTime||"".equals(publicTime)){
			album.setPublictime(new Date());
		}else{
			try {
				album.setPublictime(new SimpleDateFormat("yyyy-MM-dd").parse(publicTime));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(album.getAlbumhot()==null){
			album.setAlbumhot(0);
		}
		if(albumPicUrl!=null||("".equals(albumPicUrl))){
			//把图片名称保存到数据库
			album.setAlbumoverurl(albumPicUrl);
		}
		albumService.insertAlbum(album);
		apiResponse.setCode(200);
		apiResponse.setMassage("添加成功！");
		return apiResponse;
	}
	
	//回显图片
	@RequestMapping("/showAlbumerPic")
	@ResponseBody
	public ApiResponse showAlbumerPic(@RequestParam("albumPicfile") MultipartFile userPicfile){
		ApiResponse apiResponse = new ApiResponse();
		//获取图片路径名称
		String fileStr=userPicfile.getOriginalFilename();
		if(!fileStr.isEmpty()){
			//使用随机字母生成新的路径
			String newFileName=UUID.randomUUID().toString()+fileStr.substring(fileStr.lastIndexOf('.'));
			
			//把图片保存到硬盘
			try {
				userPicfile.transferTo(new File("D:\\image\\"+newFileName));
			} catch (Exception e) {
				e.printStackTrace();
			}
			//把图片回显
			apiResponse.setStr(newFileName);
			apiResponse.setCode(200);
		}
		return apiResponse;
	}
	
	@RequestMapping("/findAlbumDetailInfoById")
	@ResponseBody
	public ApiResponse findAlbumDetailInfoById(String albumid){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = albumService.findAlbumDetailInfoById(albumid);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功!");
		apiResponse.setData(data);
		return apiResponse;
	}
}
