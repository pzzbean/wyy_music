package com.wymusic.controller;

import java.io.File;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.wymusic.base.ApiResponse;
import com.wymusic.base.DataDetail;
import com.wymusic.pojo.Songs;
import com.wymusic.service.SongsService;

@Controller
@RequestMapping("/songs")
public class SongsController {
	
	@Autowired
	SongsService songsService;
	
	//根据歌曲id查询歌曲详细信息
	@RequestMapping("/findSongsDetailInfoById")
	@ResponseBody
	public ApiResponse findSongsDetailInfoById(String songsid){
		ApiResponse apiResponse = new ApiResponse();
		//接收歌曲信息并查询歌曲嘻嘻
		DataDetail data = songsService.findSongsDetailInfoById(songsid); 
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(data); //返回歌曲信息
		return apiResponse;
	}
	
	//根据歌曲id数组查询对应的歌曲信息
	@RequestMapping("/findSongsByIdList")
	@ResponseBody
	public ApiResponse findSongsByIdList(String songidlist){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = songsService.findSongsByIdList(songidlist);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(data);
		return apiResponse;
	}
	
	//查询全部歌曲信息
	@RequestMapping("/findAll")
	@ResponseBody
	public ApiResponse findAll(){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = songsService.findAll();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(data);//返回查询结果
		return apiResponse;
	}
	
	//更新歌曲状态
	@RequestMapping("/updateSongsStatusById")
	@ResponseBody
	public ApiResponse updateSongsStatusById(String songsid,String songsStatus){
		ApiResponse apiResponse = new ApiResponse();
		//接收歌曲信息和更改歌曲状态
		songsService.updateSongsStatusById(songsid, songsStatus);
		apiResponse.setCode(200);
		//返回下架提示信息
		if(Integer.valueOf(songsStatus)==0){
			apiResponse.setMassage("下架成功"); 
		}else{
			apiResponse.setMassage("歌曲已上架");
		}
		return apiResponse;
	}
	
	//歌曲图片回显
	@RequestMapping("/showSongsPic")
	@ResponseBody
	public ApiResponse showSongsPic(@RequestParam("songPicfile") MultipartFile songPicfile){
		ApiResponse apiResponse = new ApiResponse();
		//获取图片路径名称
		String fileStr=songPicfile.getOriginalFilename();
		if(!fileStr.isEmpty()){
			//使用随机字母生成新的路径
			String newFileName=UUID.randomUUID().toString()+fileStr.substring(fileStr.lastIndexOf('.'));
			
			//把图片保存到硬盘
			try {
				songPicfile.transferTo(new File("D:\\image\\"+newFileName));
			} catch (Exception e) {
				e.printStackTrace();
			}
			//把图片回显
			apiResponse.setStr("/pic/"+newFileName);
			apiResponse.setCode(200);
		}
		return apiResponse;
	}
	
	//更新歌曲信息
	@RequestMapping("/updatasongInfo")
	@ResponseBody
	public ApiResponse updatasongInfo(String songPicUrl,String songername,String albumname ,Songs song){
		ApiResponse apiResponse = new ApiResponse();
		//接收歌曲信息并更新歌曲信息
		songsService.updatasongInfo(songPicUrl, songername, albumname, song);
		apiResponse.setCode(200);
		apiResponse.setMassage("修改成功"); //返回提示信息
		return apiResponse;
	}
	
	//添加新的歌曲
	@RequestMapping("/addSongs")
	@ResponseBody
	public ApiResponse addSongs(@RequestParam("songMp3file") MultipartFile songMp3file,String songPicUrl,
			String songername,String albumname ,Songs song){
		System.out.println("添加歌曲");
		ApiResponse apiResponse = new ApiResponse();
		//获取歌曲路径名称
		String fileStr=songMp3file.getOriginalFilename();
		if(!fileStr.isEmpty()){
			//把歌曲文件保存到硬盘
			try {
				songMp3file.transferTo(new File("D:\\mp3\\"+fileStr));
			} catch (Exception e) {
				e.printStackTrace();
			}
			song.setMp3url("/mp3/"+fileStr);
		}
		//接收歌曲信息并添加到数据库
		songsService.addSongs(songPicUrl, songername, albumname, song);
		apiResponse.setCode(200);
		apiResponse.setMassage("添加成功");//返回提示信息
		return apiResponse;
	}
	
	//根据ID删除歌曲
	@RequestMapping("/delSongById")
	@ResponseBody
	public ApiResponse delSongById(String songid){
		ApiResponse apiResponse = new ApiResponse();
		songsService.delSongById(songid); //接收歌曲信息并删除歌曲记录
		apiResponse.setCode(200);
		apiResponse.setMassage("删除歌曲成功");//返回提示信息
		return apiResponse;
	}
	
	//热歌排行榜
	@RequestMapping("/selectBySongHotDesc")
	@ResponseBody
	public ApiResponse selectBySongHotDesc(){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = songsService.selectBySongHotDesc();
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(data);
		return apiResponse;
	}
	
	//其他排行榜
	@RequestMapping("/songsTopListByArea")
	@ResponseBody
	public ApiResponse songsTopListByArea(String area){
		ApiResponse apiResponse = new ApiResponse();
		DataDetail data = songsService.songsTopListByArea(area);
		apiResponse.setCode(200);
		apiResponse.setMassage("查询成功");
		apiResponse.setData(data);
		return apiResponse;
	}
}
