package com.wymusic.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wymusic.pojo.User;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request){
		User user = (User)request.getSession().getAttribute("admin");
		if(user!=null){
			return "forward:/WEB-INF/admin/index2.html";
		}else{
			return "redirect:/login.html";
		}
	}
}
