package com.wymusic.pojo;

import java.util.ArrayList;
import java.util.List;

public class SonglistandsongsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SonglistandsongsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSonglistandsongsidIsNull() {
            addCriterion("songlistAndsongsId is null");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidIsNotNull() {
            addCriterion("songlistAndsongsId is not null");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidEqualTo(Integer value) {
            addCriterion("songlistAndsongsId =", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidNotEqualTo(Integer value) {
            addCriterion("songlistAndsongsId <>", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidGreaterThan(Integer value) {
            addCriterion("songlistAndsongsId >", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidGreaterThanOrEqualTo(Integer value) {
            addCriterion("songlistAndsongsId >=", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidLessThan(Integer value) {
            addCriterion("songlistAndsongsId <", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidLessThanOrEqualTo(Integer value) {
            addCriterion("songlistAndsongsId <=", value, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidIn(List<Integer> values) {
            addCriterion("songlistAndsongsId in", values, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidNotIn(List<Integer> values) {
            addCriterion("songlistAndsongsId not in", values, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidBetween(Integer value1, Integer value2) {
            addCriterion("songlistAndsongsId between", value1, value2, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistandsongsidNotBetween(Integer value1, Integer value2) {
            addCriterion("songlistAndsongsId not between", value1, value2, "songlistandsongsid");
            return (Criteria) this;
        }

        public Criteria andSonglistidIsNull() {
            addCriterion("songlistId is null");
            return (Criteria) this;
        }

        public Criteria andSonglistidIsNotNull() {
            addCriterion("songlistId is not null");
            return (Criteria) this;
        }

        public Criteria andSonglistidEqualTo(Integer value) {
            addCriterion("songlistId =", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidNotEqualTo(Integer value) {
            addCriterion("songlistId <>", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidGreaterThan(Integer value) {
            addCriterion("songlistId >", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidGreaterThanOrEqualTo(Integer value) {
            addCriterion("songlistId >=", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidLessThan(Integer value) {
            addCriterion("songlistId <", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidLessThanOrEqualTo(Integer value) {
            addCriterion("songlistId <=", value, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidIn(List<Integer> values) {
            addCriterion("songlistId in", values, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidNotIn(List<Integer> values) {
            addCriterion("songlistId not in", values, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidBetween(Integer value1, Integer value2) {
            addCriterion("songlistId between", value1, value2, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistidNotBetween(Integer value1, Integer value2) {
            addCriterion("songlistId not between", value1, value2, "songlistid");
            return (Criteria) this;
        }

        public Criteria andSonglistnameIsNull() {
            addCriterion("songlistName is null");
            return (Criteria) this;
        }

        public Criteria andSonglistnameIsNotNull() {
            addCriterion("songlistName is not null");
            return (Criteria) this;
        }

        public Criteria andSonglistnameEqualTo(String value) {
            addCriterion("songlistName =", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameNotEqualTo(String value) {
            addCriterion("songlistName <>", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameGreaterThan(String value) {
            addCriterion("songlistName >", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameGreaterThanOrEqualTo(String value) {
            addCriterion("songlistName >=", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameLessThan(String value) {
            addCriterion("songlistName <", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameLessThanOrEqualTo(String value) {
            addCriterion("songlistName <=", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameLike(String value) {
            addCriterion("songlistName like", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameNotLike(String value) {
            addCriterion("songlistName not like", value, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameIn(List<String> values) {
            addCriterion("songlistName in", values, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameNotIn(List<String> values) {
            addCriterion("songlistName not in", values, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameBetween(String value1, String value2) {
            addCriterion("songlistName between", value1, value2, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSonglistnameNotBetween(String value1, String value2) {
            addCriterion("songlistName not between", value1, value2, "songlistname");
            return (Criteria) this;
        }

        public Criteria andSongsidIsNull() {
            addCriterion("songsId is null");
            return (Criteria) this;
        }

        public Criteria andSongsidIsNotNull() {
            addCriterion("songsId is not null");
            return (Criteria) this;
        }

        public Criteria andSongsidEqualTo(Integer value) {
            addCriterion("songsId =", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidNotEqualTo(Integer value) {
            addCriterion("songsId <>", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidGreaterThan(Integer value) {
            addCriterion("songsId >", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidGreaterThanOrEqualTo(Integer value) {
            addCriterion("songsId >=", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidLessThan(Integer value) {
            addCriterion("songsId <", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidLessThanOrEqualTo(Integer value) {
            addCriterion("songsId <=", value, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidIn(List<Integer> values) {
            addCriterion("songsId in", values, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidNotIn(List<Integer> values) {
            addCriterion("songsId not in", values, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidBetween(Integer value1, Integer value2) {
            addCriterion("songsId between", value1, value2, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsidNotBetween(Integer value1, Integer value2) {
            addCriterion("songsId not between", value1, value2, "songsid");
            return (Criteria) this;
        }

        public Criteria andSongsnameIsNull() {
            addCriterion("songsName is null");
            return (Criteria) this;
        }

        public Criteria andSongsnameIsNotNull() {
            addCriterion("songsName is not null");
            return (Criteria) this;
        }

        public Criteria andSongsnameEqualTo(String value) {
            addCriterion("songsName =", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameNotEqualTo(String value) {
            addCriterion("songsName <>", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameGreaterThan(String value) {
            addCriterion("songsName >", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameGreaterThanOrEqualTo(String value) {
            addCriterion("songsName >=", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameLessThan(String value) {
            addCriterion("songsName <", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameLessThanOrEqualTo(String value) {
            addCriterion("songsName <=", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameLike(String value) {
            addCriterion("songsName like", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameNotLike(String value) {
            addCriterion("songsName not like", value, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameIn(List<String> values) {
            addCriterion("songsName in", values, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameNotIn(List<String> values) {
            addCriterion("songsName not in", values, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameBetween(String value1, String value2) {
            addCriterion("songsName between", value1, value2, "songsname");
            return (Criteria) this;
        }

        public Criteria andSongsnameNotBetween(String value1, String value2) {
            addCriterion("songsName not between", value1, value2, "songsname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}