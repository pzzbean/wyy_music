package com.wymusic.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AlbumExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AlbumExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAlbumidIsNull() {
            addCriterion("albumId is null");
            return (Criteria) this;
        }

        public Criteria andAlbumidIsNotNull() {
            addCriterion("albumId is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumidEqualTo(Integer value) {
            addCriterion("albumId =", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotEqualTo(Integer value) {
            addCriterion("albumId <>", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidGreaterThan(Integer value) {
            addCriterion("albumId >", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidGreaterThanOrEqualTo(Integer value) {
            addCriterion("albumId >=", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidLessThan(Integer value) {
            addCriterion("albumId <", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidLessThanOrEqualTo(Integer value) {
            addCriterion("albumId <=", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidIn(List<Integer> values) {
            addCriterion("albumId in", values, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotIn(List<Integer> values) {
            addCriterion("albumId not in", values, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidBetween(Integer value1, Integer value2) {
            addCriterion("albumId between", value1, value2, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotBetween(Integer value1, Integer value2) {
            addCriterion("albumId not between", value1, value2, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumnameIsNull() {
            addCriterion("albumName is null");
            return (Criteria) this;
        }

        public Criteria andAlbumnameIsNotNull() {
            addCriterion("albumName is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumnameEqualTo(String value) {
            addCriterion("albumName =", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameNotEqualTo(String value) {
            addCriterion("albumName <>", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameGreaterThan(String value) {
            addCriterion("albumName >", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameGreaterThanOrEqualTo(String value) {
            addCriterion("albumName >=", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameLessThan(String value) {
            addCriterion("albumName <", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameLessThanOrEqualTo(String value) {
            addCriterion("albumName <=", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameLike(String value) {
            addCriterion("albumName like", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameNotLike(String value) {
            addCriterion("albumName not like", value, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameIn(List<String> values) {
            addCriterion("albumName in", values, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameNotIn(List<String> values) {
            addCriterion("albumName not in", values, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameBetween(String value1, String value2) {
            addCriterion("albumName between", value1, value2, "albumname");
            return (Criteria) this;
        }

        public Criteria andAlbumnameNotBetween(String value1, String value2) {
            addCriterion("albumName not between", value1, value2, "albumname");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNull() {
            addCriterion("songerId is null");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNotNull() {
            addCriterion("songerId is not null");
            return (Criteria) this;
        }

        public Criteria andSongeridEqualTo(Integer value) {
            addCriterion("songerId =", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotEqualTo(Integer value) {
            addCriterion("songerId <>", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThan(Integer value) {
            addCriterion("songerId >", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerId >=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThan(Integer value) {
            addCriterion("songerId <", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThanOrEqualTo(Integer value) {
            addCriterion("songerId <=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridIn(List<Integer> values) {
            addCriterion("songerId in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotIn(List<Integer> values) {
            addCriterion("songerId not in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridBetween(Integer value1, Integer value2) {
            addCriterion("songerId between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotBetween(Integer value1, Integer value2) {
            addCriterion("songerId not between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNull() {
            addCriterion("songerName is null");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNotNull() {
            addCriterion("songerName is not null");
            return (Criteria) this;
        }

        public Criteria andSongernameEqualTo(String value) {
            addCriterion("songerName =", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotEqualTo(String value) {
            addCriterion("songerName <>", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThan(String value) {
            addCriterion("songerName >", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThanOrEqualTo(String value) {
            addCriterion("songerName >=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThan(String value) {
            addCriterion("songerName <", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThanOrEqualTo(String value) {
            addCriterion("songerName <=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLike(String value) {
            addCriterion("songerName like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotLike(String value) {
            addCriterion("songerName not like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameIn(List<String> values) {
            addCriterion("songerName in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotIn(List<String> values) {
            addCriterion("songerName not in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameBetween(String value1, String value2) {
            addCriterion("songerName between", value1, value2, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotBetween(String value1, String value2) {
            addCriterion("songerName not between", value1, value2, "songername");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleIsNull() {
            addCriterion("albumStyle is null");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleIsNotNull() {
            addCriterion("albumStyle is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleEqualTo(String value) {
            addCriterion("albumStyle =", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleNotEqualTo(String value) {
            addCriterion("albumStyle <>", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleGreaterThan(String value) {
            addCriterion("albumStyle >", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleGreaterThanOrEqualTo(String value) {
            addCriterion("albumStyle >=", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleLessThan(String value) {
            addCriterion("albumStyle <", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleLessThanOrEqualTo(String value) {
            addCriterion("albumStyle <=", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleLike(String value) {
            addCriterion("albumStyle like", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleNotLike(String value) {
            addCriterion("albumStyle not like", value, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleIn(List<String> values) {
            addCriterion("albumStyle in", values, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleNotIn(List<String> values) {
            addCriterion("albumStyle not in", values, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleBetween(String value1, String value2) {
            addCriterion("albumStyle between", value1, value2, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumstyleNotBetween(String value1, String value2) {
            addCriterion("albumStyle not between", value1, value2, "albumstyle");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyIsNull() {
            addCriterion("publicCompany is null");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyIsNotNull() {
            addCriterion("publicCompany is not null");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyEqualTo(String value) {
            addCriterion("publicCompany =", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyNotEqualTo(String value) {
            addCriterion("publicCompany <>", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyGreaterThan(String value) {
            addCriterion("publicCompany >", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyGreaterThanOrEqualTo(String value) {
            addCriterion("publicCompany >=", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyLessThan(String value) {
            addCriterion("publicCompany <", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyLessThanOrEqualTo(String value) {
            addCriterion("publicCompany <=", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyLike(String value) {
            addCriterion("publicCompany like", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyNotLike(String value) {
            addCriterion("publicCompany not like", value, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyIn(List<String> values) {
            addCriterion("publicCompany in", values, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyNotIn(List<String> values) {
            addCriterion("publicCompany not in", values, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyBetween(String value1, String value2) {
            addCriterion("publicCompany between", value1, value2, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPubliccompanyNotBetween(String value1, String value2) {
            addCriterion("publicCompany not between", value1, value2, "publiccompany");
            return (Criteria) this;
        }

        public Criteria andPublicareaIsNull() {
            addCriterion("publicArea is null");
            return (Criteria) this;
        }

        public Criteria andPublicareaIsNotNull() {
            addCriterion("publicArea is not null");
            return (Criteria) this;
        }

        public Criteria andPublicareaEqualTo(String value) {
            addCriterion("publicArea =", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaNotEqualTo(String value) {
            addCriterion("publicArea <>", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaGreaterThan(String value) {
            addCriterion("publicArea >", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaGreaterThanOrEqualTo(String value) {
            addCriterion("publicArea >=", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaLessThan(String value) {
            addCriterion("publicArea <", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaLessThanOrEqualTo(String value) {
            addCriterion("publicArea <=", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaLike(String value) {
            addCriterion("publicArea like", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaNotLike(String value) {
            addCriterion("publicArea not like", value, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaIn(List<String> values) {
            addCriterion("publicArea in", values, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaNotIn(List<String> values) {
            addCriterion("publicArea not in", values, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaBetween(String value1, String value2) {
            addCriterion("publicArea between", value1, value2, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublicareaNotBetween(String value1, String value2) {
            addCriterion("publicArea not between", value1, value2, "publicarea");
            return (Criteria) this;
        }

        public Criteria andPublictimeIsNull() {
            addCriterion("publicTime is null");
            return (Criteria) this;
        }

        public Criteria andPublictimeIsNotNull() {
            addCriterion("publicTime is not null");
            return (Criteria) this;
        }

        public Criteria andPublictimeEqualTo(Date value) {
            addCriterion("publicTime =", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeNotEqualTo(Date value) {
            addCriterion("publicTime <>", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeGreaterThan(Date value) {
            addCriterion("publicTime >", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeGreaterThanOrEqualTo(Date value) {
            addCriterion("publicTime >=", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeLessThan(Date value) {
            addCriterion("publicTime <", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeLessThanOrEqualTo(Date value) {
            addCriterion("publicTime <=", value, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeIn(List<Date> values) {
            addCriterion("publicTime in", values, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeNotIn(List<Date> values) {
            addCriterion("publicTime not in", values, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeBetween(Date value1, Date value2) {
            addCriterion("publicTime between", value1, value2, "publictime");
            return (Criteria) this;
        }

        public Criteria andPublictimeNotBetween(Date value1, Date value2) {
            addCriterion("publicTime not between", value1, value2, "publictime");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlIsNull() {
            addCriterion("albumOverUrl is null");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlIsNotNull() {
            addCriterion("albumOverUrl is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlEqualTo(String value) {
            addCriterion("albumOverUrl =", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlNotEqualTo(String value) {
            addCriterion("albumOverUrl <>", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlGreaterThan(String value) {
            addCriterion("albumOverUrl >", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlGreaterThanOrEqualTo(String value) {
            addCriterion("albumOverUrl >=", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlLessThan(String value) {
            addCriterion("albumOverUrl <", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlLessThanOrEqualTo(String value) {
            addCriterion("albumOverUrl <=", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlLike(String value) {
            addCriterion("albumOverUrl like", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlNotLike(String value) {
            addCriterion("albumOverUrl not like", value, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlIn(List<String> values) {
            addCriterion("albumOverUrl in", values, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlNotIn(List<String> values) {
            addCriterion("albumOverUrl not in", values, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlBetween(String value1, String value2) {
            addCriterion("albumOverUrl between", value1, value2, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumoverurlNotBetween(String value1, String value2) {
            addCriterion("albumOverUrl not between", value1, value2, "albumoverurl");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusIsNull() {
            addCriterion("albumStatus is null");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusIsNotNull() {
            addCriterion("albumStatus is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusEqualTo(Integer value) {
            addCriterion("albumStatus =", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusNotEqualTo(Integer value) {
            addCriterion("albumStatus <>", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusGreaterThan(Integer value) {
            addCriterion("albumStatus >", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("albumStatus >=", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusLessThan(Integer value) {
            addCriterion("albumStatus <", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusLessThanOrEqualTo(Integer value) {
            addCriterion("albumStatus <=", value, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusIn(List<Integer> values) {
            addCriterion("albumStatus in", values, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusNotIn(List<Integer> values) {
            addCriterion("albumStatus not in", values, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusBetween(Integer value1, Integer value2) {
            addCriterion("albumStatus between", value1, value2, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("albumStatus not between", value1, value2, "albumstatus");
            return (Criteria) this;
        }

        public Criteria andAlbumhotIsNull() {
            addCriterion("albumHot is null");
            return (Criteria) this;
        }

        public Criteria andAlbumhotIsNotNull() {
            addCriterion("albumHot is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumhotEqualTo(Integer value) {
            addCriterion("albumHot =", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotNotEqualTo(Integer value) {
            addCriterion("albumHot <>", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotGreaterThan(Integer value) {
            addCriterion("albumHot >", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotGreaterThanOrEqualTo(Integer value) {
            addCriterion("albumHot >=", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotLessThan(Integer value) {
            addCriterion("albumHot <", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotLessThanOrEqualTo(Integer value) {
            addCriterion("albumHot <=", value, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotIn(List<Integer> values) {
            addCriterion("albumHot in", values, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotNotIn(List<Integer> values) {
            addCriterion("albumHot not in", values, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotBetween(Integer value1, Integer value2) {
            addCriterion("albumHot between", value1, value2, "albumhot");
            return (Criteria) this;
        }

        public Criteria andAlbumhotNotBetween(Integer value1, Integer value2) {
            addCriterion("albumHot not between", value1, value2, "albumhot");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}