package com.wymusic.pojo;

public class Albumcomments {
    private Integer id;

    private Integer commentid;

    private Integer albumid;

    private String albumname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getAlbumid() {
        return albumid;
    }

    public void setAlbumid(Integer albumid) {
        this.albumid = albumid;
    }

    public String getAlbumname() {
        return albumname;
    }

    public void setAlbumname(String albumname) {
        this.albumname = albumname == null ? null : albumname.trim();
    }

	public Albumcomments() {
		super();
	}

	@Override
	public String toString() {
		return "Albumcomments [id=" + id + ", commentid=" + commentid + ", albumid=" + albumid + ", albumname="
				+ albumname + "]";
	}

	public Albumcomments(Integer commentid, Integer albumid, String albumname) {
		super();
		this.commentid = commentid;
		this.albumid = albumid;
		this.albumname = albumname;
	}

}