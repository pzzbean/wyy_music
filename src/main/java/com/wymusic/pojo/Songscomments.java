package com.wymusic.pojo;

public class Songscomments {
    private Integer id;

    private Integer commentid;

    private Integer songsid;

    private String songname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getSongsid() {
        return songsid;
    }

    public void setSongsid(Integer songsid) {
        this.songsid = songsid;
    }

    public String getSongname() {
        return songname;
    }

    public void setSongname(String songname) {
        this.songname = songname == null ? null : songname.trim();
    }

	public Songscomments( Integer commentid, Integer songsid, String songname) {
		this.commentid = commentid;
		this.songsid = songsid;
		this.songname = songname;
	}

	public Songscomments() {
		super();
	}

	@Override
	public String toString() {
		return "Songscomments [id=" + id + ", commentid=" + commentid + ", songsid=" + songsid + ", songname="
				+ songname + "]";
	}
    
	
    
}