package com.wymusic.pojo;

public class Songercomments {
    private Integer id;

    private Integer commentid;

    private Integer songerid;

    private String songername;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getSongerid() {
        return songerid;
    }

    public void setSongerid(Integer songerid) {
        this.songerid = songerid;
    }

    public String getSongername() {
        return songername;
    }

    public void setSongername(String songername) {
        this.songername = songername == null ? null : songername.trim();
    }
}