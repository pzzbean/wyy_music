package com.wymusic.pojo;

public class Songlistandsongs {
    private Integer songlistandsongsid;

    private Integer songlistid;

    private String songlistname;

    private Integer songsid;

    private String songsname;

    public Integer getSonglistandsongsid() {
        return songlistandsongsid;
    }

    public void setSonglistandsongsid(Integer songlistandsongsid) {
        this.songlistandsongsid = songlistandsongsid;
    }

    public Integer getSonglistid() {
        return songlistid;
    }

    public void setSonglistid(Integer songlistid) {
        this.songlistid = songlistid;
    }

    public String getSonglistname() {
        return songlistname;
    }

    public void setSonglistname(String songlistname) {
        this.songlistname = songlistname == null ? null : songlistname.trim();
    }

    public Integer getSongsid() {
        return songsid;
    }

    public void setSongsid(Integer songsid) {
        this.songsid = songsid;
    }

    public String getSongsname() {
        return songsname;
    }

    public void setSongsname(String songsname) {
        this.songsname = songsname == null ? null : songsname.trim();
    }

	@Override
	public String toString() {
		return "Songlistandsongs [songlistandsongsid=" + songlistandsongsid + ", songlistid=" + songlistid
				+ ", songlistname=" + songlistname + ", songsid=" + songsid + ", songsname=" + songsname + "]";
	}
    
}