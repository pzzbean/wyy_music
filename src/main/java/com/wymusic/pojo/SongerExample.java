package com.wymusic.pojo;

import java.util.ArrayList;
import java.util.List;

public class SongerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SongerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSongeridIsNull() {
            addCriterion("songerId is null");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNotNull() {
            addCriterion("songerId is not null");
            return (Criteria) this;
        }

        public Criteria andSongeridEqualTo(Integer value) {
            addCriterion("songerId =", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotEqualTo(Integer value) {
            addCriterion("songerId <>", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThan(Integer value) {
            addCriterion("songerId >", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerId >=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThan(Integer value) {
            addCriterion("songerId <", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThanOrEqualTo(Integer value) {
            addCriterion("songerId <=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridIn(List<Integer> values) {
            addCriterion("songerId in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotIn(List<Integer> values) {
            addCriterion("songerId not in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridBetween(Integer value1, Integer value2) {
            addCriterion("songerId between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotBetween(Integer value1, Integer value2) {
            addCriterion("songerId not between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNull() {
            addCriterion("songerName is null");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNotNull() {
            addCriterion("songerName is not null");
            return (Criteria) this;
        }

        public Criteria andSongernameEqualTo(String value) {
            addCriterion("songerName =", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotEqualTo(String value) {
            addCriterion("songerName <>", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThan(String value) {
            addCriterion("songerName >", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThanOrEqualTo(String value) {
            addCriterion("songerName >=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThan(String value) {
            addCriterion("songerName <", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThanOrEqualTo(String value) {
            addCriterion("songerName <=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLike(String value) {
            addCriterion("songerName like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotLike(String value) {
            addCriterion("songerName not like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameIn(List<String> values) {
            addCriterion("songerName in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotIn(List<String> values) {
            addCriterion("songerName not in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameBetween(String value1, String value2) {
            addCriterion("songerName between", value1, value2, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotBetween(String value1, String value2) {
            addCriterion("songerName not between", value1, value2, "songername");
            return (Criteria) this;
        }

        public Criteria andSongercountryIsNull() {
            addCriterion("songerCountry is null");
            return (Criteria) this;
        }

        public Criteria andSongercountryIsNotNull() {
            addCriterion("songerCountry is not null");
            return (Criteria) this;
        }

        public Criteria andSongercountryEqualTo(String value) {
            addCriterion("songerCountry =", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryNotEqualTo(String value) {
            addCriterion("songerCountry <>", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryGreaterThan(String value) {
            addCriterion("songerCountry >", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryGreaterThanOrEqualTo(String value) {
            addCriterion("songerCountry >=", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryLessThan(String value) {
            addCriterion("songerCountry <", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryLessThanOrEqualTo(String value) {
            addCriterion("songerCountry <=", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryLike(String value) {
            addCriterion("songerCountry like", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryNotLike(String value) {
            addCriterion("songerCountry not like", value, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryIn(List<String> values) {
            addCriterion("songerCountry in", values, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryNotIn(List<String> values) {
            addCriterion("songerCountry not in", values, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryBetween(String value1, String value2) {
            addCriterion("songerCountry between", value1, value2, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongercountryNotBetween(String value1, String value2) {
            addCriterion("songerCountry not between", value1, value2, "songercountry");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlIsNull() {
            addCriterion("songerPicUrl is null");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlIsNotNull() {
            addCriterion("songerPicUrl is not null");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlEqualTo(String value) {
            addCriterion("songerPicUrl =", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlNotEqualTo(String value) {
            addCriterion("songerPicUrl <>", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlGreaterThan(String value) {
            addCriterion("songerPicUrl >", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlGreaterThanOrEqualTo(String value) {
            addCriterion("songerPicUrl >=", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlLessThan(String value) {
            addCriterion("songerPicUrl <", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlLessThanOrEqualTo(String value) {
            addCriterion("songerPicUrl <=", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlLike(String value) {
            addCriterion("songerPicUrl like", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlNotLike(String value) {
            addCriterion("songerPicUrl not like", value, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlIn(List<String> values) {
            addCriterion("songerPicUrl in", values, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlNotIn(List<String> values) {
            addCriterion("songerPicUrl not in", values, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlBetween(String value1, String value2) {
            addCriterion("songerPicUrl between", value1, value2, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerpicurlNotBetween(String value1, String value2) {
            addCriterion("songerPicUrl not between", value1, value2, "songerpicurl");
            return (Criteria) this;
        }

        public Criteria andSongerstatusIsNull() {
            addCriterion("songerStatus is null");
            return (Criteria) this;
        }

        public Criteria andSongerstatusIsNotNull() {
            addCriterion("songerStatus is not null");
            return (Criteria) this;
        }

        public Criteria andSongerstatusEqualTo(Integer value) {
            addCriterion("songerStatus =", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusNotEqualTo(Integer value) {
            addCriterion("songerStatus <>", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusGreaterThan(Integer value) {
            addCriterion("songerStatus >", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerStatus >=", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusLessThan(Integer value) {
            addCriterion("songerStatus <", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusLessThanOrEqualTo(Integer value) {
            addCriterion("songerStatus <=", value, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusIn(List<Integer> values) {
            addCriterion("songerStatus in", values, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusNotIn(List<Integer> values) {
            addCriterion("songerStatus not in", values, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusBetween(Integer value1, Integer value2) {
            addCriterion("songerStatus between", value1, value2, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("songerStatus not between", value1, value2, "songerstatus");
            return (Criteria) this;
        }

        public Criteria andSongerhotIsNull() {
            addCriterion("songerHot is null");
            return (Criteria) this;
        }

        public Criteria andSongerhotIsNotNull() {
            addCriterion("songerHot is not null");
            return (Criteria) this;
        }

        public Criteria andSongerhotEqualTo(Integer value) {
            addCriterion("songerHot =", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotNotEqualTo(Integer value) {
            addCriterion("songerHot <>", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotGreaterThan(Integer value) {
            addCriterion("songerHot >", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerHot >=", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotLessThan(Integer value) {
            addCriterion("songerHot <", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotLessThanOrEqualTo(Integer value) {
            addCriterion("songerHot <=", value, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotIn(List<Integer> values) {
            addCriterion("songerHot in", values, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotNotIn(List<Integer> values) {
            addCriterion("songerHot not in", values, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotBetween(Integer value1, Integer value2) {
            addCriterion("songerHot between", value1, value2, "songerhot");
            return (Criteria) this;
        }

        public Criteria andSongerhotNotBetween(Integer value1, Integer value2) {
            addCriterion("songerHot not between", value1, value2, "songerhot");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}