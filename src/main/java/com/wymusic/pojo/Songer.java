package com.wymusic.pojo;

public class Songer {
    private Integer songerid; //歌手ID

    private String songername; //歌手名字

    private String songercountry;//歌手国籍

    private String songerpicurl;//歌手图片

    private Integer songerstatus;//歌手状态  0 为删除 1为正常

    private Integer songerhot;//歌手热门程度

    private String songerinfo;//歌手简介

    public Integer getSongerid() {
        return songerid;
    }

    public void setSongerid(Integer songerid) {
        this.songerid = songerid;
    }

    public String getSongername() {
        return songername;
    }

    public void setSongername(String songername) {
        this.songername = songername == null ? null : songername.trim();
    }

    public String getSongercountry() {
        return songercountry;
    }

    public void setSongercountry(String songercountry) {
        this.songercountry = songercountry == null ? null : songercountry.trim();
    }

    public String getSongerpicurl() {
        return songerpicurl;
    }

    public void setSongerpicurl(String songerpicurl) {
        this.songerpicurl = songerpicurl == null ? null : songerpicurl.trim();
    }

    public Integer getSongerstatus() {
        return songerstatus;
    }

    public void setSongerstatus(Integer songerstatus) {
        this.songerstatus = songerstatus;
    }

    public Integer getSongerhot() {
        return songerhot;
    }

    public void setSongerhot(Integer songerhot) {
        this.songerhot = songerhot;
    }

    public String getSongerinfo() {
        return songerinfo;
    }

    public void setSongerinfo(String songerinfo) {
        this.songerinfo = songerinfo == null ? null : songerinfo.trim();
    }

	@Override
	public String toString() {
		return "Songer [songerid=" + songerid + ", songername=" + songername + ", songercountry=" + songercountry
				+ ", songerpicurl=" + songerpicurl + ", songerstatus=" + songerstatus + ", songerhot=" + songerhot
				+ ", songerinfo=" + songerinfo + "]";
	}
    
}