package com.wymusic.pojo;

public class Songlist {
    private Integer songlistid;

    private String songlistpic;

    private String songlistname;

    private Integer userid;

    private String username;

    private Integer songliststatus;

    private String songlistinfo;

    public Integer getSonglistid() {
        return songlistid;
    }

    public void setSonglistid(Integer songlistid) {
        this.songlistid = songlistid;
    }

    public String getSonglistpic() {
        return songlistpic;
    }

    public void setSonglistpic(String songlistpic) {
        this.songlistpic = songlistpic == null ? null : songlistpic.trim();
    }

    public String getSonglistname() {
        return songlistname;
    }

    public void setSonglistname(String songlistname) {
        this.songlistname = songlistname == null ? null : songlistname.trim();
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getSongliststatus() {
        return songliststatus;
    }

    public void setSongliststatus(Integer songliststatus) {
        this.songliststatus = songliststatus;
    }

    public String getSonglistinfo() {
        return songlistinfo;
    }

    public void setSonglistinfo(String songlistinfo) {
        this.songlistinfo = songlistinfo == null ? null : songlistinfo.trim();
    }

	public Songlist(Integer songlistid, String songlistpic, String songlistname, Integer userid, String username,
			Integer songliststatus, String songlistinfo) {
		super();
		this.songlistid = songlistid;
		this.songlistpic = songlistpic;
		this.songlistname = songlistname;
		this.userid = userid;
		this.username = username;
		this.songliststatus = songliststatus;
		this.songlistinfo = songlistinfo;
	}

	public Songlist() {
		super();
	}
	@Override
	public String toString() {
		return "Songlist [songlistid=" + songlistid + ", songlistpic=" + songlistpic + ", songlistname=" + songlistname
				+ ", userid=" + userid + ", username=" + username + ", songliststatus=" + songliststatus
				+ ", songlistinfo=" + songlistinfo + "]";
	}
}