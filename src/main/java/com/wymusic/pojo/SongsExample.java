package com.wymusic.pojo;

import java.util.ArrayList;
import java.util.List;

public class SongsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SongsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSongidIsNull() {
            addCriterion("songId is null");
            return (Criteria) this;
        }

        public Criteria andSongidIsNotNull() {
            addCriterion("songId is not null");
            return (Criteria) this;
        }

        public Criteria andSongidEqualTo(Integer value) {
            addCriterion("songId =", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidNotEqualTo(Integer value) {
            addCriterion("songId <>", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidGreaterThan(Integer value) {
            addCriterion("songId >", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidGreaterThanOrEqualTo(Integer value) {
            addCriterion("songId >=", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidLessThan(Integer value) {
            addCriterion("songId <", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidLessThanOrEqualTo(Integer value) {
            addCriterion("songId <=", value, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidIn(List<Integer> values) {
            addCriterion("songId in", values, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidNotIn(List<Integer> values) {
            addCriterion("songId not in", values, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidBetween(Integer value1, Integer value2) {
            addCriterion("songId between", value1, value2, "songid");
            return (Criteria) this;
        }

        public Criteria andSongidNotBetween(Integer value1, Integer value2) {
            addCriterion("songId not between", value1, value2, "songid");
            return (Criteria) this;
        }

        public Criteria andSongnameIsNull() {
            addCriterion("songName is null");
            return (Criteria) this;
        }

        public Criteria andSongnameIsNotNull() {
            addCriterion("songName is not null");
            return (Criteria) this;
        }

        public Criteria andSongnameEqualTo(String value) {
            addCriterion("songName =", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameNotEqualTo(String value) {
            addCriterion("songName <>", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameGreaterThan(String value) {
            addCriterion("songName >", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameGreaterThanOrEqualTo(String value) {
            addCriterion("songName >=", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameLessThan(String value) {
            addCriterion("songName <", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameLessThanOrEqualTo(String value) {
            addCriterion("songName <=", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameLike(String value) {
            addCriterion("songName like", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameNotLike(String value) {
            addCriterion("songName not like", value, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameIn(List<String> values) {
            addCriterion("songName in", values, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameNotIn(List<String> values) {
            addCriterion("songName not in", values, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameBetween(String value1, String value2) {
            addCriterion("songName between", value1, value2, "songname");
            return (Criteria) this;
        }

        public Criteria andSongnameNotBetween(String value1, String value2) {
            addCriterion("songName not between", value1, value2, "songname");
            return (Criteria) this;
        }

        public Criteria andSongstyleIsNull() {
            addCriterion("songStyle is null");
            return (Criteria) this;
        }

        public Criteria andSongstyleIsNotNull() {
            addCriterion("songStyle is not null");
            return (Criteria) this;
        }

        public Criteria andSongstyleEqualTo(String value) {
            addCriterion("songStyle =", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleNotEqualTo(String value) {
            addCriterion("songStyle <>", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleGreaterThan(String value) {
            addCriterion("songStyle >", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleGreaterThanOrEqualTo(String value) {
            addCriterion("songStyle >=", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleLessThan(String value) {
            addCriterion("songStyle <", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleLessThanOrEqualTo(String value) {
            addCriterion("songStyle <=", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleLike(String value) {
            addCriterion("songStyle like", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleNotLike(String value) {
            addCriterion("songStyle not like", value, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleIn(List<String> values) {
            addCriterion("songStyle in", values, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleNotIn(List<String> values) {
            addCriterion("songStyle not in", values, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleBetween(String value1, String value2) {
            addCriterion("songStyle between", value1, value2, "songstyle");
            return (Criteria) this;
        }

        public Criteria andSongstyleNotBetween(String value1, String value2) {
            addCriterion("songStyle not between", value1, value2, "songstyle");
            return (Criteria) this;
        }

        public Criteria andAlbumidIsNull() {
            addCriterion("albumId is null");
            return (Criteria) this;
        }

        public Criteria andAlbumidIsNotNull() {
            addCriterion("albumId is not null");
            return (Criteria) this;
        }

        public Criteria andAlbumidEqualTo(Integer value) {
            addCriterion("albumId =", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotEqualTo(Integer value) {
            addCriterion("albumId <>", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidGreaterThan(Integer value) {
            addCriterion("albumId >", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidGreaterThanOrEqualTo(Integer value) {
            addCriterion("albumId >=", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidLessThan(Integer value) {
            addCriterion("albumId <", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidLessThanOrEqualTo(Integer value) {
            addCriterion("albumId <=", value, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidIn(List<Integer> values) {
            addCriterion("albumId in", values, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotIn(List<Integer> values) {
            addCriterion("albumId not in", values, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidBetween(Integer value1, Integer value2) {
            addCriterion("albumId between", value1, value2, "albumid");
            return (Criteria) this;
        }

        public Criteria andAlbumidNotBetween(Integer value1, Integer value2) {
            addCriterion("albumId not between", value1, value2, "albumid");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNull() {
            addCriterion("songerId is null");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNotNull() {
            addCriterion("songerId is not null");
            return (Criteria) this;
        }

        public Criteria andSongeridEqualTo(Integer value) {
            addCriterion("songerId =", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotEqualTo(Integer value) {
            addCriterion("songerId <>", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThan(Integer value) {
            addCriterion("songerId >", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerId >=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThan(Integer value) {
            addCriterion("songerId <", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThanOrEqualTo(Integer value) {
            addCriterion("songerId <=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridIn(List<Integer> values) {
            addCriterion("songerId in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotIn(List<Integer> values) {
            addCriterion("songerId not in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridBetween(Integer value1, Integer value2) {
            addCriterion("songerId between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotBetween(Integer value1, Integer value2) {
            addCriterion("songerId not between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andMp3urlIsNull() {
            addCriterion("mp3Url is null");
            return (Criteria) this;
        }

        public Criteria andMp3urlIsNotNull() {
            addCriterion("mp3Url is not null");
            return (Criteria) this;
        }

        public Criteria andMp3urlEqualTo(String value) {
            addCriterion("mp3Url =", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlNotEqualTo(String value) {
            addCriterion("mp3Url <>", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlGreaterThan(String value) {
            addCriterion("mp3Url >", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlGreaterThanOrEqualTo(String value) {
            addCriterion("mp3Url >=", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlLessThan(String value) {
            addCriterion("mp3Url <", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlLessThanOrEqualTo(String value) {
            addCriterion("mp3Url <=", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlLike(String value) {
            addCriterion("mp3Url like", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlNotLike(String value) {
            addCriterion("mp3Url not like", value, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlIn(List<String> values) {
            addCriterion("mp3Url in", values, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlNotIn(List<String> values) {
            addCriterion("mp3Url not in", values, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlBetween(String value1, String value2) {
            addCriterion("mp3Url between", value1, value2, "mp3url");
            return (Criteria) this;
        }

        public Criteria andMp3urlNotBetween(String value1, String value2) {
            addCriterion("mp3Url not between", value1, value2, "mp3url");
            return (Criteria) this;
        }

        public Criteria andSongurlIsNull() {
            addCriterion("songUrl is null");
            return (Criteria) this;
        }

        public Criteria andSongurlIsNotNull() {
            addCriterion("songUrl is not null");
            return (Criteria) this;
        }

        public Criteria andSongurlEqualTo(String value) {
            addCriterion("songUrl =", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlNotEqualTo(String value) {
            addCriterion("songUrl <>", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlGreaterThan(String value) {
            addCriterion("songUrl >", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlGreaterThanOrEqualTo(String value) {
            addCriterion("songUrl >=", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlLessThan(String value) {
            addCriterion("songUrl <", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlLessThanOrEqualTo(String value) {
            addCriterion("songUrl <=", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlLike(String value) {
            addCriterion("songUrl like", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlNotLike(String value) {
            addCriterion("songUrl not like", value, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlIn(List<String> values) {
            addCriterion("songUrl in", values, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlNotIn(List<String> values) {
            addCriterion("songUrl not in", values, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlBetween(String value1, String value2) {
            addCriterion("songUrl between", value1, value2, "songurl");
            return (Criteria) this;
        }

        public Criteria andSongurlNotBetween(String value1, String value2) {
            addCriterion("songUrl not between", value1, value2, "songurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlIsNull() {
            addCriterion("songLrcUrl is null");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlIsNotNull() {
            addCriterion("songLrcUrl is not null");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlEqualTo(String value) {
            addCriterion("songLrcUrl =", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlNotEqualTo(String value) {
            addCriterion("songLrcUrl <>", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlGreaterThan(String value) {
            addCriterion("songLrcUrl >", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlGreaterThanOrEqualTo(String value) {
            addCriterion("songLrcUrl >=", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlLessThan(String value) {
            addCriterion("songLrcUrl <", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlLessThanOrEqualTo(String value) {
            addCriterion("songLrcUrl <=", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlLike(String value) {
            addCriterion("songLrcUrl like", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlNotLike(String value) {
            addCriterion("songLrcUrl not like", value, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlIn(List<String> values) {
            addCriterion("songLrcUrl in", values, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlNotIn(List<String> values) {
            addCriterion("songLrcUrl not in", values, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlBetween(String value1, String value2) {
            addCriterion("songLrcUrl between", value1, value2, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSonglrcurlNotBetween(String value1, String value2) {
            addCriterion("songLrcUrl not between", value1, value2, "songlrcurl");
            return (Criteria) this;
        }

        public Criteria andSongtimeIsNull() {
            addCriterion("songTime is null");
            return (Criteria) this;
        }

        public Criteria andSongtimeIsNotNull() {
            addCriterion("songTime is not null");
            return (Criteria) this;
        }

        public Criteria andSongtimeEqualTo(String value) {
            addCriterion("songTime =", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeNotEqualTo(String value) {
            addCriterion("songTime <>", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeGreaterThan(String value) {
            addCriterion("songTime >", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeGreaterThanOrEqualTo(String value) {
            addCriterion("songTime >=", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeLessThan(String value) {
            addCriterion("songTime <", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeLessThanOrEqualTo(String value) {
            addCriterion("songTime <=", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeLike(String value) {
            addCriterion("songTime like", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeNotLike(String value) {
            addCriterion("songTime not like", value, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeIn(List<String> values) {
            addCriterion("songTime in", values, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeNotIn(List<String> values) {
            addCriterion("songTime not in", values, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeBetween(String value1, String value2) {
            addCriterion("songTime between", value1, value2, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongtimeNotBetween(String value1, String value2) {
            addCriterion("songTime not between", value1, value2, "songtime");
            return (Criteria) this;
        }

        public Criteria andSongstatusIsNull() {
            addCriterion("songStatus is null");
            return (Criteria) this;
        }

        public Criteria andSongstatusIsNotNull() {
            addCriterion("songStatus is not null");
            return (Criteria) this;
        }

        public Criteria andSongstatusEqualTo(Integer value) {
            addCriterion("songStatus =", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusNotEqualTo(Integer value) {
            addCriterion("songStatus <>", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusGreaterThan(Integer value) {
            addCriterion("songStatus >", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("songStatus >=", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusLessThan(Integer value) {
            addCriterion("songStatus <", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusLessThanOrEqualTo(Integer value) {
            addCriterion("songStatus <=", value, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusIn(List<Integer> values) {
            addCriterion("songStatus in", values, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusNotIn(List<Integer> values) {
            addCriterion("songStatus not in", values, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusBetween(Integer value1, Integer value2) {
            addCriterion("songStatus between", value1, value2, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSongstatusNotBetween(Integer value1, Integer value2) {
            addCriterion("songStatus not between", value1, value2, "songstatus");
            return (Criteria) this;
        }

        public Criteria andSonghotIsNull() {
            addCriterion("songHot is null");
            return (Criteria) this;
        }

        public Criteria andSonghotIsNotNull() {
            addCriterion("songHot is not null");
            return (Criteria) this;
        }

        public Criteria andSonghotEqualTo(Integer value) {
            addCriterion("songHot =", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotNotEqualTo(Integer value) {
            addCriterion("songHot <>", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotGreaterThan(Integer value) {
            addCriterion("songHot >", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotGreaterThanOrEqualTo(Integer value) {
            addCriterion("songHot >=", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotLessThan(Integer value) {
            addCriterion("songHot <", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotLessThanOrEqualTo(Integer value) {
            addCriterion("songHot <=", value, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotIn(List<Integer> values) {
            addCriterion("songHot in", values, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotNotIn(List<Integer> values) {
            addCriterion("songHot not in", values, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotBetween(Integer value1, Integer value2) {
            addCriterion("songHot between", value1, value2, "songhot");
            return (Criteria) this;
        }

        public Criteria andSonghotNotBetween(Integer value1, Integer value2) {
            addCriterion("songHot not between", value1, value2, "songhot");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}