package com.wymusic.pojo;

import java.util.Date;

public class User {
	
    private Integer userid;  //用户ID
   
    private String account;  //用户账号

    private String password; //用户密码

    private String username; //用户昵称

    private String userphone; //用户手机

    private String useremail; //用户邮箱

    private Integer userstatus;  //用户状态 0表示注销 1表示已登录  3为已删除 默认是已注销 

    private Integer userauthority; //用户权限：0是普通用户 1是管理员

    private String province; //用户所在省

    private String city;  //用户所在市

    private String userpicurl; //用户头像

    private String usersex;  //用户性别

    private Date createtime; //创建时间
    
    public User() {
    	
	}
    
    
    
    public User(String account, String password, String userphone) {
		this.account = account;
		this.password = password;
		this.userphone = userphone;
		this.userstatus = 0;
		this.userauthority = 0;
		this.createtime = new Date();
	}

    

	public User(Integer userid, String account, String password, String userphone, Integer userstatus,Integer userauthority) {
		this.userid = userid;
		this.account = account;
		this.password = password;
		this.userphone = userphone;
		this.userstatus = userstatus;
		this.userauthority = userauthority;
	}


	public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone == null ? null : userphone.trim();
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail == null ? null : useremail.trim();
    }

    public Integer getUserstatus() {
        return userstatus;
    }

    public void setUserstatus(Integer userstatus) {
        this.userstatus = userstatus;
    }

    public Integer getUserauthority() {
        return userauthority;
    }

    public void setUserauthority(Integer userauthority) {
        this.userauthority = userauthority;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getUserpicurl() {
        return userpicurl;
    }

    public void setUserpicurl(String userpicurl) {
        this.userpicurl = userpicurl == null ? null : userpicurl.trim();
    }

    public String getUsersex() {
        return usersex;
    }

    public void setUsersex(String usersex) {
        this.usersex = usersex == null ? null : usersex.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

	@Override
	public String toString() {
		return "User [userid=" + userid + ", account=" + account + ", password=" + password + ", username=" + username
				+ ", userphone=" + userphone + ", useremail=" + useremail + ", userstatus=" + userstatus
				+ ", userauthority=" + userauthority + ", province=" + province + ", city=" + city + ", userpicurl="
				+ userpicurl + ", usersex=" + usersex + ", createtime=" + createtime + "]";
	}
    
}