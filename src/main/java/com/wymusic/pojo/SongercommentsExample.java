package com.wymusic.pojo;

import java.util.ArrayList;
import java.util.List;

public class SongercommentsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SongercommentsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCommentidIsNull() {
            addCriterion("commentId is null");
            return (Criteria) this;
        }

        public Criteria andCommentidIsNotNull() {
            addCriterion("commentId is not null");
            return (Criteria) this;
        }

        public Criteria andCommentidEqualTo(Integer value) {
            addCriterion("commentId =", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotEqualTo(Integer value) {
            addCriterion("commentId <>", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThan(Integer value) {
            addCriterion("commentId >", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentId >=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThan(Integer value) {
            addCriterion("commentId <", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidLessThanOrEqualTo(Integer value) {
            addCriterion("commentId <=", value, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidIn(List<Integer> values) {
            addCriterion("commentId in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotIn(List<Integer> values) {
            addCriterion("commentId not in", values, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidBetween(Integer value1, Integer value2) {
            addCriterion("commentId between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andCommentidNotBetween(Integer value1, Integer value2) {
            addCriterion("commentId not between", value1, value2, "commentid");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNull() {
            addCriterion("songerId is null");
            return (Criteria) this;
        }

        public Criteria andSongeridIsNotNull() {
            addCriterion("songerId is not null");
            return (Criteria) this;
        }

        public Criteria andSongeridEqualTo(Integer value) {
            addCriterion("songerId =", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotEqualTo(Integer value) {
            addCriterion("songerId <>", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThan(Integer value) {
            addCriterion("songerId >", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridGreaterThanOrEqualTo(Integer value) {
            addCriterion("songerId >=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThan(Integer value) {
            addCriterion("songerId <", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridLessThanOrEqualTo(Integer value) {
            addCriterion("songerId <=", value, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridIn(List<Integer> values) {
            addCriterion("songerId in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotIn(List<Integer> values) {
            addCriterion("songerId not in", values, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridBetween(Integer value1, Integer value2) {
            addCriterion("songerId between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongeridNotBetween(Integer value1, Integer value2) {
            addCriterion("songerId not between", value1, value2, "songerid");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNull() {
            addCriterion("songerName is null");
            return (Criteria) this;
        }

        public Criteria andSongernameIsNotNull() {
            addCriterion("songerName is not null");
            return (Criteria) this;
        }

        public Criteria andSongernameEqualTo(String value) {
            addCriterion("songerName =", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotEqualTo(String value) {
            addCriterion("songerName <>", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThan(String value) {
            addCriterion("songerName >", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameGreaterThanOrEqualTo(String value) {
            addCriterion("songerName >=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThan(String value) {
            addCriterion("songerName <", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLessThanOrEqualTo(String value) {
            addCriterion("songerName <=", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameLike(String value) {
            addCriterion("songerName like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotLike(String value) {
            addCriterion("songerName not like", value, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameIn(List<String> values) {
            addCriterion("songerName in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotIn(List<String> values) {
            addCriterion("songerName not in", values, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameBetween(String value1, String value2) {
            addCriterion("songerName between", value1, value2, "songername");
            return (Criteria) this;
        }

        public Criteria andSongernameNotBetween(String value1, String value2) {
            addCriterion("songerName not between", value1, value2, "songername");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}