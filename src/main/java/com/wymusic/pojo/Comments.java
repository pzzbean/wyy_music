package com.wymusic.pojo;

import java.util.Date;

public class Comments {
    private Integer commentid;

    private Integer userid;

    private String username;

    private Integer status;

    private Date pubtime;

    private String context;

    public Integer getCommentid() {
        return commentid;
    }

    public void setCommentid(Integer commentid) {
        this.commentid = commentid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getPubtime() {
        return pubtime;
    }

    public void setPubtime(Date pubtime) {
        this.pubtime = pubtime;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context == null ? null : context.trim();
    }

	@Override
	public String toString() {
		return "Comments [commentid=" + commentid + ", userid=" + userid + ", username=" + username + ", status="
				+ status + ", pubtime=" + pubtime + ", context=" + context + "]";
	}

	public Comments(Integer userid, String username, Integer status, Date pubtime, String context) {
		super();
		this.userid = userid;
		this.username = username;
		this.status = status;
		this.pubtime = pubtime;
		this.context = context;
	}

	public Comments() {
		super();
	}
    
    
}