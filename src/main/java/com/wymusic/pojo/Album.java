package com.wymusic.pojo;

import java.util.Date;

public class Album {
    private Integer albumid; //专辑ID

    private String albumname;// 专辑名称

    private Integer songerid;//所属歌手ID

    private String songername;//所属歌手名称

    private String albumstyle;//专辑风格

    private String publiccompany;//出版公司

    private String publicarea;//出版地区

    private Date publictime;//出版时间

    private String albumoverurl;//封面图片地址

    private Integer albumstatus;//专辑状态 0为已下架 1为正常 2为无版权

    private Integer albumhot;//专辑热门程度

    private String albuminfo; //专辑简介

    public Integer getAlbumid() {
        return albumid;
    }

    public void setAlbumid(Integer albumid) {
        this.albumid = albumid;
    }

    public String getAlbumname() {
        return albumname;
    }

    public void setAlbumname(String albumname) {
        this.albumname = albumname == null ? null : albumname.trim();
    }

    public Integer getSongerid() {
        return songerid;
    }

    public void setSongerid(Integer songerid) {
        this.songerid = songerid;
    }

    public String getSongername() {
        return songername;
    }

    public void setSongername(String songername) {
        this.songername = songername == null ? null : songername.trim();
    }

    public String getAlbumstyle() {
        return albumstyle;
    }

    public void setAlbumstyle(String albumstyle) {
        this.albumstyle = albumstyle == null ? null : albumstyle.trim();
    }

    public String getPubliccompany() {
        return publiccompany;
    }

    public void setPubliccompany(String publiccompany) {
        this.publiccompany = publiccompany == null ? null : publiccompany.trim();
    }

    public String getPublicarea() {
        return publicarea;
    }

    public void setPublicarea(String publicarea) {
        this.publicarea = publicarea == null ? null : publicarea.trim();
    }

    public Date getPublictime() {
        return publictime;
    }

    public void setPublictime(Date publictime) {
        this.publictime = publictime;
    }

    public String getAlbumoverurl() {
        return albumoverurl;
    }

    public void setAlbumoverurl(String albumoverurl) {
        this.albumoverurl = albumoverurl == null ? null : albumoverurl.trim();
    }

    public Integer getAlbumstatus() {
        return albumstatus;
    }

    public void setAlbumstatus(Integer albumstatus) {
        this.albumstatus = albumstatus;
    }

    public Integer getAlbumhot() {
        return albumhot;
    }

    public void setAlbumhot(Integer albumhot) {
        this.albumhot = albumhot;
    }

    public String getAlbuminfo() {
        return albuminfo;
    }

    public void setAlbuminfo(String albuminfo) {
        this.albuminfo = albuminfo == null ? null : albuminfo.trim();
    }

	@Override
	public String toString() {
		return "Album [albumid=" + albumid + ", albumname=" + albumname + ", songerid=" + songerid + ", songername="
				+ songername + ", albumstyle=" + albumstyle + ", publiccompany=" + publiccompany + ", publicarea="
				+ publicarea + ", publictime=" + publictime + ", albumoverurl=" + albumoverurl + ", albumstatus="
				+ albumstatus + ", albumhot=" + albumhot + ", albuminfo=" + albuminfo + "]";
	}
    
}