package com.wymusic.pojo;

public class Songs {
    private Integer songid;

    private String songname;

    private String songstyle;

    private Integer albumid;

    private Integer songerid;

    private String mp3url;

    private String songurl;

    private String songlrcurl;

    private String songtime;

    private Integer songstatus;

    private Integer songhot;

    private String songlrc;

    public Integer getSongid() {
        return songid;
    }

    public void setSongid(Integer songid) {
        this.songid = songid;
    }

    public String getSongname() {
        return songname;
    }

    public void setSongname(String songname) {
        this.songname = songname == null ? null : songname.trim();
    }

    public String getSongstyle() {
        return songstyle;
    }

    public void setSongstyle(String songstyle) {
        this.songstyle = songstyle == null ? null : songstyle.trim();
    }

    public Integer getAlbumid() {
        return albumid;
    }

    public void setAlbumid(Integer albumid) {
        this.albumid = albumid;
    }

    public Integer getSongerid() {
        return songerid;
    }

    public void setSongerid(Integer songerid) {
        this.songerid = songerid;
    }

    public String getMp3url() {
        return mp3url;
    }

    public void setMp3url(String mp3url) {
        this.mp3url = mp3url == null ? null : mp3url.trim();
    }

    public String getSongurl() {
        return songurl;
    }

    public void setSongurl(String songurl) {
        this.songurl = songurl == null ? null : songurl.trim();
    }

    public String getSonglrcurl() {
        return songlrcurl;
    }

    public void setSonglrcurl(String songlrcurl) {
        this.songlrcurl = songlrcurl == null ? null : songlrcurl.trim();
    }

    public String getSongtime() {
        return songtime;
    }

    public void setSongtime(String songtime) {
        this.songtime = songtime == null ? null : songtime.trim();
    }

    public Integer getSongstatus() {
        return songstatus;
    }

    public void setSongstatus(Integer songstatus) {
        this.songstatus = songstatus;
    }

    public Integer getSonghot() {
        return songhot;
    }

    public void setSonghot(Integer songhot) {
        this.songhot = songhot;
    }

    public String getSonglrc() {
        return songlrc;
    }

    public void setSonglrc(String songlrc) {
        this.songlrc = songlrc == null ? null : songlrc.trim();
    }

	@Override
	public String toString() {
		return "Songs [songid=" + songid + ", songname=" + songname + ", songstyle=" + songstyle + ", albumid="
				+ albumid + ", songerid=" + songerid + ", mp3url=" + mp3url + ", songurl=" + songurl + ", songlrcurl="
				+ songlrcurl + ", songtime=" + songtime + ", songstatus=" + songstatus + ", songhot=" + songhot
				+ ", songlrc=" + songlrc + "]";
	}
    
}