package com.wymusic.mapper;

import com.wymusic.pojo.Album;
import com.wymusic.pojo.AlbumExample;
import com.wymusic.vo.PageVo;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumMapper {
    int countByExample(AlbumExample example);

    int deleteByExample(AlbumExample example);

    int deleteByPrimaryKey(Integer albumid);

    int insert(Album record);

    int insertSelective(Album record);

    List<Album> selectByExampleWithBLOBs(AlbumExample example);

    List<Album> selectByExample(AlbumExample example);

    Album selectByPrimaryKey(Integer albumid);

    int updateByExampleSelective(@Param("record") Album record, @Param("example") AlbumExample example);

    int updateByExampleWithBLOBs(@Param("record") Album record, @Param("example") AlbumExample example);

    int updateByExample(@Param("record") Album record, @Param("example") AlbumExample example);
    
    int updateByPrimaryKeySelective(Album record);

    int updateByPrimaryKeyWithBLOBs(Album record);

    int updateByPrimaryKey(Album record);
    
    List<Album> findByPage(PageVo pageVo);
    
    List<Album> findByPublicArea(String publicArea);
    
    int updateAlbumStatusById(Album album);
    
    List<Album> selectAlbumBySongerId(Integer songerid);
    
    Album selectAlbumByName(String albumName);
    
}