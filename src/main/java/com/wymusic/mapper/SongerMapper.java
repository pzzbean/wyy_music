package com.wymusic.mapper;

import com.wymusic.pojo.Songer;
import com.wymusic.pojo.SongerExample;
import com.wymusic.vo.PageVo;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SongerMapper {
    int countByExample(SongerExample example);

    int deleteByExample(SongerExample example);

    int deleteByPrimaryKey(Integer songerid);

    int insert(Songer record);

    int insertSelective(Songer record);

    List<Songer> selectByExampleWithBLOBs(SongerExample example);

    List<Songer> selectByExample(SongerExample example);

    Songer selectByPrimaryKey(Integer songerid);

    int updateByExampleSelective(@Param("record") Songer record, @Param("example") SongerExample example);

    int updateByExampleWithBLOBs(@Param("record") Songer record, @Param("example") SongerExample example);

    int updateByExample(@Param("record") Songer record, @Param("example") SongerExample example);

    int updateByPrimaryKeySelective(Songer record);

    int updateByPrimaryKeyWithBLOBs(Songer record);

    int updateByPrimaryKey(Songer record);
    
    Songer selectSongerByName(String songername);
    
    List<Songer> findByPage(PageVo pagevo);
    
    List<Songer> findBysongerCountry(List list);
    
    int updateSongerStatusById(Songer songer);
}