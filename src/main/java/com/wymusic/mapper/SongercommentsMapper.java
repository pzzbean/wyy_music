package com.wymusic.mapper;

import com.wymusic.pojo.Songercomments;
import com.wymusic.pojo.SongercommentsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SongercommentsMapper {
    int countByExample(SongercommentsExample example);

    int deleteByExample(SongercommentsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Songercomments record);

    int insertSelective(Songercomments record);

    List<Songercomments> selectByExample(SongercommentsExample example);

    Songercomments selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Songercomments record, @Param("example") SongercommentsExample example);

    int updateByExample(@Param("record") Songercomments record, @Param("example") SongercommentsExample example);

    int updateByPrimaryKeySelective(Songercomments record);

    int updateByPrimaryKey(Songercomments record);
}