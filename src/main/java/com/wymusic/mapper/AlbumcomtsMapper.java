package com.wymusic.mapper;

import com.wymusic.pojo.Albumcomts;
import com.wymusic.pojo.AlbumcomtsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumcomtsMapper {
    int countByExample(AlbumcomtsExample example);

    int deleteByExample(AlbumcomtsExample example);

    int deleteByPrimaryKey(Integer commentid);

    int insert(Albumcomts record);

    int insertSelective(Albumcomts record);

    List<Albumcomts> selectByExampleWithBLOBs(AlbumcomtsExample example);

    List<Albumcomts> selectByExample(AlbumcomtsExample example);

    Albumcomts selectByPrimaryKey(Integer commentid);

    int updateByExampleSelective(@Param("record") Albumcomts record, @Param("example") AlbumcomtsExample example);

    int updateByExampleWithBLOBs(@Param("record") Albumcomts record, @Param("example") AlbumcomtsExample example);

    int updateByExample(@Param("record") Albumcomts record, @Param("example") AlbumcomtsExample example);

    int updateByPrimaryKeySelective(Albumcomts record);

    int updateByPrimaryKeyWithBLOBs(Albumcomts record);

    int updateByPrimaryKey(Albumcomts record);
    
    Albumcomts selectMaxId();
}