package com.wymusic.mapper;

import com.wymusic.pojo.Songlist;
import com.wymusic.pojo.SonglistExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SonglistMapper {
    int countByExample(SonglistExample example);

    int deleteByExample(SonglistExample example);

    int deleteByPrimaryKey(Integer songlistid);

    int insert(Songlist record);

    int insertSelective(Songlist record);

    List<Songlist> selectByExampleWithBLOBs(SonglistExample example);

    List<Songlist> selectByExample(SonglistExample example);

    Songlist selectByPrimaryKey(Integer songlistid);

    int updateByExampleSelective(@Param("record") Songlist record, @Param("example") SonglistExample example);

    int updateByExampleWithBLOBs(@Param("record") Songlist record, @Param("example") SonglistExample example);

    int updateByExample(@Param("record") Songlist record, @Param("example") SonglistExample example);

    int updateByPrimaryKeySelective(Songlist record);

    int updateByPrimaryKeyWithBLOBs(Songlist record);

    int updateByPrimaryKey(Songlist record);
    
    List<Songlist> findSongListByUserId(Integer userId);
}