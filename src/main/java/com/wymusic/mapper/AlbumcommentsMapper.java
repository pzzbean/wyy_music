package com.wymusic.mapper;

import com.wymusic.pojo.Albumcomments;
import com.wymusic.pojo.AlbumcommentsExample;
import com.wymusic.pojo.Albumcomts;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AlbumcommentsMapper {
    int countByExample(AlbumcommentsExample example);

    int deleteByExample(AlbumcommentsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Albumcomments record);

    int insertSelective(Albumcomments record);

    List<Albumcomments> selectByExample(AlbumcommentsExample example);

    Albumcomments selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Albumcomments record, @Param("example") AlbumcommentsExample example);

    int updateByExample(@Param("record") Albumcomments record, @Param("example") AlbumcommentsExample example);

    int updateByPrimaryKeySelective(Albumcomments record);

    int updateByPrimaryKey(Albumcomments record);
    
    List<Albumcomments> findAlbumComtsByAlbumId(Integer albumId);
}