package com.wymusic.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.wymusic.pojo.Songlistandsongs;
import com.wymusic.pojo.SonglistandsongsExample;

@Repository
public interface SonglistandsongsMapper {
    int countByExample(SonglistandsongsExample example);

    int deleteByExample(SonglistandsongsExample example);

    int deleteByPrimaryKey(Integer songlistandsongsid);

    int insert(Songlistandsongs record);

    int insertSelective(Songlistandsongs record);

    List<Songlistandsongs> selectByExample(SonglistandsongsExample example);

    Songlistandsongs selectByPrimaryKey(Integer songlistandsongsid);

    int updateByExampleSelective(@Param("record") Songlistandsongs record, @Param("example") SonglistandsongsExample example);

    int updateByExample(@Param("record") Songlistandsongs record, @Param("example") SonglistandsongsExample example);

    int updateByPrimaryKeySelective(Songlistandsongs record);

    int updateByPrimaryKey(Songlistandsongs record);
    
    List<Songlistandsongs> findSongBySongListId(Integer songlistId);
    
    void updateSonglistNameBysonglistId(Songlistandsongs songlistandsongs);
    
    Songlistandsongs songsExist(Songlistandsongs parm);
}