package com.wymusic.mapper;

import com.wymusic.pojo.Songscomments;
import com.wymusic.pojo.SongscommentsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SongscommentsMapper {
    int countByExample(SongscommentsExample example);

    int deleteByExample(SongscommentsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Songscomments record);

    int insertSelective(Songscomments record);

    List<Songscomments> selectByExample(SongscommentsExample example);

    Songscomments selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Songscomments record, @Param("example") SongscommentsExample example);

    int updateByExample(@Param("record") Songscomments record, @Param("example") SongscommentsExample example);

    int updateByPrimaryKeySelective(Songscomments record);

    int updateByPrimaryKey(Songscomments record);
    
    List<Songscomments> findCommentsBySongId(Integer songId);
}