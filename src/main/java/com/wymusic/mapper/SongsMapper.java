package com.wymusic.mapper;

import com.wymusic.pojo.Songs;
import com.wymusic.pojo.SongsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SongsMapper {
    int countByExample(SongsExample example);

    int deleteByExample(SongsExample example);

    int deleteByPrimaryKey(Integer songid);

    int insert(Songs record);

    int insertSelective(Songs record);

    List<Songs> selectByExampleWithBLOBs(SongsExample example);

    List<Songs> selectByExample(SongsExample example);

    Songs selectByPrimaryKey(Integer songid);

    int updateByExampleSelective(@Param("record") Songs record, @Param("example") SongsExample example);

    int updateByExampleWithBLOBs(@Param("record") Songs record, @Param("example") SongsExample example);

    int updateByExample(@Param("record") Songs record, @Param("example") SongsExample example);

    int updateByPrimaryKeySelective(Songs record);

    int updateByPrimaryKeyWithBLOBs(Songs record);

    int updateByPrimaryKey(Songs record);
    
    List<Songs> selectSongBySongerId(Integer songerid);
    
    List<Songs> selectSongByAlbumId(Integer albumid);
    
    List<Songs> selectBySongHotDesc();
    
    List<Songs> selectBySongIdDesc();
    
    List<Songs> selectBySongerCountryAndSongHotDesc(List list);
}