package com.wymusic.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wymusic.mapper.AlbumMapper;
import com.wymusic.mapper.SongerMapper;
import com.wymusic.mapper.SongsMapper;
import com.wymusic.pojo.Album;
import com.wymusic.pojo.Songer;
import com.wymusic.pojo.Songs;
import com.wymusic.vo.PageVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:config/applicationContext-dao.xml")
public class AlbumTest {
	
	@Autowired
	private AlbumMapper albumMapper;
	
	@Autowired
	private SongerMapper songerMapper;
	
	@Autowired
	private SongsMapper songsMapper;
	
	
	@Test
	public void findByPage() {
		int pageSize = 20;
		int page = 1*pageSize-pageSize;
		
		PageVo pVo=new PageVo(page,pageSize, 0,null);
		
		List<Album> albumList = albumMapper.findByPage(pVo);
		for (Album album : albumList) {
			System.out.println("---->"+album);
		}
	}
	
	@Test
	public void findByPublicArea(){
		String publicArea="未知";
		List<Album> albumList = albumMapper.findByPublicArea(publicArea);
		for (Album album : albumList) {
			System.out.println("---->"+album);
		}
	}
	
	@Test
	public void findBySongerCountry(){
		List list =new ArrayList<>();
//		String parm="法国,德国,意大利,荷兰,比利时,卢森堡,英国,丹麦,爱尔兰,希腊,西班牙,葡萄牙,"
//		        +"奥地利,瑞典,芬兰塞,浦路斯,匈牙利,捷克,爱沙尼亚,拉脱维亚,立陶宛,马耳他,波兰,"
//				+"斯洛伐克,斯洛文尼亚,美国,挪威";
//		
//		String[] strArray = parm.split(",");
//		for (String string : strArray) {
//			list.add(string);
//		}
		list.add("日本");
		list.add("韩国");
		List<Songs> resList = songsMapper.selectBySongerCountryAndSongHotDesc(list);
		for (Songs song : resList) {
			System.out.println(song);
		}
	}
	
	@Test
	public void testSongs(){
		Songs song = new Songs();
		song.setSongid(Integer.valueOf("12"));
		song.setSongstatus(Integer.valueOf("0"));
		songsMapper.updateByPrimaryKeySelective(song);
		System.out.println(song);
	}
}
