$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/userList",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
		   var number = 0;
		   $("strong").html(""+res.data.length);
		   var html="";
		   $.each(res.data, function(index, obj){ 
			  var time=new Date(obj.createtime);
			  var createtime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate()
			               +" "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();
			  
			  var status="";
			  if(obj.userstatus==0){
				  status="已注销";
			  }else if(obj.userstatus==1){
				  status="已登录";
			  }else{
				  number = number+1;
				  status="已删除"; 
			  }
			  if(obj.userstatus==3){
				  $("strong").html(""+number);
				  html=html+"<tr class='text-c'>"
	               +"<td><input type='checkbox' value='"+obj.userid+"' name='userid' id='userid'></td>"
	               +"<td>"+obj.userid+"</td>"
	               +"<td style='text-align: left;'><img class='userPic' src='/pic/"+obj.userpicurl+"' style='width: 25px;height: 25px;border-radius: 15px;'>"
	               +"<span style='cursor:pointer;padding-left: 6px;' class='text-primary'>"+obj.username+"</span></td>"
	               +"<td>"+obj.usersex+"</td>"
	               +"<td>"+obj.userphone+"</td>"
	               +"<td>"+obj.useremail+"</td>"
	               +"<td>"+obj.province+" "+obj.city+"</td>"
	               +"<td style='text-align: left;'>"+createtime+"</td>"
	               +"<td class='td-status'><span class='label label-danger radius'>"+status+"</span></td>"
	               +"<td class='td-manage'>"
	               +"<a style='text-decoration:none; margin: 0 15px;' href='javascript:;' onclick='recoverUser(this)' title='还原'>"
	               +"<i class='Hui-iconfont'>&#xe66b;</i>"
	               +"</a>"
	               +"<a title='删除' href='javascript:;' onclick='delUserFromData(this)' class='ml-5' style='text-decoration:none'>"
	               +"<i class='Hui-iconfont'>&#xe6e2;</i>"
	               +"</a>"
	               +"</td>"
	               +"</tr>"
			  }
		   });
		   $("tbody").append(html);
		}
	});
	
	
	$('.table-sort').dataTable({
		"aaSorting": [[ 1, "desc" ]],//默认第几个排序
		"bStateSave": true,//状态保存
		"aLengthMenu" : [5, 10, 15,25,100 ],//更改显示条数
		"aoColumnDefs": [
		  //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
		  {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
		]
	});
})

/*用户-还原*/
function recoverUser(curr){
	layer.confirm('确认要还原吗？',function(){
		var rowindex = $(curr).parent().parent().find("td");
		var userid = rowindex.eq(1).text()
		$.ajax({
			type:"post",
			url:"/wymusic/user/recoverUser",
			async: false,
			data:{'userid':userid},
			dataType: "json",//返回数据类型
			success: function(res){
				alert(res.massage);
				location.reload();
			}
		});
	});
}

/*用户-删除*/
function delUserFromData(curr){
	layer.confirm('确认要永久删除该用户吗？',function(){
		var rowindex = $(curr).parent().parent().find("td");
		var userid = rowindex.eq(1).text()
		$.ajax({
			type:"post",
			url:"/wymusic/user/delUserFromData",
			async: false,
			data:{'userid':userid},
			dataType: "json",//返回数据类型
			success: function(res){
				alert(res.massage);
				location.reload();
			}
		});
	});
}