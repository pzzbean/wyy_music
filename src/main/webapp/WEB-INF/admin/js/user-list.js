$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/userList",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
	      var number = 0;
		   var html="";
		   $.each(res.data, function(index, obj){ 
			  var status="";
			  if(obj.userstatus==0){
				  number = number+1;
				  status="已注销";
			  }else if(obj.userstatus==1){
				  number = number+1;
				  status="已登录";
			  }else{
				  status="已删除"; 
			  }
			  $("strong").html(""+number);
			  var time=new Date(obj.createtime);
			  var createtime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate()
			               +" "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();
			  
			  if(obj.userstatus==0||obj.userstatus==1){
				  html=html+"<tr class='text-c'>"
	               +"<td><input type='checkbox' value='"+obj.userid+"' name='userid' id='userid'></td>"
	               +"<td>"+obj.userid+"</td>"
	               +"<td style='text-align: left;'><img class='userPic' src='/pic/"+obj.userpicurl+"' style='width: 25px;height: 25px;border-radius: 15px;'>"
	               +"<span style='cursor:pointer;padding-left: 6px;' class='text-primary'>"+obj.username+"</span></td>"
	               +"<td>"+obj.usersex+"</td>"
	               +"<td>"+obj.userphone+"</td>"
	               +"<td>"+obj.useremail+"</td>"
	               +"<td>"+obj.province+" "+obj.city+"</td>"
	               +"<td style='text-align: left;'>"+createtime+"</td>"
	               +"<td class='td-status'><span class='label label-success radius'>"+status+"</span></td>"
	               +"<td class='td-manage'>"
	               +"<a title='编辑用户信息' href='javascript:;' class='ml-5 user-edit' style='text-decoration:none; margin: 0 15px;' onclick='userEdit(this)'>"
	               +"<i class='Hui-iconfont'>&#xe6df;</i>"
	               +"</a>"
	               +"<a style='text-decoration:none' class='ml-5' href='javascript:;' title='修改密码'  onclick='changePssword(this)'>"
	               +"<i class='Hui-iconfont'>&#xe63f;</i>"
	               +"</a>"
	               +"<a title='删除用户' href='javascript:;' class='ml-5' style='text-decoration:none; margin: 0 15px;' onclick='delUser(this)'>"
	               +"<i class='Hui-iconfont'>&#xe6e2;</i>"
	               +"</a></td></tr>"
			  }
		   });
		   $("tbody").append(html);
		}
	});
	
	$('.table-sort').dataTable({
		"aaSorting": [[ 1, "desc" ]],//默认第几个排序
		"bStateSave": true,//状态保存
		"aLengthMenu" : [ 5, 10, 15,25,100 ],//更改显示条数
		"aoColumnDefs": [
//		  {"bVisible": false, "aTargets": [ 3 ]}, //控制列的隐藏显示
		  {"orderable":false,"aTargets":[0,8,9]}// 制定列不参与排序
		]
	});
})

function userEdit(curr){
	var rowindex = $(curr).parent().parent().find("td");
	var index = rowindex.eq(1).text()
	layer_show('编辑',"user-edit.html?userId="+index+"",'510','510');
}

function changePssword(curr){
	var rowindex = $(curr).parent().parent().find("td");
	var index = rowindex.eq(1).text()
	layer_show('修改密码',"change-password.html?userId="+index+"",'520','270');
}

function delUser(curr){
	var rowindex = $(curr).parent().parent().find("td");
	var userid = rowindex.eq(1).text()
	layer.confirm('确认要删除吗？',function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/delUser",
			async: false,
			data:{'userid':userid},
			dataType: "json",//返回数据类型
			success: function(res){
				alert(res.massage);
				location.reload();
			}
		});
	});
}
//layer.closeAll('dialog');

