$(function(){
	var url = location.href;
	if(url!=""){
	   var index=url.substr(url.indexOf("=") + 1);
		$.ajax({
			type:"post",
			url:"/wymusic/user/findUserById",
			async: false,
			data:{'userId':index},
			dataType: "json",//返回数据类型
			success: function(res){
				$("#userid").attr("value",res.data.userid);
				$("#account").html(res.data.account);
			}
		});
	}
	
	$("#form-change-password").validate({
		rules:{
			newpassword:{
				required:true,
				minlength:6,
				maxlength:16
			},
			newpassword2:{
				required:true,
				minlength:6,
				maxlength:16,
				equalTo: "#newpassword"
			},
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
				type:"post",
				url:"/wymusic/user/updateUserPassword",
				async: false,
				dataType: "json",//返回数据类型
				success: function(res){
					alert(res.massage);
				}
			});
			var index = parent.layer.getFrameIndex(window.name);
			parent.$('.btn-refresh').click();
			parent.layer.close(index);
		}
	});
})