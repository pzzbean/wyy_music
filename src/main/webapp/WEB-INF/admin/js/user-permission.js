$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/userList",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
	      var number = 0;
		   var html="";
		   $.each(res.data, function(index, obj){ 
			  var status="";
			  if(obj.userstatus==0){
				  number = number+1;
				  status="已注销";
			  }else if(obj.userstatus==1){
				  number = number+1;
				  status="已登录";
			  }else{
				  status="已删除"; 
			  }
			  $("strong").html(""+number);
			  var time=new Date(obj.createtime);
			  var createtime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate()
			               +" "+time.getHours()+":"+time.getMinutes()+":"+time.getSeconds();
			  
			  if(obj.userstatus==0||obj.userstatus==1){
				  var userAuthority = "";
				  if(0==obj.userauthority){
					  userAuthority="普通用户"
				  }else if(1==obj.userauthority){
					  userAuthority="管理员"
				  }else{
					  userAuthority=""
				  }
				  html=html+"<tr class='text-c'>"
	               +"<td><input type='checkbox' value='"+obj.userid+"' name='userid' id='userid'></td>"
	               +"<td>"+obj.userid+"</td>"
	               +"<td style='text-align: left;'><img class='userPic' src='/pic/"+obj.userpicurl+"' style='width: 25px;height: 25px;border-radius: 15px;'>"
	               +"<span style='cursor:pointer;padding-left: 6px;' class='text-primary'>"+obj.account+"</span></td>"
	               +"<td>"+userAuthority+"</td>"
	               +"<td>"
	               +"<a title='更改用户权限' href='javascript:;' class='ml-5' style='text-decoration:none' onclick='updateUserAuthority(this)'>"
				   +"<i class='Hui-iconfont'>&#xe6df;</i>"
				   +"</a></td>" 
			  }
		   });
		   $("tbody").append(html);
		}
	 });
})

function updateUserAuthority(curr){
	var rowindex = $(curr).parent().parent().find("td");
	var index = rowindex.eq(1).text()
	layer_show('修改密码',"user-authority.html?userId="+index+"",'420','270');
}