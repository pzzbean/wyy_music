var userPicUrl="";
$(function(){
	var url = location.href;
	if(url!=""){
	   var index=url.substr(url.indexOf("=") + 1);
		$.ajax({
			type:"post",
			url:"/wymusic/user/findUserById",
			async: false,
			data:{'userId':index},
			dataType: "json",//返回数据类型
			success: function(res){
				userPicUrl = res.data.userpicurl;
				$("#userimg").attr("src","/pic/"+res.data.userpicurl);
				$("#userid").attr("value",res.data.userid);
				$("#username").attr("value",res.data.username);
				$("#mobile").attr("value",res.data.userphone);
				$("#useremail").attr("value",res.data.useremail);
				if("女"==res.data.usersex){
					$("#sex-2").attr("checked",true);
				}else{
					$("#sex-1").attr("checked",true);
				}
				
				//城市二级联动
				if(""!=res.data.province&&""!=res.data.city){
					$("#city_3").citySelect({
			            prov: res.data.province,
			            city: res.data.city
				    });
				}else if(""!=res.data.province&&""==res.data.city){
					$("#city_3").citySelect({
			            prov: res.data.province,
			            required:false,
				    });
				}else{
					$("#city_3").citySelect({
						required:false,
				    });
				}
			}
		});
	}
	
	$("#userPicfile").change(function(){
		var form=document.getElementById("form-member-edit");
		var formData = new FormData(form);
		$.ajax({
			type:"post",
			url:"/wymusic/user/showUserPic",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
				userPicUrl = res.str;
				$("#userimg").attr("src","/pic/"+res.str);
			}
		})
	});
	
})

//保存用户
function sumbitFrom(){
	var form=document.getElementById("form-member-edit");
	var formData = new FormData(form);
    formData.append("userPicUrl", userPicUrl);
	$.ajax({
		type:"post",
		url:"/wymusic/user/updateUser",
		async: false,
		data: formData,  
	    processData: false,  
	    contentType: false, 
		dataType: "json",//返回数据类型
		success: function(res){
		   alert(res.massage);
		}
	})
	parent.location.reload();
}
