var user;
$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					user = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	var songidarr;
	var url = location.href;
	if(url!=""){
		var albumid = url.substr(url.indexOf("=") + 1);
		$.post("/wymusic/album/findAlbumDetailInfoById",{'albumid':albumid},function(res){
			var data = res.data;
			songidarr = data.songList;
			var songlist="";
			var time=new Date(data.album.publictime);
		    var publictime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate();
		    $("#albumPic").attr("src","/pic/"+data.album.albumoverurl);
			$(".data__name_txt").html(data.album.albumname);
			$(".data__name_txt").attr("title",data.album.albumname);
			$("#songer").html(data.album.songername);
			$("#songer").attr("title",data.album.songername);
			$("#songer").attr("href","songer-detail.html?songerid="+data.album.songerid+"");
			$("#style").append(data.album.albumstyle);
			$("#area").append(data.album.publicarea);
			$("#company").append(data.album.publiccompany);
			$("#publictime").append(publictime);
			for (var i = 0; i < data.songList.length; i++) {
				songlist=songlist+""
				        +"<div class='song-playlist'>"
				        +"<ul class='playList'>"
				        +"<li class='playList-number'><div>"+(i+1)+"</div></li>"
				        +"<li class='playList-songname'><input class='hide' type='text' id='songid' value='"+data.songList[i].songid+"'></input><a href='songs.html?songsid="+data.songList[i].songid+"' style='float: left;'>"+data.songList[i].songname+"</a>"
				        +"<a href='#songSetting' data-toggle='collapse' class='playList-sign addto_songlist'><i class='glyphicon glyphicon-plus sign'></i></a>"
				        +"<a href='"+data.songList[i].mp3url+"' download='"+data.songList[i].mp3url+"' class='playList-sign'><i class='glyphicon glyphicon-save sign'></i></a>"
				        +"<a href='player.html?songid="+data.songList[i].songid+"' class='playList-sign'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
				        +"</li>"
				        +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songList[i].songerid+"'>"+data.album.songername+"</a></li>"
				        +"<li class='playList-songtime'>"+data.songList[i].songtime+"</li>"
				        +"</ul>"
				        +"</div>";
			}
			$(".songs-list-datas").append(songlist);
		});
		
		$.ajax({
			type:"post",
			url:"/wymusic/comments/findAlbumComtsByAlbumId",
			async: false,
			data:{'albumId':albumid},
			dataType: "json",//返回数据类型
			success: function(res){
				data = res.data;
				if(data.albumcomtsList.length!=0){
					var html="";
					var number = 0;
					for (var i = 0; i < data.albumcomtsList.length; i++) {
						if(data.albumcomtsList[i].status==1){
							number = number+1;
							var time=new Date(data.albumcomtsList[i].pubtime);
							var pubtime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate();
							html=html+"<li class='comment-data'>"
							    +"<a><img alt='' src='/pic/"+data.userList[i].userpicurl+"' class='userPic'></a>"
							    +"<h4><a class='username'>"+data.userList[i].username+"</a></h4>"
							    +"<div><p class='comment-text'>"+data.albumcomtsList[i].context+"</p></div>"
							    +"<div><span class='comment-time'>"+pubtime+"</span></div>"
							    +"</li>"
						}
					}
					$(".comment-title").html("精彩评论("+number+")");
					$(".show-comments ul").html(html);
				}else{
					$(".comment-title").html("精彩评论(0)");
					$(".show-comments ul").html("暂时没有任何评论，快来抢占沙发吧！");
				}
			}
		});
	}
	
	$("#playAll").click(function(){
		var songidList=new Array();
		for (var i = 0; i < songidarr.length; i++) {
			songidList.push(songidarr[i].songid);
		}
		window.location.href="player.html?songidList="+songidList+"";
	});
	
	$(".sendMyidea").click(function(){
		var cotext = $("textarea").val();
		var albumid = url.substr(url.indexOf("=") + 1);
		if(user!==undefined){
			var userid=user.userid;
			$.ajax({
				type:"post",
				url:"/wymusic/comments/addAlbumcomts",
				async: false,
				data:{"AlbumId":albumid,"cotext":cotext,"userid":userid},
				dataType: "json",
				success: function(res){
					alert(res.massage);
					$("textarea").val("");
					location.reload();
				}
			});
		}else{
			alert("亲，要登录后才可以评论专辑哟！");
		}
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
	
	$("#comments").click(function(){
		var a = $("#songs-comments").offset().top;  
		$("html,body").animate({scrollTop:a}, 'slow');   
	});
	
	$(".addto_songlist").click(function(){
		var songid = $(this).parent().find("#songid").val();
		if(user!==undefined){
			var userid=user.userid;
			var x = $(this).offset().top;
//			var y = $(this).offset().left;
			$("#songSetting").css("top",x+"px");
			$.ajax({
				type:"post",
				url:"/wymusic/songlist/findSongListByUserId",
				async: false,
				data:{"userId":userid},
				dataType: "json",//返回数据类型
				success: function(res){
					songlistArr = res.data.songListAndSongsDetailsList;
					html="<li><a href='javascript:;' class='songlistname add_songlist'>新建歌单</a></li>";
					for (var i = 0; i < songlistArr.length; i++) {
						if(songlistArr[i].songlist.songliststatus==1){
							html=html+"<li>"
							    +"<input class='mysonglistid hide' value='"+songlistArr[i].songlist.songlistid+"'>"
							    +"<a href='javascript:;' class='songlistname mysonglist'>"+songlistArr[i].songlist.songlistname+"</a>"
							    +"</li>"
						}
					}
					$("#songSetting").html(html);
				}
			});
			
			$(".mysonglist").click(function(){
				var songlistid = $(this).parent().find("input").val();
				if(user!==undefined){
					$.ajax({
						type:"post",
						url:"/wymusic/songlist/addToSongList",
						async: false,
						data:{"songlistid":songlistid,"songid":songid},
						dataType: "json",//返回数据类型
						success: function(res){
							$("#songSetting").removeClass("in");
							alert(res.massage);
						}
					});
				}
			});
		}else{
			alert("亲，要登录后才可以添加歌曲哟！");
		}
	});
})