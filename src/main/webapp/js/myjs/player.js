$(function (){
	var user;
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					user = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	var url = location.href;
	if(url!=""){
		var songidlist = url.substr(url.indexOf("=") + 1);
		$.ajax({
			type:"post",
			url:"/wymusic/songs/findSongsByIdList",
			async: false,
			data:{"songidlist":songidlist},
			dataType: "json",//返回数据类型
			success: function(res){
				var data=res.data;
				var html="";
				for (var i = 0; i < data.songList.length; i++) {
					html=html+""
					    +"<div class='song-playlist'>"
					    +"<ul class='playList'>"
					    +"<input type='text' class='hide songindex' value='"+i+"'>"
					    +"<li class='playList-number'>"
					    +"<div style='margin-top: -3px;'>"
					    +"<input type='checkbox' class='box' >"
					    +"<label class='number' >"+(i+1)+"</label>"
					    +"</div>"
					    +"</li>"
					    +"<li class='playList-songname'><input class='hide' type='text' id='songid' value='"+data.songList[i].songid+"'></input><a href='songs.html?songid="+data.songList[i].songid+"' style='float: left;' id='songname'>"+data.songList[i].songname+"</a>"
					    +"<a href='#songSetting' data-toggle='collapse' class='playList-sign addto_songlist'><i class='glyphicon glyphicon-plus sign'></i></a>"
					    +"<a href='"+data.songList[i].mp3url+"' download='"+data.songList[i].songname+"' class='playList-sign currdownload'><i class='glyphicon glyphicon-save sign'></i></a>"
					    +"<a href='javascript:;' class='playList-sign currplay'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
					    +"</li>"
					    +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songerList[i].songerid+"' style='float: left;color: #b0b0b0;' id='songername'>"+data.songerList[i].songername+"</a></li>"
					    +"<li class='playList-songtime' id='songtime'>"+data.songList[i].songtime+"</li>"
					    +"</ul>"
					    +"</div>";
					
					$("#audio").append("<source title='"+data.songList[i].songname+"' data-img='"+data.songList[i].songurl+"' src='"+data.songList[i].mp3url+"'>")
					    
				}
				
				$(".song-playlist-context").append(html);
			}
			
		});
	}
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
	
	$(".addto_songlist").click(function(){
		var songid = $(this).parent().find("#songid").val();
		if(user!==undefined){
			var userid=user.userid;
			var x = $(this).offset().top;
//			var y = $(this).offset().left;
			$("#songSetting").css("top",(x-190)+"px");
			$.ajax({
				type:"post",
				url:"/wymusic/songlist/findSongListByUserId",
				async: false,
				data:{"userId":userid},
				dataType: "json",//返回数据类型
				success: function(res){
					songlistArr = res.data.songListAndSongsDetailsList;
					html="<li><a href='javascript:;' class='songlistname add_songlist'>新建歌单</a></li>";
					for (var i = 0; i < songlistArr.length; i++) {
						if(songlistArr[i].songlist.songliststatus==1){
							html=html+"<li>"
							    +"<input class='mysonglistid hide' value='"+songlistArr[i].songlist.songlistid+"'>"
							    +"<a href='javascript:;' class='songlistname mysonglist'>"+songlistArr[i].songlist.songlistname+"</a>"
							    +"</li>"
						}
					}
					$("#songSetting").html(html);
				}
			});
			
			$(".mysonglist").click(function(){
				var songlistid = $(this).parent().find("input").val();
				if(user!==undefined){
					$.ajax({
						type:"post",
						url:"/wymusic/songlist/addToSongList",
						async: false,
						data:{"songlistid":songlistid,"songid":songid},
						dataType: "json",//返回数据类型
						success: function(res){
							$("#songSetting").removeClass("in");
							alert(res.massage);
						}
					});
				}
			});
		}else{
			alert("亲，要登录后才可以添加歌曲哟！");
		}
	});
})