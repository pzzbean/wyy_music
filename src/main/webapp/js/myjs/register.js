$(function(){
//	验证用户名格式和用户名是否存在
    $("input[name='account']").keyup(function(value){
		var account = $("input[name='account']").val();
		var uPattern = /^[a-zA-Z0-9_]{6,16}$/;
		var flg = uPattern.test(account);
		if(account!=""&&flg){
			$.ajax({
				type:"post",
				url:"/wymusic/user/validataAccount",
				async: false,
				data:{'account':account},
				dataType: "json",//返回数据类型
				success: function(res){
				   if(res.more){
					   $("#accountTip").html(res.massage);
					   $("#accountTip").css("color","red")
					   $("#accountTip").css("display","inline")
					   
				   }else{
					   $("#accountTip").html(res.massage);
					   $("#accountTip").css("color","green")
					   $("#accountTip").css("display","inline")
				   }
				}
			});
		}else if(account.length<6){
			$("#accountTip").css("display","none")
		}else{
			$("#accountTip").html("用户名格式不正确！");
			$("#accountTip").css("color","red")
			$("#accountTip").css("display","inline")
		}
	});
	
	//验证手机格式和是否已被注册
	$("input[name='userPhone']").keyup(function(){
		var userPhone = $("input[name='userPhone']").val();
		var mPattern = /^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0-3,5-9]))\d{8}$/;
		var flg2 = mPattern.test(userPhone);
		if(userPhone!=""&&flg2){
			$.ajax({
				type:"post",
				url:"/wymusic/user/validataUserPhone",
				async: false,
				data:{'userPhone':userPhone},
				dataType: "json",//返回数据类型
				success: function(res){
					if(res.more){
						   $("#phoneTip").html(res.massage);
						   $("#phoneTip").css("color","red")
						   $("#phoneTip").css("display","inline")
					   }else{
						   $("#phoneTip").html(res.massage);
						   $("#phoneTip").css("color","green")
						   $("#phoneTip").css("display","inline")
					   }
				}
			});
		}else if(userPhone.length<11){
			$("#phoneTip").css("display","none")
		}else{
			$("#phoneTip").html("请输入正确的手机号码！");
			$("#phoneTip").css("color","red")
			$("#phoneTip").css("display","inline")
		}
	});
	
	//验证密码格式
	$("input[name='password']").keyup(function(){
		var password = $("input[name='password']").val();
		var uPattern = /^[a-zA-Z0-9_]{6,16}$/;
		var flg3 = uPattern.test(password);
		if(password!=""&&flg3){
			$("#pwdTip").html("密码可用");
			$("#pwdTip").css("color","green")
			$("#pwdTip").css("display","inline")
		}else if(password.length<6){
			$("#pwdTip").css("display","none")
		}else if(password.length>16){
			$("#pwdTip").html("密码格式不正确！");
			$("#pwdTip").css("color","red")
			$("#pwdTip").css("display","inline");
		}
	});
	
	//用户注册
	$("#regist-btn").click(function(){
		var account = $("input[name='account']").val();
		var userPhone = $("input[name='userPhone']").val();
		var password = $("input[name='password']").val();
		var isChecked = $("input[name='checkName']").prop('checked')
		var uPattern = /^[a-zA-Z0-9_]{6,16}$/;
		var mPattern = /^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0-3,5-9]))\d{8}$/;
		var flg = uPattern.test(account);
		var flg1 = uPattern.test(password);
		var flg2 = mPattern.test(userPhone);
		if(account!=""&&userPhone!="" && password!=""&&flg&&flg1&&flg2){
			if(isChecked){
				$.ajax({
					type:"post",
					url:"/wymusic/user/insertUser",
					async: false,
					data:{'account':account,'userPhone':userPhone,'password':password},
					dataType: "json",//返回数据类型
					success: function(res){
						alert(res.massage);
//						setTimeout("location.href='index.html'",5000);
						var t=5;
						showTime(t);
					}
				});
			}else{
				alert("请仔细阅读相关协议！");
			}
		}else{
			alert("请填写正确注册信息！");
		}
	});
})

function showTime(t){
    if(t==1){  
    	location.href='index.html';  
    }
    t=t-1;
	$("#success-tip").css("display","inline");
	$("#delaytime").html("页面将在"+t+"秒后跳转到主页");
    //每秒执行一次,showTime()  
    setTimeout("showTime("+t+")",1000);  
}  
