$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			console.log(res.data);
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	$.ajax({
		type:"post",
		url:"/wymusic/album/findAll",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			var data=res.data
			html="";
			for (var i = 0; i < data.length; i++) {
				if (data[i].albumstatus==1&&i<15) {
					html=html+"<li>"
					+"<a href='album-detail.html?adbumid="+data[i].albumid+"' class='img'>"
					+"<img src='/pic/"+data[i].albumoverurl+"' alt='#' class='albumPic'>"
					+"<span class='mask'></span>"
					+"<i class='icon-play'></i>"
					+"</a>"
					+"<div class='info'>"
					+"<div class='title'>"
					+"<a href='album-detail.html?adbumid="+data[i].albumid+"'>"+data[i].albumname+"</a>"
					+"</div>"
					+"<a href='songer-detail.html?songerid="+data[i].songerid+"' class='author'>"+data[i].songername+"</a>"
					+"</div>"
					+"</li>"
				}
			}
			$(".slider-wrapper").html(html);
		}
		
	});
	
	toplist(".toplist1","巅峰热歌榜");
	toplist(".toplist2","巅峰新歌榜");
	toplist(".toplist3","华语热歌榜");
	toplist(".toplist4","欧美热歌榜");
	toplist(".toplist5","日韩热歌榜");
	newSong();
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
})

//榜单
function toplist(classname,area){
	$.ajax({
		type:"post",
		url:"/wymusic/songs/songsTopListByArea",
		async: false,
		data:{'area':area},
		dataType: "json",//返回数据类型
		success: function(res){
			var data=res.data
			html="";
			for (var i = 0; i < 4; i++) {
				html=html+"<li class='song-list__item'>"
				    +"<a href='songs.html?songid="+data.songList[i].songid+"'><span>"+(i+1)+"</span>"+data.songList[i].songname+"</a>"
				    +"<span>"+data.songerList[i].songername+"</span>"
				    +"</li>"
			}
			
			$(classname).html(html);
			
		}
		
	});
}

//新歌推荐
function newSong(){
	$.ajax({
		type:"post",
		url:"/wymusic/songs/songsTopListByArea",
		async: false,
		data:{'area':"巅峰新歌榜"},
		dataType: "json",//返回数据类型
		success: function(res){
			var data=res.data
			html="";
//			var random = parseInt(Math.random()*8);
			for (var i = 0; i < 8; i++) {
				html=html+"<li class='item'>"
				    +"<a href='songs.html?songid="+data.songList[i].songid+"' class='img'><img src='"+data.songList[i].songurl+"' alt='#'><i class='icon-play'></i></a>"
				    +"<div class='info'>"
				    +"<a href='songs.html?songid="+data.songList[i].songid+"' class='title'>"+data.songList[i].songname+"</a>"
				    +"<a href='songer-detail.html?songerid="+data.songerList[i].songerid+"' class='author'>"+data.songerList[i].songername+"</a>"
				    +"<span class='play-total'><span style='margin-right: 10px;'>点击量:</span>"+data.songList[i].songhot+"</span>"
				    +"</div>"
				    +"</li>"
				    
			}
			
			$(".mv-list").html(html);
		}
		
	});
}