$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	var pageSize =10;
	var count;
	var publicArea = "全部";
	var index = 1;
	count = onloadAll(pageSize,count);
	
	var navIndex = 0;
	$(".item").click(function(){
		$("#albumKind a").eq(navIndex).attr("class","item");
		$(this).attr("class","item item-cur");
		publicArea = $(this).text();
		count = onloadByKind(pageSize,count,publicArea);
		navIndex = $(this).index();
		index = 1;
		//点击对应页数跳转页
		$(".pageLi").click(function(){
			$("#pageUl li").eq(index).attr("class","");
			$(this).attr("class","active");
			var value = $(this).attr("value");
			onloadPage(value,pageSize,publicArea);
			index = $(this).index();
		});
		
		//上一页
		$("#pre").click(function(){
			var value = $("#pageUl li").eq(index).attr("value");
			var preValue= value-1;
			if(value==1||value==0){
				alert("已经是最前一页了！");
			}else{
				$("#pageUl li").eq(value).attr("class","");
				$("#pageUl li").eq(preValue).attr("class","active");
				onloadPage(preValue,pageSize,publicArea);
				index = preValue;
			}
		});
		
		//下一页
		$("#next").click(function(){
			var value = $("#pageUl li").eq(index).attr("value");
			if(count == 0||count == 1||value == count){
				alert("已经是最后一页了！");
			}else{
				var nextValue= ++value;
				$("#pageUl li").eq(value-1).attr("class","");
				$("#pageUl li").eq(nextValue).attr("class","active");
				onloadPage(nextValue,pageSize,publicArea);
				index = nextValue;
			}
		});
		
	});
	
	//点击对应页数跳转页
	$(".pageLi").click(function(){
		$("#pageUl li").eq(index).attr("class","");
		$(this).attr("class","active");
		var value = $(this).attr("value");
		onloadPage(value,pageSize,publicArea);
		index = $(this).index();
	});
	
	//上一页
	$("#pre").click(function(){
		var value = $("#pageUl li").eq(index).attr("value");
		var preValue= value-1;
		if(value==1||value==0){
			alert("已经是最前一页了！");
		}else{
			$("#pageUl li").eq(value).attr("class","");
			$("#pageUl li").eq(preValue).attr("class","active");
			onloadPage(preValue,pageSize,publicArea);
			index = preValue;
		}
	});
	
	//下一页
	$("#next").click(function(){
		var value = $("#pageUl li").eq(index).attr("value");
		if(count == 0||count == 1||value == count){
			alert("已经是最后一页了！");
		}else{
			var nextValue= ++value;
			$("#pageUl li").eq(value-1).attr("class","");
			$("#pageUl li").eq(nextValue).attr("class","active");
			onloadPage(nextValue,pageSize,publicArea);
			index = nextValue;
		}
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
})

function onloadPage(page,pageSize,publicArea){
	$.ajax({
		type:"post",
		url:"/wymusic/album/findByPage",
		async: false,
		data:{'page':page,'pageSize':pageSize,'publicArea':publicArea},
		dataType: "json",//返回数据类型
		success: function(res){
			console.log(res.data)
			var html="";
			$.each(res.data, function(index, obj){
				html=html+"<li>"
			    +"<a href='album-detail.html?albumid="+obj.albumid+"' class='img'>"
			    +"<img src='/pic/"+obj.albumoverurl+"' width=225px height=225px>"
			    +"<span class='mask'></span>"
			    +"<i class='icon-play'></i>"
			    +"</a>"
			    +"<div class='info'>"
			    +"<div class='title'>"
			    +"<a href='album-detail.html?albumid="+obj.albumid+"'>"+obj.albumname+"</a>"
			    +"</div>"
			    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='author'>"+obj.songername+"</a>"
			    +"</div>"
			    +"</li>";
			});
			$("#albumShow").html(html);
		}
		
	});
}

function onloadAll(pageSize,count){
	$.ajax({
		type:"post",
		url:"/wymusic/album/findAll",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			var html="";
			$.each(res.data, function(index, obj){
				if(index<pageSize){
					html=html+"<li>"
				    +"<a href='album-detail.html?albumid="+obj.albumid+"' class='img'>"
				    +"<img src='/pic/"+obj.albumoverurl+"' width=225px height=225px>"
				    +"<span class='mask'></span>"
				    +"<i class='icon-play'></i>"
				    +"</a>"
				    +"<div class='info'>"
				    +"<div class='title'>"
				    +"<a href='album-detail.html?albumid="+obj.albumid+"'>"+obj.albumname+"</a>"
				    +"</div>"
				    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='author'>"+obj.songername+"</a>"
				    +"</div>"
				    +"</li>";
				}else{
					return false;
				}
			});
			$("#albumShow").html(html);
		    count = Math.ceil(res.data.length/pageSize);
			pageHelper = "<li id='pre'><a href='#'><i class='fa fa-chevron-left'></i></a></li>"
				         +"<li class='active pageLi' value='1'><a href='#' >1</a></li>";
			for(var i=1; i<count;i++){
				var page = i+1;
				pageHelper=pageHelper+"<li value='"+page+"' class='pageLi'><a href='#'>"+page+"</a></li>";
			}
			pageHelper = pageHelper+"<li id='next'><a href='#'><i class='fa fa-chevron-right'></i></a></li>";
			$("#pageUl").html(pageHelper);
		}
	});
	
	return count;
}

function onloadByKind(pageSize,count,publicArea){
	$.ajax({
		type:"post",
		url:"/wymusic/album/findByPublicArea",
		async: false,
		data:{'publicArea':publicArea},
		dataType: "json",//返回数据类型
		success: function(res){
			var html="";
			$.each(res.data, function(index, obj){
				if(index<pageSize){
					html=html+"<li>"
				    +"<a href='album-detail.html?albumid="+obj.albumid+"' class='img'>"
				    +"<img src='/pic/"+obj.albumoverurl+"' width=225px height=225px>"
				    +"<span class='mask'></span>"
				    +"<i class='icon-play'></i>"
				    +"</a>"
				    +"<div class='info'>"
				    +"<div class='title'>"
				    +"<a href='album-detail.html?albumid="+obj.albumid+"'>"+obj.albumname+"</a>"
				    +"</div>"
				    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='author'>"+obj.songername+"</a>"
				    +"</div>"
				    +"</li>";
				}else{
					return false;
				}
			});
			$("#albumShow").html(html);
		    count = Math.ceil(res.data.length/pageSize);
		    pageHelper = "<li id='pre'><a href='#'><i class='fa fa-chevron-left'></i></a></li>"
		         +"<li class='active pageLi' value='1'><a href='#' >1</a></li>";
		    for(var i=1; i<count;i++){
		    	var page = i+1;
		    	pageHelper=pageHelper+"<li value='"+page+"' class='pageLi'><a href='#'>"+page+"</a></li>";
		    }
		    pageHelper = pageHelper+"<li id='next'><a href='#'><i class='fa fa-chevron-right'></i></a></li>";
		    $("#pageUl").html(pageHelper);
		}
	});
	
	return count;
}
