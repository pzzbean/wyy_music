var data;
var user;
$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					user = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	var url = location.href;
	if(url!=""){
		var songsid = url.substr(url.indexOf("=") + 1);
		$.ajax({
			type:"post",
			url:"/wymusic/songs/findSongsDetailInfoById",
			async: false,
			data:{'songsid':songsid},
			dataType: "json",//返回数据类型
			success: function(res){
				data=res.data;
				console.log(data);
				$(".data__name_txt").html(data.song.songname);
				$(".data__name_txt").attr("title",data.song.songname);
				$("#songPic").attr("src",data.song.songurl);
				$("#songer").html(data.songer.songername);
				$("#songer").attr("title",data.songer.songername);
				$("#songer").attr("href","songer-detail.html?songerid="+data.song.songerid);
				$("#play").attr("href","player.html?songid="+data.song.songid);
				$("#download").attr("href",data.song.mp3url);
				$("#download").attr("download",data.song.mp3url);
				if(JSON.stringify(data.album ) =='{}'){
					$("#album").append("无");
					$("#style").append("无");
					$("#area").append("无");
					$("#company").append("无");
					$("#publictime").append("无");
				}else{
					var time=new Date(data.album.publictime);
				    var publictime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate();
					$("#album").append(data.album.albumname);
					$("#style").append(data.album.albumstyle);
					$("#area").append(data.album.publicarea);
					$("#company").append(data.album.publiccompany);
					$("#publictime").append(publictime);
				}
				
				
				if(!(data.song.songlrc!==undefined)){
					$("#lrc_content").html("<h2>暂无相关歌词</h2>");
				}else{
					var lrc = data.song.songlrc;
					var lrcArr = lrc.split("\r\n\r\n");
					var html="";
					var html2="";
					for (var i = 0; i < lrcArr.length; i++) {
						if(i<=15){
							html=html+"<p></p>"+lrcArr[i]
						}else{
							html2=html2+"<p></p>"+lrcArr[i]
						}
					}
					$("#lrc_content").html(html+"<p></p><a class='lookLrc' id='lookLrc'>[查看全部歌词]</a>");
					$("#lrc_content2").html(html2+"<p></p><a class='lookLrc' id='lookLrc1'>[收起歌词]</a>");
					$("#lrc_content2").css("display","none");
					$("#lookLrc").click(function(){
						$("#lrc_content2").css("display","inline");
						$("#lookLrc").css("display","none");
					});
					
					$("#lookLrc1").click(function(){
						$("#lrc_content2").css("display","none");
						$("#lookLrc").css("display","inline");
					});
				}
				
			}
	   });
		
		$.ajax({
			type:"post",
			url:"/wymusic/comments/findCommentsBySongId",
			async: false,
			data:{'songId':songsid},
			dataType: "json",//返回数据类型
			success: function(res){
				data = res.data;
				if(data.commentsList.length!=0){
					var html="";
					var number = 0;
					for (var i = 0; i < data.commentsList.length; i++) {
						if(data.commentsList[i].status==1){
							number = number+1;
							var time=new Date(data.commentsList[i].pubtime);
							var pubtime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate();
							html=html+"<li class='comment-data'>"
							    +"<a><img alt='' src='/pic/"+data.userList[i].userpicurl+"' class='userPic'></a>"
							    +"<h4><a class='username'>"+data.userList[i].username+"</a></h4>"
							    +"<div><p class='comment-text'>"+data.commentsList[i].context+"</p></div>"
							    +"<div><span class='comment-time'>"+pubtime+"</span></div>"
							    +"</li>"
						}
					}
					$(".comment-title").html("精彩评论("+number+")");
					$(".show-comments ul").html(html);
				}else{
					$(".comment-title").html("精彩评论(0)");
					$(".show-comments ul").html("暂时没有任何评论，快来抢占沙发吧！");
				}
			}
		});
	}
	
	$("#play").click(function(){
		$("#play").attr("href","player.html?songid="+data.song.songid)
	});
	
	$("#download").click(function(){
		$("#download").attr("href",data.song.mp3url)
		$("#download").attr("download",data.song.mp3url)
	});
	
	$(".sendMyidea").click(function(){
		var cotext = $("textarea").val();
		var songsid = url.substr(url.indexOf("=") + 1);
		if(user!==undefined){
			var userid=user.userid;
			$.ajax({
				type:"post",
				url:"/wymusic/comments/addComments",
				async: false,
				data:{"songId":songsid,"cotext":cotext,"userid":userid},
				dataType: "json",
				success: function(res){
					alert(res.massage);
					$("textarea").val("");
					location.reload();
				}
			});
		}else{
			alert("亲，要登录后才可以评论歌曲哟！");
		}
	});
	
	$("#comment").click(function(){
		var a = $("#songs-comments").offset().top;  
		$("html,body").animate({scrollTop:a}, 'slow');   
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
	
		$(".addto_songlist").click(function(){
  			var songid = url.substr(url.indexOf("=") + 1);
  			if(user!==undefined){
  				var userid=user.userid;
//  			var x = $(this).offset().top;
//  			var y = $(this).offset().left;
//  			$("#songSetting").css("top",(x-100)+"px");
  				$.ajax({
  					type:"post",
  					url:"/wymusic/songlist/findSongListByUserId",
  					async: false,
  					data:{"userId":userid},
  					dataType: "json",//返回数据类型
  					success: function(res){
  						songlistArr = res.data.songListAndSongsDetailsList;
  						html="<li><a href='javascript:;' class='songlistname add_songlist'>新建歌单</a></li>";
  						for (var i = 0; i < songlistArr.length; i++) {
  							if(songlistArr[i].songlist.songliststatus==1){
  								html=html+"<li>"
  								    +"<input class='mysonglistid hide' value='"+songlistArr[i].songlist.songlistid+"'>"
  								    +"<a href='javascript:;' class='songlistname mysonglist'>"+songlistArr[i].songlist.songlistname+"</a>"
  								    +"</li>"
  							}
  						}
  						$("#songSetting").html(html);
  					}
  				});
  				
				$(".mysonglist").click(function(){
  					var songlistid = $(this).parent().find("input").val();
  					if(user!==undefined){
  						$.ajax({
  							type:"post",
  							url:"/wymusic/songlist/addToSongList",
  							async: false,
  							data:{"songlistid":songlistid,"songid":songid},
  							dataType: "json",//返回数据类型
  							success: function(res){
  								$("#songSetting").removeClass("in");
  								alert(res.massage);
  							}
  						});
  					}
  				});
  			}else{
  				alert("亲，要登录后才可以添加歌曲哟！");
  			}
  		});
})