$(function(){
	var user;
	//加载登录用户信息
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					user = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
  		var url = location.href;
  		if(url!=""){
  			var songerid = url.substr(url.indexOf("=") + 1);
  			$.ajax({
  				type:"post",
  				url:"/wymusic/songer/findSongerDetailInfoById",
  				async: false,
  				data:{'songerid':songerid},
  				dataType: "json",//返回数据类型
  				success: function(res){
  					var data = res.data;
  					var songlist = "";
  					var albumlist = "";
  					console.log(data);
  					$(".data__name_txt").html(data.songer.songername);
  					$(".data__name_txt").attr("title",data.songer.songername);
  					$("#country").append(data.songer.songercountry);
  					$(".info-data").append(data.songer.songerinfo);
  					$("#songs").append(data.songList.length+"首");
  					$("#album").append(data.albumList.length+"张");
  					$("#songerHot").append(data.songer.songerhot);
  					$("#songerPic").attr("src","/pic/"+data.songer.songerpicurl)
  					if(data.songList!=null){
  						for (var i = 0; i < data.songList.length; i++) {
  		  				     songlist=songlist+"<div class='song-playlist'>"
  								     +"<ul class='playList'>"
  								     +"<li class='playList-number'><div>"+(i+1)+"</div></li>"
  								     +"<li class='playList-songname'><input class='hide' type='text' id='songid' value='"+data.songList[i].songid+"'></input><a href='songs.html?songsid="+data.songList[i].songid+"' style='float: left;'>"+data.songList[i].songname+"</a>"
  								     +"<a href='#songSetting' data-toggle='collapse' class='playList-sign addto_songlist'><i class='glyphicon glyphicon-plus sign'></i></a>"
  								     +"<a href='"+data.songList[i].mp3url+"' download='"+data.songList[i].mp3url+"' class='playList-sign'><i class='glyphicon glyphicon-save sign'></i></a>"
  								     +"<a href='player.html?songid="+data.songList[i].songid+"' class='playList-sign'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
  								     +"</li>"
  								     +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songer.songerid+"'>"+data.songer.songername+"</a></li>"
  								     +"<li class='playList-songtime'>"+data.songList[i].songtime+"</li>"
  								     +"</ul>"
  								     +"</div>"
  							}
  		  					$(".songs-content").append(songlist);
  					}
  					
  					if(data.albumList!=null){
  						for (var i = 0; i < data.albumList.length; i++) {
  							var time=new Date(data.albumList[i].publictime);
  							var publictime=time.getFullYear()+"-"+(time.getMonth()+1)+"-"+time.getDate();
  							albumlist=albumlist+"<li>"
  							         +"<a href='album-detail.html?albumid="+data.albumList[i].albumid+"' class='img'>"
  							         +"<img src='/pic/"+data.albumList[i].albumoverurl+"' width=225px height=225px alt='#'>"
  							         +"<span class='mask'></span>"
  							         +"<i class='icon-play'></i>"
  							         +"</a>"
  							         +"<div class='info'>"
  							         +"<div class='title'>"
  							         +"<a href='album-detail.html?albumid="+data.albumList[i].albumid+"'>"+data.albumList[i].albumname+"</a>"
  							         +"</div>"
  							         +"<a href='#' class='author'>"+publictime+"</a>"
  							         +"</div>"
  							         +"</li>";
  						}
  						$(".slider-wrapper").append(albumlist);
  							
  					}
  				}
  		   });
  	    }
  		
  		$(".logout").click(function(){
  			$.ajax({
  				type:"post",
  				url:"/wymusic/user/removeSession",
  				async: false,
  				data:{},
  				dataType: "json",//返回数据类型
  				success: function(res){
  					location.reload();
  				}
  			});
  		});
  		
  		$(".addto_songlist").click(function(){
  			var songid = $(this).parent().find("#songid").val();
  			if(user!==undefined){
  				var userid=user.userid;
  				var x = $(this).offset().top;
//  			var y = $(this).offset().left;
  				$("#songSetting").css("top",(x-100)+"px");
  				$.ajax({
  					type:"post",
  					url:"/wymusic/songlist/findSongListByUserId",
  					async: false,
  					data:{"userId":userid},
  					dataType: "json",//返回数据类型
  					success: function(res){
  						songlistArr = res.data.songListAndSongsDetailsList;
  						html="<li><a href='javascript:;' class='songlistname add_songlist'>新建歌单</a></li>";
  						for (var i = 0; i < songlistArr.length; i++) {
  							if(songlistArr[i].songlist.songliststatus==1){
  								html=html+"<li>"
  								    +"<input class='mysonglistid hide' value='"+songlistArr[i].songlist.songlistid+"'>"
  								    +"<a href='javascript:;' class='songlistname mysonglist'>"+songlistArr[i].songlist.songlistname+"</a>"
  								    +"</li>"
  							}
  						}
  						$("#songSetting").html(html);
  					}
  				});
  				
  				$(".mysonglist").click(function(){
  					var songlistid = $(this).parent().find("input").val();
  					if(user!==undefined){
  						$.ajax({
  							type:"post",
  							url:"/wymusic/songlist/addToSongList",
  							async: false,
  							data:{"songlistid":songlistid,"songid":songid},
  							dataType: "json",//返回数据类型
  							success: function(res){
  								$("#songSetting").removeClass("in");
  								alert(res.massage);
  							}
  						});
  					}
  				});
  			}else{
  				alert("亲，要登录后才可以添加歌曲哟！");
  			}
  		});
  	})