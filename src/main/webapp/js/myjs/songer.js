$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	var pageSize = 15;
	var count;
	var index = 1;
	var songerCountry=null;
	count = onloadAll(pageSize,count);//加载首页
	
	//点击对应页数跳转页
	$(".pageLi").click(function(){
		$("#pageUl li").eq(index).attr("class","");
		$(this).attr("class","active");
		var value = $(this).attr("value");
		index = $(this).index();
		onloadPage(value,pageSize,songerCountry);
	});
	
	//上一页
	$("#pre").click(function(){
		var value = $("#pageUl li").eq(index).attr("value");
		var preValue= value-1;
		if(value==1||value==0){
			alert("已经是最前一页了！");
		}else{
			$("#pageUl li").eq(value).attr("class","");
			$("#pageUl li").eq(preValue).attr("class","active");
			onloadPage(preValue,pageSize,songerCountry);
			index = preValue;
		}
	});
	
	//下一页
	$("#next").click(function(){
		var value = $("#pageUl li").eq(index).attr("value");
		if(count == 0||count == 1||value == count){
			alert("已经是最后一页了！");
		}else{
			var nextValue= ++value;
			$("#pageUl li").eq(value-1).attr("class","");
			$("#pageUl li").eq(nextValue).attr("class","active");
			onloadPage(nextValue,pageSize,songerCountry);
			index = nextValue;
		}
	});
	
	//点击分类
	$(".title").click(function(){
		var Country = $(this).text();
		count = onloadByKind(pageSize,count,Country);
		
		index=1;
		//点击对应页数跳转页
		$(".pageLi").click(function(){
			$("#pageUl li").eq(index).attr("class","");
			$(this).attr("class","active");
			var value = $(this).attr("value");
			index = $(this).index();
			onloadPage(value,pageSize,songerCountry);
		});
		
		//上一页
		$("#pre").click(function(){
			var value = $("#pageUl li").eq(index).attr("value");
			var preValue= value-1;
			if(value==1||value==0){
				alert("已经是最前一页了！");
			}else{
				$("#pageUl li").eq(value).attr("class","");
				$("#pageUl li").eq(preValue).attr("class","active");
				onloadPage(preValue,pageSize,songerCountry);
				index = preValue;
			}
		});
		
		//下一页
		$("#next").click(function(){
			var value = $("#pageUl li").eq(index).attr("value");
			if(count == 0||count == 1||value == count){
				alert("已经是最后一页了！");
			}else{
				var nextValue= ++value;
				$("#pageUl li").eq(value-1).attr("class","");
				$("#pageUl li").eq(nextValue).attr("class","active");
				onloadPage(nextValue,pageSize,songerCountry);
				index = nextValue;
			}
		});
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
})

function onloadAll(pageSize,count){
	$.ajax({
		type:"post",
		url:"/wymusic/songer/findAll",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			var songerArray = new Array();
			var j = 0;
			for(var i=0;i<res.data.length;i++){
				if(res.data[i].songerstatus == 1){
					songerArray[j] =  res.data[i];
					j=j+1;
				}
			}
			var html="";
			$.each(songerArray, function(index, obj){
				console.log(obj);
				if(index<pageSize){
					html=html+"<li>"
					    +"<div class='songer-td'>"
					    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerPic'><img alt='' src='/pic/"+obj.songerpicurl+"' class='songer_img'></a>"
					    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerName'>"+obj.songername+"</a>"
					    +"</div></li>"
				}else{
					return false;
				}
			});
			$(".songer-ul").html(html);
		    count = Math.ceil(songerArray.length/pageSize);
			pageHelper = "<li id='pre'><a href='#'><i class='fa fa-chevron-left'></i></a></li>"
				         +"<li class='active pageLi' value='1'><a href='#' >1</a></li>";
			for(var i=1; i<count;i++){
				var page = i+1;
				pageHelper=pageHelper+"<li value='"+page+"' class='pageLi'><a href='#'>"+page+"</a></li>";
			}
			pageHelper = pageHelper+"<li id='next'><a href='#'><i class='fa fa-chevron-right'></i></a></li>";
			$("#pageUl").html(pageHelper);
		}
	});
	
	return count;
}

function onloadPage(page,pageSize,songerCountry){
	$.ajax({
		type:"post",
		url:"/wymusic/songer/findByPage",
		async: false,
		data:{'page':page,'pageSize':pageSize,'songerCountry':songerCountry},
		dataType: "json",//返回数据类型
		success: function(res){
			var html="";
			$.each(res.data, function(index, obj){
				html=html+"<li>"
			    +"<div class='songer-td'>"
			    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerPic'><img alt='' src='/pic/"+obj.songerpicurl+"' class='songer_img'></a>"
			    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerName'>"+obj.songername+"</a>"
			    +"</div></li>"
			});
			$(".songer-ul").html(html);
		}
	});
}

function onloadByKind(pageSize,count,songerCountry){
	$.ajax({
		type:"post",
		url:"/wymusic/songer/findBysongerCountry",
		async: false,
		data:{'songerCountry':songerCountry},
		dataType: "json",//返回数据类型
		success: function(res){
			var html="";
			$.each(res.data, function(index, obj){
				if(index<pageSize){
					html=html+"<li>"
				    +"<div class='songer-td'>"
				    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerPic'><img alt='' src='/pic/"+obj.songerpicurl+"' class='songer_img'></a>"
				    +"<a href='songer-detail.html?songerid="+obj.songerid+"' class='songerName'>"+obj.songername+"</a>"
				    +"</div></li>"
				}else{
					return false;
				}
			});
			$(".songer-ul").html(html);
		    count = Math.ceil(res.data.length/pageSize);
		    pageHelper = "<li id='pre'><a href='#'><i class='fa fa-chevron-left'></i></a></li>"
		         +"<li class='active pageLi' value='1'><a href='#' >1</a></li>";
		    for(var i=1; i<count;i++){
		    	var page = i+1;
		    	pageHelper=pageHelper+"<li value='"+page+"' class='pageLi'><a href='#'>"+page+"</a></li>";
		    }
		    pageHelper = pageHelper+"<li id='next'><a href='#'><i class='fa fa-chevron-right'></i></a></li>";
		    $("#pageUl").html(pageHelper);
		}
	});
	
	return count;
}
