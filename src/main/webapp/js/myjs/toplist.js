$(function(){
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
				}
			}
		}
		
	});
	
	$.ajax({
		type:"post",
		url:"/wymusic/songs/selectBySongHotDesc",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			var data=res.data
			console.log(data)
			html="";
			for (var i = 0; i < data.songList.length; i++) {
				html=html+"<div class='song-playlist'>"
				    +"<ul class='playList'>"
				    +"<li class='playList-number'><div>"+(i+1)+"</div></li>"
				    +"<li class='playList-songname'>"
				    +"<a href='songs.html?songsid="+data.songList[i].songid+"' style='float: left;'>"+data.songList[i].songname+"</a>"
				    +"<a href='"+data.songList[i].mp3url+"' download='"+data.songList[i].mp3url+"' class='playList-sign'><i class='glyphicon glyphicon-save sign'></i></a>"
				    +"<a href='javascript:;' class='playList-sign'><i class='glyphicon glyphicon-plus sign'></i></a>"
				    +"<a href='player.html?songid="+data.songList[i].songid+"' class='playList-sign'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
				    +"</li>"
				    +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songerList[i].songerid+"'>"+data.songerList[i].songername+"</a></li>"
				    +"<li class='playList-songtime'>"+data.songList[i].songtime+"</li>"
				    +"</ul>"
				    +"</div>"
			}
			$(".songs-list-datas").html(html);
		}
		
	});
	
	$(".title").click(function(){
		var area = $(this).text();
		$(".toplist-title").html(area);
		toplist(area);
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
})

function toplist(area){
	$.ajax({
		type:"post",
		url:"/wymusic/songs/songsTopListByArea",
		async: false,
		data:{'area':area},
		dataType: "json",//返回数据类型
		success: function(res){
			var data=res.data
			console.log(data)
			html="";
			for (var i = 0; i < data.songList.length; i++) {
				html=html+"<div class='song-playlist'>"
				    +"<ul class='playList'>"
				    +"<li class='playList-number'><div>"+(i+1)+"</div></li>"
				    +"<li class='playList-songname'>"
				    +"<a href='songs.html?songsid="+data.songList[i].songid+"' style='float: left;'>"+data.songList[i].songname+"</a>"
				    +"<a href='"+data.songList[i].mp3url+"' download='"+data.songList[i].mp3url+"' class='playList-sign'><i class='glyphicon glyphicon-save sign'></i></a>"
				    +"<a href='javascript:;' class='playList-sign'><i class='glyphicon glyphicon-plus sign'></i></a>"
				    +"<a href='player.html?songid="+data.songList[i].songid+"' class='playList-sign'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
				    +"</li>"
				    +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songerList[i].songerid+"'>"+data.songerList[i].songername+"</a></li>"
				    +"<li class='playList-songtime'>"+data.songList[i].songtime+"</li>"
				    +"</ul>"
				    +"</div>"
			}
			$(".songs-list-datas").html(html);
		}
		
	});
	
}
