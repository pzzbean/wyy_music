$(function(){
	var userPicUrl="";
	var songlistPicUrl="";
	var add_songlistPicUrl="";
	var userInfo;
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					userInfo = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
					$(".username").html(res.data.username);
					if(""!=res.data.userpicurl){
						$("#userPure").attr("src","/pic/"+res.data.userpicurl);
						$("#songerPic").attr("src","/pic/"+res.data.userpicurl);
					}
					$("#userid").attr("value",res.data.userid);
					$("#add_userid").attr("value",res.data.userid);
					$("#username").attr("value",res.data.username);
					$("#userphone").attr("value",res.data.userphone);
					$("#useremail").attr("value",res.data.useremail);
					$("#province").attr("value",res.data.province);
					$("#city").attr("value",res.data.city);
					if("女"==res.data.usersex){
						$("#women").attr("checked",true);
					}else{
						$("#men").attr("checked",true);
					}
					
					$.ajax({
						type:"post",
						url:"/wymusic/songlist/findSongListByUserId",
						async: false,
						data:{"userId":res.data.userid},
						dataType: "json",//返回数据类型
						success: function(res){
							songlistDate = res.data.songListAndSongsDetailsList;
							var html="";
							for (var i = 0; i < songlistDate.length; i++) {
								if(songlistDate[i].songlist.songliststatus==1){
									html=html+""
								    +"<ul style='width: 100%;height: 50px;'>"
								    +"<li class='songlist songlistname'>"
								    +"<input class='songlidid' value='"+songlistDate[i].songlist.songlistid+"' style='display: none'>"
								    +"<img alt='' src='/pic/"+songlistDate[i].songlist.songlistpic+"' style='width:50px;height:50px;float: left;' />"
								    +"<a href='songlist-detail.html?index="+songlistDate[i].songlist.songlistid+"' class='name'>"+songlistDate[i].songlist.songlistname+"</a>"
								    +"</li>"
								    +"<li class='songlist songscount'>"+songlistDate[i].songsList.length+"首</li>"
								    +"<li class='songlist songlistoper'>"
								    +"<a class=' songlistplay glyphicon glyphicon-play-circle' title='播放歌单'></a>"
								    +"<a class='glyphicon glyphicon-pencil edit-songlist' title='编辑歌单' data-toggle='modal' data-target='#exampleModal'></a>"
								    +"<a class='delsonglist glyphicon glyphicon-trash' title='删除歌单'></a>"
								    +"</li>"
								    +"</ul>"
								}
							}
							$(".mysonglist-data-cont").html(html);
						}
					});
					
				}else{
					$(".content").html("<a href='login.html' style='height: 400px; width: 100%;font-size: 20px;text-align: center;padding: 180px;'>亲，你还没有登录哦!点击我立即登录</a>");
				}
			}else{
				$(".content").html("<a href='login.html' style='height: 400px; width: 100%;font-size: 20px;text-align: center;padding: 180px;'>亲，你还没有登录哦!点击我立即登录</a>");
			}
		}
		
	});
	
	$(".mydata").click(function(){
		$(".mysonglist-cont").css("display","none");
		$(".mydata-cont").css("display","block");
	});
	$(".mysonglist").click(function(){
		$(".mysonglist-cont").css("display","block");
		$(".mydata-cont").css("display","none");
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
	
	$("#edit-data").click(function(){
		$("#username").removeAttr("readonly");
		$("#userphone").removeAttr("readonly");
		$("#useremail").removeAttr("readonly");
		$("#province").removeAttr("readonly");
		$("#city").removeAttr("readonly");
	});
	
	$("#userPicfile").change(function(){
		var form=document.getElementById("mydataFrom");
		var formData = new FormData(form);
		console.log(formData);
		$.ajax({
			type:"post",
			url:"/wymusic/user/showUserPic",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
				userPicUrl = res.str;
				$("#userPure").attr("src","/pic/"+res.str);
			}
		});
	});
	
	$("#save").click(function(){
		var form=document.getElementById("mydataFrom");
		var formData = new FormData(form);
	    formData.append("userPicUrl", userPicUrl);
		$.ajax({
			type:"post",
			url:"/wymusic/user/updateUser2",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
			   alert(res.massage);
			   $("#userPicfile").val("");
			}
		})
		location.reload();
	});
	
	//点击编辑弹框
	$(".edit-songlist").click('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget) // 模态触发器
		  var modal = $(this)
		  var rowIndex = modal.parent().parent();
		  var songlistid= $(rowIndex).find("li").eq(0).find("input").eq(0).val();
		  $.ajax({
				type:"post",
				url:"/wymusic/songlist/findSongListById",
				async: false,
				data:{"songlistid":songlistid},
				dataType: "json",//返回数据类型
				success: function(res){
					$("#songlistPic").attr("src","/pic/"+res.data.songlistpic);
					$("#edit-songlistid").attr("value",res.data.songlistid);
					$("#edit-songlistname").attr("value",res.data.songlistname);
					$("#edit-songlistInfo").val(res.data.songlistinfo);
				}
		  });
		  modal.find('.modal-title').text('编辑歌单信息');
	})
	
	$("#songlistPicfile").change(function(){
		var form=document.getElementById("mySonglistFrom");
		var formData = new FormData(form);
		console.log(formData);
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/showUserPic",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
				songlistPicUrl = res.str;
				$("#songlistPic").attr("src","/pic/"+res.str);
			}
		});
	});
	
	$("#edit_save_songlist").click(function(){
		var form=document.getElementById("mySonglistFrom");
		var formData = new FormData(form);
	    formData.append("songlistPicUrl", songlistPicUrl);
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/updateSonglist",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
			   alert(res.massage);
			   $("#songlistPicfile").val("");
//			   $("#mysonglist-data-cont").load(window.location.href+"#mysonglist-data-cont");
			   window.location.reload();
			}
		});
	});
	
	$(".songlistPicfile").change(function(){
		var form=document.getElementById("add_SonglistFrom");
		var formData = new FormData(form);
		console.log(formData);
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/showUserPic",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
				add_songlistPicUrl = res.str;
				$("#add_songlistPic").attr("src","/pic/"+res.str);
			}
		});
	});
	
	$("#add_songlist").click(function(){
		var form=document.getElementById("add_SonglistFrom");
		var formData = new FormData(form);
	    formData.append("add_songlistPicUrl", add_songlistPicUrl);
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/addSonglist",
			async: false,
			data: formData,  
		    processData: false,  
		    contentType: false, 
			dataType: "json",//返回数据类型
			success: function(res){
//			   alert(res.massage);
			   $("#songlistPicfile").val("");
			   window.location.reload();
			}
		});
	});
	
	$(".delsonglist").click(function(){
		var modal = $(this)
		var rowIndex = modal.parent().parent();
		var songlistid= $(rowIndex).find("li").eq(0).find("input").eq(0).val();
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/updateStatusById",
			async: false,
			data:{"songlistid":songlistid,"status":"0"},
			dataType: "json",//返回数据类型
			success: function(res){
				alert(res.massage);
				location.reload();
			}
		});
	});
	
	$(".songlistplay").click(function(){
		var modal = $(this)
		var rowIndex = modal.parent().parent();
		var songlistid= $(rowIndex).find("li").eq(0).find("input").eq(0).val();
		$.ajax({
			type:"post",
			url:"/wymusic/songlist/findSongsBySonglistId",
			async: false,
			data:{'songlistid':songlistid},
			dataType: "json",//返回数据类型
			success: function(res){
				var data = res.data;
				var songidList=new Array();
				for (var i = 0; i < data.songsList.length; i++) {
					songidList.push(data.songsList[i].songid);
				}
				console.log(songidList);
				window.location.href="player.html?songidList="+songidList+"";
			}
		});
	});
})
