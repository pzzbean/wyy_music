$(function(){
	var user;
	var url = location.href;
	//加载登录用户信息
	$.ajax({
		type:"post",
		url:"/wymusic/user/loaduser",
		async: false,
		data:{},
		dataType: "json",//返回数据类型
		success: function(res){
			if(undefined!==res.data){
				if(res.data.userauthority==0){
					user = res.data;
					$(".account").attr("href","mymusic.html");
					$(".account").html("欢迎你！"+res.data.account)
					$(".logout").css("display","block");
					$(".account").css("display","block");
					$(".login").css("display","none");
					$(".regist").css("display","none");
					
					if(url!=""){
						var songlistid = url.substr(url.indexOf("=") + 1);
						$.ajax({
							type:"post",
							url:"/wymusic/songlist/findSongsBySonglistId",
							async: false,
							data:{'songlistid':songlistid},
							dataType: "json",//返回数据类型
							success: function(res){
								var data = res.data;
								console.log(data);
							    $(".data__name_txt").html(data.songlist.songlistname);
								$(".data__name_txt").attr("title",data.songlist.songlistname);
								$("#songer").attr("title",data.songlist.username);
							    $("#songer").html(data.songlist.username);
								$("#songerPic").attr("src","/pic/"+data.songlist.songlistpic);
								$(".info-data").append(data.songlist.songlistinfo);
								var html="";
								if(data.songsList.length!=0){
									for (var i = 0; i < data.songsList.length; i++) {
				  				        html=html+"<div class='song-playlist'>"
										     +"<ul class='playList'>"
										     +"<li class='playList-number'><div>"+(i+1)+"</div></li>"
										     +"<li class='playList-songname'><input class='hide' type='text' id='songid' value='"+data.songsList[i].songid+"'></input><a href='songs.html?songsid="+data.songsList[i].songid+"' style='float: left;'>"+data.songsList[i].songname+"</a>"
										     +"<a href='#songSetting' data-toggle='collapse' class='playList-sign addto_songlist'><i class='glyphicon glyphicon-plus sign'></i></a>"
										     +"<a href='"+data.songsList[i].mp3url+"' download='"+data.songsList[i].mp3url+"' class='playList-sign'><i class='glyphicon glyphicon-save sign'></i></a>"
										     +"<a href='player.html?songid="+data.songsList[i].songid+"' class='playList-sign'><i class='glyphicon glyphicon-play-circle sign'></i></a>"
										     +"</li>"
										     +"<li class='playList-songer'><a href='songer-detail.html?songerid="+data.songerList[i].songerid+"'>"+data.songerList[i].songername+"</a></li>"
										     +"<li class='playList-songtime'>"+data.songsList[i].songtime+"</li>"
										     +"</ul>"
										     +"</div>"
									}
					  				$(".songs-content").append(html);
								}else{
									$(".songs-content").html("<div style='margin: 50px 0 0 480px;font-size: 20px;'>此歌单暂无任何歌曲快去添加吧！</div>");
								}
							}
					   });
				    }
					
					
				}
			}
		}
		
	});
	
	$(".logout").click(function(){
		$.ajax({
			type:"post",
			url:"/wymusic/user/removeSession",
			async: false,
			data:{},
			dataType: "json",//返回数据类型
			success: function(res){
				location.reload();
			}
		});
	});
	
	$(".addto_songlist").click(function(){
		var songid = $(this).parent().find("#songid").val();
		if(user!==undefined){
			var userid=user.userid;
			var x = $(this).offset().top;
//			var y = $(this).offset().left;
			$("#songSetting").css("top",(x-100)+"px");
			$.ajax({
				type:"post",
				url:"/wymusic/songlist/findSongListByUserId",
				async: false,
				data:{"userId":userid},
				dataType: "json",//返回数据类型
				success: function(res){
					songlistArr = res.data.songListAndSongsDetailsList;
					html="<li><a href='javascript:;' class='songlistname add_songlist'>新建歌单</a></li>";
					for (var i = 0; i < songlistArr.length; i++) {
						if(songlistArr[i].songlist.songliststatus==1){
							html=html+"<li>"
							    +"<input class='mysonglistid hide' value='"+songlistArr[i].songlist.songlistid+"'>"
							    +"<a href='javascript:;' class='songlistname mysonglist'>"+songlistArr[i].songlist.songlistname+"</a>"
							    +"</li>"
						}
					}
					$("#songSetting").html(html);
				}
			});
			
			$(".mysonglist").click(function(){
				var songlistid = $(this).parent().find("input").val();
				if(user!==undefined){
					$.ajax({
						type:"post",
						url:"/wymusic/songlist/addToSongList",
						async: false,
						data:{"songlistid":songlistid,"songid":songid},
						dataType: "json",//返回数据类型
						success: function(res){
							$("#songSetting").removeClass("in");
							alert(res.massage);
						}
					});
				}
			});
		}else{
			alert("亲，要登录后才可以添加歌曲哟！");
		}
	});
	
 })